<!DOCTYPE html>
<?php 
    $title = 'Aviso de Privacidad Grupo COB';
	$description = 'Este es nuestro aviso de privacidad.';
	$keywords = 'Aviso, privacidad, grupo, cob';
    $author = 'Posicionart';
    
    $httpProtocol = 'https://';
    $host = $_SERVER['SERVER_NAME'];
    $url = '/grupocob/';

    include('commons/_headOpen.php');
    $css .= '
        <link rel="stylesheet" type="text/css" href="'.$httpProtocol.$host.$url.'css/stylesSecondaryNavbar.css">
        ';
    include('commons/_headClose.php');
    include('views/navbar2.html');
    include('views/aviso-privacidad.html');
    include('views/footer.html');
?>