<!DOCTYPE html>
<?php 
    session_start();
    $_SESSION["error"] = 0;
    $title = 'Grupo Cob, servicios para la construcción en Querétaro';
	$description = 'Arquitectura, ingeniería, modelaje y renders para construcciones residenciales, comerciales e industriales en Querétaro';
	$keywords = 'arquitectura, ingeniería, servicios para construcción, residenciales, comerciales, industriales';
    $author = 'Posicionart';
    
    $httpProtocol = 'https://';
    $host = $_SERVER['SERVER_NAME'];
    $url = '/grupocob/';

    $page = 'contacto';
    include('commons/_headOpen.php');
    $js.= '
        <script src="'.$httpProtocol.$host.$url.'js/contacto.js"></script>';
    $css .= '
        <link rel="stylesheet" type="text/css" href="'.$httpProtocol.$host.$url.'css/stylesHome.css">
        <link rel="stylesheet" type="text/css" href="'.$httpProtocol.$host.$url.'css/stylesContacto.css">
        ';
    include('commons/_headClose.php');
    include('views/navbar.html');
    include('views/contacto.html');
    include('views/footer.html');
?>