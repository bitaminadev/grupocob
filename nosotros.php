<!DOCTYPE html>
<?php 
    $title = 'Grupo Cob despacho de proyectos arquitectónicos en Querétaro';
	$description = 'En Grupo Cob desarrollamos proyectos de vivienda, comercial e industrial en Querétaro. Servicios de arquitectura, modelos 3d, ingeniería y construcción.';
	$keywords = 'grupo cob, despacho en querétaro, proyectos arquitectónicos, proyecto comercial, proyecto de vivienda, proyecto industrial';
    $author = 'Posicionart';
    
    $httpProtocol = 'https://';
    $host = $_SERVER['SERVER_NAME'];
    $url = '/grupocob/';

    $page = 'nosotros';
    include('commons/_headOpen.php');
    $css .= '
        <link rel="stylesheet" type="text/css" href="'.$httpProtocol.$host.$url.'css/stylesNosotros.css">
        <link rel="stylesheet" type="text/css" href="'.$httpProtocol.$host.$url.'css/stylesHome.css">
        ';
    include('commons/_headClose.php');
    include('views/navbar.html');
    include('views/nosotros.html');
    include('views/footer.html');
?>