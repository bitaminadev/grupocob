<?php
session_start();
$httpProtocol = 'https://';
$host = $_SERVER['SERVER_NAME'];
$url = '/grupocob/';
if(isset($_POST["name"]) && isset($_POST["email"]) && isset($_POST["tel"]) && isset($_POST["service"]) && isset($_POST["message"])){
    include("../mail/class.phpmailer.php");
    include("../mail/class.smtp.php");
    
    $nombre = $_POST["name"];
    $email = $_POST["email"];
    $telefono = $_POST["tel"];
    $servicio = $_POST["service"];
    $mensaje = $_POST["message"];
    $contenido = "";
    
    $contenido .= "<html>
			<head>
				<meta charset='UTF-8'>
				<style>
					*{
						font-family: Arial;
					}
					td{
						border: 1px solid #ddd;
					}
					table{
						background: #f1f1f1;
						border-collapse: collapse;
					}
				</style>
			</head>
			<body>
				<table width='600' align='center' valign='center' cellpadding='10'>
					<tr>
						<td colspan='2'><img src='https://demos.posicionart.com/grupocob/img/grupo_cob_logo.png'></td>
					</tr>
					<tr>
						<td>Nombre: </td><td>".$nombre."</td>
					</tr>
					<tr>
						<td>Correo: </td><td>".$email."</td>
					</tr>
					<tr>
						<td>Teléfono: </td><td>".$telefono."</td>
					</tr>
					<tr>
						<td>Servicio de Interés: </td><td>".$servicio."</td>
					</tr>
                    <tr>
						<td>Mensaje: </td><td>".$mensaje."</td>
					</tr>
				</table>
			</body>
		</html>";
    
    $mail = new PHPMailer(); 
    //$mail->IsSMTP();
    //$mail->SMTPAuth = true;
    //$mail->Host = "titan.hosting-mexico.net";
    //$mail->Port = 587;
    //$mail->Password = 'WExtNEo}3T}M'; 
    //$mail->Username = "info@grupocob.com";
    $mail->From = "info@grupocob.com";
    $mail->FromName = "Formulario de contacto web";
    $mail->AddAddress("ventas@grupocob.com.mx");
    $mail->AddBCC("testposicionart@gmail.com");
    $mail->AddBCC("maria.grupocobertura@gmail.com");
    $mail->IsHTML(true); 
    //$contenido = utf8_decode($contenido);
    $mail->Subject = "Correo enviado desde formulario de Contacto";
    $mail->Body = $contenido;
    
    if($mail->Send()){
	   header('Location: '.$httpProtocol.$host.$url.'correo-enviado.php');
    }
}else{
    $_SESSION["error"] = 1;
	header('Location: '.$_SERVER['HTTP_REFERER']);
}
?>