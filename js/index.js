$(document).ready(function() {
    $(".services")
    .mouseenter(function() {
        var background = $(this).data('bg');
        
        $(".services").each(function() {
            $(this).css('background', 'rgba(0,0,0,0.3)');
        });
        
        $("#capa").css({
          'background-image': background,
          'display': 'flex'
        });
        
    });
    
    $("#capa").mouseleave(function(){
        $(this).css({
          'background-color': 'rgba(0,0,0,0)'
        });
        $(".services").each(function() {
            $(this).css('background','');
        });
    })
    
    $("#capa").mousemove(function(e){      
      $(".services").each(function(index, elemento){
        if(e.originalEvent.clientX >= $(elemento).offset().left){
          $("#capa").css({
            'background-image': $(elemento).data('bg')
          });
        }
      })
    })
});