$(document).ready(function() {
    $('#nombre-caso').html('Harley Davison QRO');
    $('#descripcion-caso').html('Diseño y construcción - Área de 1,550 M2');
    
    // cuando se hace el slide a traves de los puntos
    $('ol.carousel-indicators > li').on("click",function() {
        
        var clickedObj = $('ol.carousel-indicators > li.clicked');
        clickedObj.removeClass('clicked');
        
        $(this).addClass('clicked');
        showInformation2();
    });
    
    // cuando se hace el slide automatico
    $('#myCarousel').bind('slide.bs.carousel', function (e) {
        showInformation();
    });
    
});

function showInformation2(){
    var title;
    var desc;
    var active = $('ol.carousel-indicators > li.clicked');

    title = active.data('title');
    desc = active.data('desc');
    $('#nombre-caso').html(title);
    $('#descripcion-caso').html(desc);
}

function showInformation(){
    var clickedObj = $('ol.carousel-indicators > li.clicked');
    clickedObj.removeClass('clicked');
    var title;
    var desc;
    var active = $('ol.carousel-indicators > li.active');
    var index = active.data('index');
    var newIndex = 0;
    var activeIndex;
    
    if(index == 9){
        newIndex = 0;
    }else{
        newIndex = index + 1;
    }
    
    $('ol.carousel-indicators > li').each(function(i,e){
       if( $(e).data('index') == newIndex ){
           activeIndex = $(e);
       }
    });

    title = activeIndex.data('title');
    desc = activeIndex.data('desc');
    $('#nombre-caso').html(title);
    $('#descripcion-caso').html(desc);
}

$('#myCarousel').carousel({
    interval: false
});

//scroll slides on swipe for touch enabled devices
$("#myCarousel").on("touchstart", function (event) {
    var yClick = event.originalEvent.touches[0].pageY;
    $(this).one("touchmove", function (event) {
        var yMove = event.originalEvent.touches[0].pageY;
        if (Math.floor(yClick - yMove) > 1) {
            $(".carousel").carousel('next');
        } else if (Math.floor(yClick - yMove) < -1) {
            $(".carousel").carousel('prev');
        }
    });
    $(".carousel").on("touchend", function () {
        $(this).off("touchmove");
    });
});