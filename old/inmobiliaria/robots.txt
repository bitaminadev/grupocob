User-agent: Googlebot

Disallow: /images/*
Disallow: /css/*
Disallow: /js/*
Disallow: /consola/*
Disallow: /cabeza.php
Disallow: /correo-enviado.htm
Disallow: /correo-no-enviado.htm
Disallow: /correo.php
Disallow: /pie.php
Disallow: /propiedad.php
Allow:/

User-agent: * 

Disallow: /images/*
Disallow: /css/*
Disallow: /js/*
Disallow: /consola/*
Disallow: /cabeza.php
Disallow: /correo-enviado.htm
Disallow: /correo-no-enviado.htm
Disallow: /correo.php
Disallow: /pie.php
Disallow: /propiedad.php

Sitemap:http://inmobiliaria.grupocob.com.mx/sitemap.xml