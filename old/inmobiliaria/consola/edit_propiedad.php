<?php session_start(); if(!isset($_SESSION['usr'])){header("Location:login.php");} ?>
<?php $cabeza='
	<title>Propiedad</title>
	<link href="css/propiedades.css" rel="stylesheet" type="text/css" />
	<script type="text/javascript" src="js/jquery-1.10.1.js"></script>
	
	<script type="text/javascript" src="js/custom-form-elements.js"></script>
	<script type="text/javascript" src="js/gen_validatorv4.js"></script>
	<script type="text/javascript" src="js/ajaxupload.3.5.js"></script>
	<script type="text/javascript" src="js/validCampoFranz.js"></script>
	<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
	<script type="text/javascript">
			//<!--
			$(document).ready(function() {
				subir();
				cargar();
				contar ();
				$("#datos5").validCampoFranz("0123456789");  
				$("#recamaras").validCampoFranz("0123456789");  
				$("#estacionamiento").validCampoFranz("0123456789");  
				$("#niveles").validCampoFranz("0123456789");   
				$("#valor").validCampoFranz("0123456789");   
				$("#cancelar").click(function(){
					window.location="index.php";
				});
				
				$(".elim").click(function(){ $(this).parent("li").remove(); 
					contar (); });
			});	
			
			// -->
		</script>'; ?>
<?php require("estructura/cabeza.php"); ?>
	<form action="home_sql.php?ev=21" method="post" name="MyForm" id="MyForm">
		<p class="campos">T&iacute;tulo: <input type="text" name="titulo" id="titulo"/></p>
		<p class="campos">Keywords: <input type="text" name="keywords" id="keywords"/></p>
		<p class="campos">Descripci&oacute;n: <input type="text" name="descripcion" id="descripcion"/></p>	
		<p class="campos dir">Direcci&oacute;n de la propiedad</p>
		<div class="linea">
			<p class="campos">Calle y N&uacute;mero: 	<input type="text" name="calle" id="datos6"/></p>
			<p class="campos">Colonia: 	<input type="text" name="colonia" id="datos2"/></p>
			<p class="campos">Ciudad: 	<input type="text" name="ciudad" id="datos1"/></p>
			<p class="campos">Pa&iacute;s: 	<input type="text" name="pais" id="datos7"/></p>
		</div>
		<p class="campos dir">Informaci&oacute;n del Propietario</p>
		<div class="linea">
			<p class="campos">Nombre: 	<input type="text" name="propietario" id="datos3"/></p>
			<p class="campos">Correo: 	<input type="text" name="correo" id="datos4"/></p>
			<p class="campos">Tel&eacute;fono: 	<input type="text" name="telefono" id="datos5"/></p>
		</div>
		<div class="informacion">
			<div class="inmueble">
				<p>Tipo de Inmueble:</p>
				<div class="radio">
					<label>Bodega</label>
					<input type="radio" value="Bodega" name="inmueble" class="styled" id="Bodega"/>
				</div>
				<div class="radio">
					<label>Departamento</label>
					<input type="radio" value="Departamento" name="inmueble" class="styled" id="Departamento"/>
				</div>
				<div class="radio">
					<label>Oficina</label>
					<input type="radio" value="Oficina" name="inmueble" class="styled" id="Oficina" />
				</div>
				<div class="radio">
					<label>Rancho</label>
					<input type="radio" value="Rancho" name="inmueble" class="styled" id="Rancho"/>
				</div>
				<div class="radio">
					<label>Casa</label>
					<input type="radio" value="Casa" name="inmueble" class="styled" id="Casa"/>
				</div>
				<div class="radio">
					<label>Local</label>
					<input type="radio" value="Local" name="inmueble" class="styled" id="Local"/>
				</div>
				<div class="radio">
					<label>Terreno</label>
					<input type="radio" value="Terreno" name="inmueble" class="styled" id="Terreno"/>
				</div>
			</div>
			<div class="operacion">
				<p>Tipo de Operaci&oacute;n:</p>
				<div class="radio">
					<label>Venta</label>
					<input type="radio" value="Venta" name="operacion" class="styled" id="Venta"/>
				</div>
				<div class="radio">
					<label>Renta</label>
					<input type="radio" value="Renta" name="operacion" class="styled" id="Renta" />
				</div>
			</div>
			<div class="imagenes">
				<div class="img"><input type="button" id="me1" class="boton btn3" onclick="" value="Im&aacute;genes"/><p id="mestatu1"></p><p id="files1"></p></div>
				<a href="javascript:izq();"><img src="../images/flechas_izq.png" alt="flechas_izq"/></a>
				<div class="cantidad">
					<ul>
					</ul>
				</div>
				<a href="javascript:der();"><img src="../images/flechas_der.png" alt="flechas_der"/></a>
				<input type="text" name="imagenes" class="oculto" value=""/>
			</div>
			<div class="inf">
				<label class="desc">Descripci&oacute;n: <textarea cols="3" rows="3" name="des" id="des"></textarea></label>
				<label>Mapa:<input type="text" name="direccion" id="direccion"/> <a href="http://maps.google.com/" target="_blank">A&ntilde;adir Mapa</a></label>
				<label>Valor:<input type="text" name="valor" id="valor"/></label>
				<label class="terreno">Terreno m<sup>2</sup>:<input type="text" name="terreno" id="terreno"/></label>
				<label>Construcci&oacute;n <br/>m<sup>2</sup>:<input type="text" name="construccion" id="construccion"/></label>
				<label>Antig�edad <input type="text" name="antigu" id="antigu"/></label>
			</div>
			<div class="opciones">
				<div class="text">
					<label>Rec&aacute;maras<input type="text" name="recamaras"id="recamaras"/></label>
					<label>Ba&ntilde;os<input type="text" name="banios" id="banios"/></label>
					<label>Estacionamiento<input type="text" name="estacionamiento" id="estacionamiento"/></label>
					<label>Niveles<input type="text" name="niveles" id="niveles"/></label>
				</div>
				<label class="otro">Otro<textarea cols="3" rows="3" name="otro" id="otro"></textarea></label>
				<div class="checkbox">
					<input type="checkbox" value="Cocina" name="cocina" class="styled" id="cocina"/>
					<label>Cocina</label>
				</div>
				<div class="checkbox">
					<input type="checkbox" value="Family Room" name="family_room" class="styled" id="family_room"/>
					<label>Family Room</label>
				</div>
				<div class="checkbox">
					<input type="checkbox" value="Sala" name="Sala" class="styled" id="sala"/>
					<label>Sala</label>
				</div>
				<div class="checkbox">
					<input type="checkbox" value="Comedor" name="Comedor" class="styled" id="comedor"/>
					<label>Comedor</label>
				</div>
				<div class="checkbox">
					<input type="checkbox" value="Jard&iacute;n" name="Jardin" class="styled" id="jardin"/>
					<label>Jard&iacute;n</label>
				</div>
				<div class="checkbox">
					<input type="checkbox" value="Vestidor" name="Vestidor" class="styled" id="vestidor"/>
					<label>Vestidor</label>
				</div>
				<div class="checkbox">
					<input type="checkbox" value="Cuarto de Lavado" name="Cuarto_de_Lavado" class="styled" id="cuarto_lavado"/>
					<label>Cuarto de Lavado</label>
				</div>
				<div class="checkbox">
					<label>Estudio</label>
					<input type="checkbox" value="Estudio" name="estudio" class="styled" id="estudio"/>
				</div>
				<div class="checkbox">
					<label>Roof Garden</label>
					<input type="checkbox" value="Roof Garden" name="Roof_garden" class="styled" id="Roof_garden"/>
				</div>
				<div class="checkbox">
					<label>Cuarto de Servicio</label>
					<input type="checkbox" value="Cuarto de Servicio" name="cuarto_servicio" class="styled" id="cuarto_servicio"/>
				</div>
			</div>
			<div class="detalles">
				<p>Detalles</p>
				<div class="checkbox">
					<label>Aparece en home</label>
					<input type="checkbox" value="home" name="home" class="styled" id="home"/>
				</div>
				<div class="checkbox">
					<label>Casa nueva</label>
					<input type="checkbox" value="nueva" name="nueva" class="styled" id="casa_nueva"/>
				</div>
				<div class="checkbox">
					<label>Destacada</label>
					<input type="checkbox" value="Destacada" name="Destacada" class="styled" id="destacada"/>
				</div>
			</div>
		</div>
			<input type="submit" name="guardar" value="Guardar" id="Enviar"/>
			<input type="button" name="cancelar" value="Cancelar" id="cancelar"/>
			<input type="text" name="id" id="id"/>
	</form>
	<script type="text/javascript">
		var frmvalidator  = new Validator("MyForm");
		frmvalidator.addValidation("titulo","req","Por favor coloca un t�tulo");
		frmvalidator.addValidation("keywords","req","Por favor coloca unas palabras clave");
		frmvalidator.addValidation("descripcion","req","Por favor coloca una descripci�n");
		frmvalidator.addValidation("correo","email","Correo inv�lido");
	</script>
<script type="text/javascript">
	x=0;
	function subir(){
			var btnUpload=$('#me1');
			var mestatus=$('#mestatus1');
			var files=$('#files1');
			new AjaxUpload(btnUpload, {
				action: 'uploadPhotoG.php',
				name: 'uploadfile[]',
				data: {dir:"../images/propiedades/"},
				onSubmit: function(file, ext){
					if (! (ext && /^(jpg|jpeg)$/.test(ext))){
						// extension is not allowed 
						alert('Solo JPG o JPEG');
						return false;
					}
					mestatus.html('<img src="images/ajax-loader.gif" height="16" width="16">');
				},
				onComplete: function(file, response){
					//On completion clear the status
					mestatus.text('');
					//On completion clear the status
					files.html('');
					//Add uploaded file to list
					if(response!="error"){
						x++;
						lista = response;
						//lista="<li class='imagen'><img src='../images/propiedades/"+file+"' alt='"+file+"'/><span class='elim'>Eliminar</span></li>";
						 $(".cantidad ul").append(lista);
						 lista2= $(".cantidad ul").html();
						$('.oculto').val(lista2);// cuando lo elimina :::::::::
						contar ();
						$(".elim").click(function(){
							$(this).parent("li").remove();
							contar ();
						});
					} else{
						alert("Erro al subir "+file);
					}
				}
			});
		}
	var cant = 0;
	var numero = 0;
	 var datosCom1;
	 
	function contar (){
		x=0;
		$(".cantidad ul li").each(function(index){
			x++;
		});
		lista2= $(".cantidad ul").html();
		$('.oculto').val(lista2);
	}
	function izq(){
		numero = x*162;
		if(cant+4 < x){
			$('.cantidad ul').animate({'margin-left':'-=162px'},200);
			cant +=1;
		}
	}
	function der(){
		numero = x*156;
		if(cant > 0){
			$('.cantidad ul').animate({'margin-left':'+=162px'},200);
			cant-=1;
		}
	}
	 $(function() {
			datosCom1 = [
		<?php
		require("lib/conectar_php.php");
			$dbinfo = mysql_query ("SELECT DISTINCT ciudad FROM inmobiliaria_tab_propiedades");	
			while ($variables= mysql_fetch_array($dbinfo)){ ?>
				"<?php echo  ($variables["ciudad"]); ?>",
		<?php } ?>
		];
		$( "#datos1" ).autocomplete({
		source: datosCom1
		});
	});
	function cargar(){
		<?php if(isset($_GET['id'])){
			require("lib/conectar_php.php");
			$dbinfo = mysql_query ("SELECT * FROM inmobiliaria_tab_propiedades WHERE id = ".$_GET['id']);	
			while ($variables= mysql_fetch_array($dbinfo)){ 
			echo "$('#titulo').val('".$variables['titulo']."');";
			echo "$('#descripcion').val('".($variables['descripcion1'])."');";
			echo "$('#keywords').val('".($variables['keywords'])."');";
			echo "$('#datos6').val('".($variables['calle'])."');";
			echo "$('#datos1').val('".($variables['ciudad'])."');";
			echo "$('#datos2').val('".($variables['colonia'])."');";
			echo "$('#datos7').val('".($variables['pais'])."');";
			echo "$('#datos3').val('".($variables['propietario'])."');";
			echo "$('#datos4').val('".($variables['correo'])."');";
			echo "$('#datos5').val('".($variables['telefono'])."');";
			echo "$('#".$variables['inmueble']."').attr('checked', true);";
			echo "$('#".$variables['operacion']."').attr('checked', true);";
			echo "$('.cantidad ul').html('".$variables['imagenes']."');";
			echo "$('.imagenes .oculto').val('".$variables['imagenes']."');";
			echo "$('#des').html('".(str_replace("<br/>","\\n",$variables['descripcion']))."');";
			echo "$('#direccion').val('".($variables['direccion'])."');";
			echo "$('#valor').val('".($variables['valor'])."');";
			echo "$('#terreno').val('".($variables['terreno'])."');";
			echo "$('#construccion').val('".($variables['construccion'])."');";
			echo "$('#antigu').val('".($variables['antiguedad'])."');";
			echo "$('#recamaras').val('".($variables['recamaras'])."');";
			echo "$('#banios').val('".($variables['banios'])."');";
			echo "$('#estacionamiento').val('".($variables['estacionamiento'])."');";
			echo "$('#niveles').val('".($variables['niveles'])."');";
			echo "$('#otro').html('".(str_replace("<br/>","\\n",$variables['otro']))."');";
			if($variables['cocina']==1)
				echo "$('#cocina').attr('checked', true);";
			if($variables['family_room']==1)
				echo "$('#family_room').attr('checked', true);";
			if($variables['sala']==1)
				echo "$('#sala').attr('checked', true);";
			if($variables['comedor']==1)
				echo "$('#comedor').attr('checked', true);";
			if($variables['jardin']==1)
				echo "$('#jardin').attr('checked', true);";
			if($variables['vestidor']==1)
				echo "$('#vestidor').attr('checked', true);";
			if($variables['cuarto_lavado']==1)
				echo "$('#cuarto_lavado').attr('checked', true);";
			if($variables['estudio']==1)
				echo "$('#estudio').attr('checked', true);";
			if($variables['roof_garden']==1)
				echo "$('#Roof_garden').attr('checked', true);";
			if($variables['cuarto_servicio']==1)
				echo "$('#cuarto_servicio').attr('checked', true);";
			if($variables['home']==1)
				echo "$('#home').attr('checked', true);";
			if($variables['casa_nueva']==1)
				echo "$('#casa_nueva').attr('checked', true);";
			if($variables['destacada']==1)
				echo "$('#destacada').attr('checked', true);";
			echo "$('#id').val('".$variables['id']."');";
			}
			echo "$('#MyForm').attr('action','home_sql.php?ev=31')";
		}
		?>
	}
</script>
	
<?php require("estructura/pie.php"); ?>