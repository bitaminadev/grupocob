-- phpMyAdmin SQL Dump
-- version 3.4.10.2
-- http://www.phpmyadmin.net
--
-- Servidor: localhost:3306
-- Tiempo de generación: 16-11-2016 a las 11:26:44
-- Versión del servidor: 5.1.56
-- Versión de PHP: 5.3.29

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `inmobiliaria`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `inmobiliaria_tab_propiedades`
--

CREATE TABLE IF NOT EXISTS `inmobiliaria_tab_propiedades` (
  `titulo` varchar(60) NOT NULL,
  `keywords` text NOT NULL,
  `descripcion1` text NOT NULL,
  `calle` text NOT NULL,
  `ciudad` text NOT NULL,
  `colonia` text NOT NULL,
  `pais` text NOT NULL,
  `propietario` varchar(150) NOT NULL,
  `correo` varchar(100) NOT NULL,
  `telefono` text NOT NULL,
  `inmueble` text NOT NULL,
  `operacion` text NOT NULL,
  `imagenes` text NOT NULL,
  `descripcion` text NOT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `direccion` text NOT NULL,
  `valor` int(11) NOT NULL,
  `terreno` text NOT NULL,
  `construccion` text NOT NULL,
  `antiguedad` text NOT NULL,
  `recamaras` int(11) NOT NULL,
  `banios` float NOT NULL,
  `estacionamiento` int(11) NOT NULL,
  `niveles` int(11) NOT NULL,
  `cocina` tinyint(1) NOT NULL,
  `family_room` tinyint(1) NOT NULL,
  `sala` tinyint(1) NOT NULL,
  `comedor` tinyint(1) NOT NULL,
  `jardin` tinyint(1) NOT NULL,
  `vestidor` tinyint(1) NOT NULL,
  `cuarto_lavado` tinyint(1) NOT NULL,
  `estudio` tinyint(1) NOT NULL,
  `roof_garden` tinyint(1) NOT NULL,
  `cuarto_servicio` tinyint(1) NOT NULL,
  `otro` text NOT NULL,
  `home` tinyint(1) NOT NULL,
  `casa_nueva` tinyint(1) NOT NULL,
  `destacada` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=25 ;

--
-- Volcado de datos para la tabla `inmobiliaria_tab_propiedades`
--

INSERT INTO `inmobiliaria_tab_propiedades` (`titulo`, `keywords`, `descripcion1`, `calle`, `ciudad`, `colonia`, `pais`, `propietario`, `correo`, `telefono`, `inmueble`, `operacion`, `imagenes`, `descripcion`, `id`, `direccion`, `valor`, `terreno`, `construccion`, `antiguedad`, `recamaras`, `banios`, `estacionamiento`, `niveles`, `cocina`, `family_room`, `sala`, `comedor`, `jardin`, `vestidor`, `cuarto_lavado`, `estudio`, `roof_garden`, `cuarto_servicio`, `otro`, `home`, `casa_nueva`, `destacada`) VALUES
('Inmobiliaria, casas, departamentos, bodegas, naves industria', 'casas en renta, casas en venta, departamentos en renta, departamentos en ventas, bodegas en venta, bodegas en renta, naves industriales en venta y renta, locales comerciales en venta y renta , queretaro', 'Inmobiliaria Grupo Cob, casas, departamentos, locales, bodegas y naves industriales en venta y renta en QuerÃ©taro', '', '', '', '', '', '', '0', '', '', '', '', 1, '', 0, '0', '0', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 0, 0, 0),
('Casa en venta en San Francisco Juriquilla Querétaro', 'casa en venta, juriquilla, queretaro, san francisco', 'Venta de casa nueva en Juriquilla San Francisco Querétaro de entrega inmediata', 'San Lorenzo No. 104', 'Querétaro', 'San Francisco Juriquilla', 'México', 'Grupo Cob', 'ventas@grupocob.com.mx', '', 'Casa', 'Venta', '					<li class="imagen"><img src="../images/propiedades/juriquilla2.jpg" alt="juriquilla2.jpg"><span class="elim">Eliminar</span></li><li class="imagen"><img src="../images/propiedades/juriquilla1.jpg" alt="juriquilla1.jpg"><span class="elim">Eliminar</span></li><li class="imagen"><img src="../images/propiedades/juriquilla3.JPG" alt="juriquilla3.JPG"><span class="elim">Eliminar</span></li><li class="imagen"><img src="../images/propiedades/la foto (2).JPG" alt="la foto (2).JPG"><span class="elim">Eliminar</span></li><li class="imagen"><img src="../images/propiedades/la foto 1.JPG" alt="la foto 1.JPG"><span class="elim">Eliminar</span></li><li class="imagen"><img src="../images/propiedades/la foto 2.JPG" alt="la foto 2.JPG"><span class="elim">Eliminar</span></li><li class="imagen"><img src="../images/propiedades/la foto 3.JPG" alt="la foto 3.JPG"><span class="elim">Eliminar</span></li><li class="imagen"><img src="../images/propiedades/la foto 4.JPG" alt="la foto 4.JPG"><span class="elim">Eliminar</span></li><li class="imagen"><img src="../images/propiedades/la foto 5.JPG" alt="la foto 5.JPG"><span class="elim">Eliminar</span></li>', 'Casa Nueva en San Francisco Juriquilla, con excelente ubicación, acabados de primera. Cerca de centros comerciales, escuelas, avenidas.          <br/>Sistema de riego automático, sistema hidroneumático de presión constante, etc.', 2, '<iframe src="https://www.google.com/maps/embed?pb=!1m27!1m12!1m3!1d1866.111683400181!2d-100.45111128412132!3d20.70115234667841!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!4m12!1i0!3e2!4m5!1s0x85d35742d1a84c4f%3A0x7120ab9a0f1b9135!2sSan+Lorenzo%2C+Juriquilla%2C+Santiago+de+Quer%C3%A9taro%2C+QRO!3m2!1d20.701072699999997!2d-100.4503281!4m3!3m2!1d20.7010298!2d-100.4504714!5e0!3m2!1ses-419!2smx!4v1400614208407" width="600" height="450" frameborder="0" style="border:0"></iframe>', 5600000, '650', '362', '', 3, 3.5, 4, 3, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, '', 1, 1, 1),
('Casa ', 'Casa, Queretaro, venta, renta, refugio, oportunidad', 'Casa nueva con diseño modernista ', 'Cuatro caminos 110', 'Querétaro', 'El Refugio', 'México', 'Grupo Cob', 'ventas@grupocob.com.mx', '4421560853', 'Casa', 'Venta', '					<li class="imagen"><img alt="IMG00264-20120528-1951.jpg" src="../images/propiedades/IMG00264-20120528-1951.jpg"><span class="elim">Eliminar</span></li><li class="imagen"><img alt="IMG00266-20120528-1952.jpg" src="../images/propiedades/IMG00266-20120528-1952.jpg"><span class="elim">Eliminar</span></li><li class="imagen"><img alt="IMG00270-20120528-1954.jpg" src="../images/propiedades/IMG00270-20120528-1954.jpg"><span class="elim">Eliminar</span></li><li class="imagen"><img alt="IMG00269-20120528-1954.jpg" src="../images/propiedades/IMG00269-20120528-1954.jpg"><span class="elim">Eliminar</span></li><li class="imagen"><img alt="IMG00284-20120528-2001.jpg" src="../images/propiedades/IMG00284-20120528-2001.jpg"><span class="elim">Eliminar</span></li>', 'Casa con diseño modernista, espacios amplios, nueva', 23, '<iframe src="https://www.google.com/maps/embed?pb=!1m10!1m8!1m3!1d1866.7673782727625!2d-100.3483646!3d20.6478121!3m2!1i1024!2i768!4f13.1!5e0!3m2!1ses!2s!4v1402673801727" width="600" height="450" frameborder="0" style="border:0"></iframe>', 1600000, '128', '132', '', 3, 2.5, 2, 2, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, '', 1, 1, 1),
('Local comercial', 'Bernardo Quintana, Local, comercial, Ubicación, Excelentes acabados, Querétaro, Mejor zona', 'Local comercial con Excelente ubicación', 'Blvd. Bernardo Quintana 113', 'Queretaro', 'Bernardo Quintana', 'Mexico', 'Grupo Cob', 'ventas@grupocob.com.mx', '4421560853', 'Local', 'Renta', '					<li class="imagen"><img alt="11.jpg" src="../images/propiedades/11.jpg"><span class="elim">Eliminar</span></li><li class="imagen"><img alt="4.jpg" src="../images/propiedades/4.jpg"><span class="elim">Eliminar</span></li>', 'Local Comercial en Excelente Ubicación, sobre Bernardo Quintana', 24, '<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d1867.1451299084003!2d-100.3901209!3d20.6170224!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x3898324f7f36721d!2sPlaza+Centrum!5e0!3m2!1ses!2s!4v1402673745827" width="600" height="450" frameborder="0" style="border:0"></iframe>', 25000, '', '', '', 0, 2, 1, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 1, 0, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `inmobiliaria_tab_usr`
--

CREATE TABLE IF NOT EXISTS `inmobiliaria_tab_usr` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `usr` varchar(20) NOT NULL,
  `pass` varchar(50) NOT NULL,
  `tipo` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Volcado de datos para la tabla `inmobiliaria_tab_usr`
--

INSERT INTO `inmobiliaria_tab_usr` (`id`, `usr`, `pass`, `tipo`) VALUES
(1, 'cobpropiedades', '2e2679cc97ab00358cdadf9456cd2bfc', 1);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
