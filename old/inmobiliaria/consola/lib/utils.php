<?php 
/*	Elimina caracteres para editor tinymce
 *	
 *
 *
 *
 *
 */
function elimP($val){
	$val=str_replace("<p>","",$val);
	$val=str_replace("</p>","",$val);
	$val=str_replace("Â","",$val);
	$val=str_replace("º","&deg;",$val);
	$val=str_replace(";&Atilde;",";",$val);
	$val=str_replace("&Acirc;","",$val);
	return $val;
}
/*	Combierte los caracteres a codigo HTML ascii
 *	
 *
 *
 *
 *
 */
function limpiar_caracter_esp($val){
	$val=utf8_decode($val);
	$val=str_replace(";&Atilde;",";",$val);
	$val=str_replace("&Acirc;","",$val);
	$val=str_replace("á","&aacute;",$val);
	$val=str_replace("é","&eacute;",$val);
	$val=str_replace("º","&deg;",$val);
	$val=str_replace("í","&iacute;",$val);
	$val=str_replace("ó","&oacute;",$val);
	$val=str_replace("ú","&uacute;",$val);
	$val=str_replace("Á","&Aacute;",$val);
	$val=str_replace("É","&Eacute;",$val);
	$val=str_replace("Í","&Iacute;",$val);
	$val=str_replace("Ó","&Oacute;",$val);
	$val=str_replace("Ú","&Uacute;",$val);
	$val=str_replace("Ñ","&Ntilde;",$val);
	$val=str_replace("ñ","&ntilde;",$val);
	$val=str_replace("¿","&iquest;",$val);
	$val=str_replace("®","&reg;",$val);
	$val=str_replace("¡","&ntilde;",$val);
	$val=str_replace("'","&#39;",$val);
	$val=str_replace("€'","&euro;",$val);
	return $val;
}
/*	Elimina Simbolos Raroz
 *	
 *
 *
 *
 *
 */
function limpiar_caracter($val){
	$val=strtolower($val);
	$val=str_replace(";&Atilde;","",$val);
	$val=str_replace("&Acirc;","",$val);
	$val=str_replace("á","a",$val);
	$val=str_replace("º","&deg;",$val);
	$val=str_replace("é","e",$val);
	$val=str_replace("í","i",$val);
	$val=str_replace("ó","o",$val);
	$val=str_replace("ú","u",$val);
	$val=str_replace("Á","A",$val);
	$val=str_replace("É","E",$val);
	$val=str_replace("Í","I",$val);
	$val=str_replace("Ó","O",$val);
	$val=str_replace("Ú","U",$val);
	$val=str_replace("Ñ","N",$val);
	$val=str_replace("ñ","n",$val);
	$val=str_replace("¿","",$val);
	$val=str_replace("¡","",$val);
	$val=str_replace("?","",$val);
	$val=str_replace("!","",$val);
	$val=str_replace("'","",$val);
	$val=str_replace("€'","",$val);
	$val=str_replace("%","",$val);
	$val=str_replace("®","&reg;",$val);
	$val=str_replace("/'","",$val);
	$val=str_replace("\'","",$val);
	$val=str_replace("|'","",$val);
	$val=str_replace("·'","",$val);
	$val=str_replace("$'","",$val);
	$val=str_replace("('","",$val);
	$val=str_replace(")'","",$val);
	$val=str_replace("='","",$val);
	$val=str_replace("?'","",$val);
	$val=str_replace("¿'","",$val);
	$val=str_replace("¡'","",$val);
	$val=str_replace("!'","",$val);
	return $val;
}
/*	limpia una frace para convertirla en una url limpia 
 *	(No soporta parametros)

 */
function limpiar_url($val){
	$val=strtolower($val);
	$val=str_replace(" ","-",$val);	
	$val=str_replace("/","",$val);
	$val=str_replace("º","",$val);
	$val=str_replace("ª","",$val);
	$val=str_replace('"',"",$val);
	$val=str_replace('&deg;',"",$val);
	$val=str_replace('"',"",$val);
	$val=str_replace(".","",$val);
	$val=str_replace(",","",$val);
	$val=str_replace('&amp;"',"",$val);
	$val=str_replace('&"',"",$val);
	$val=str_replace("\\","",$val);
	$val=str_replace("á","a",$val);
	$val=str_replace("é","e",$val);
	$val=str_replace("í","i",$val);
	$val=str_replace("ó","o",$val);
	$val=str_replace("ú","u",$val);
	$val=str_replace("ñ","n",$val);
	$val=str_replace("&aacute;","a",$val);
	$val=str_replace("&eacute;","e",$val);
	$val=str_replace("&iacute;","i",$val);
	$val=str_replace("&oacute;","o",$val);
	$val=str_replace("&uacute;","u",$val);
	$val=str_replace("&ntilde;","n",$val);
	
	$val=str_replace("¿","",$val);
	$val=str_replace("&reg;","",$val);
	$val=str_replace("®","",$val);
	$val=str_replace("¡","",$val);
	$val=str_replace("?","",$val);
	$val=str_replace("!","",$val);
	$val=str_replace("'","",$val);
	$val=str_replace("€'","",$val);
	$val=str_replace("--","-",$val);
	$val=str_replace("-.php",".php",$val);
	return $val;}
/*	Desplegar option con las categorias en un idioma definido
 *	
 *
 *
 *
 *
 
	function  lista_cat_edit($id,$lv){						
		$simb="";
		switch($lv){
			case '1':{$simb="-";break;}
			case '2':{$simb="&nbsp;&nbsp;--";break;}
			case '3':{$simb="&nbsp;&nbsp;&nbsp;---";break;}
			case '4':{$simb="&nbsp;&nbsp;&nbsp;&nbsp;---";break;}
			default:{$simb="*";break;}
		}
		$lv++;
		$dbinfo = mysql_query("SELECT * FROM `cas_tab_categorias` WHERE id_padre=".$id." AND idioma='".$_SESSION['lg']."' ");
		while ($variables=mysql_fetch_array($dbinfo)){
			echo '<option class="opn'.$variables["id_categoria"].'"  value="'.$variables["id_categoria"].'">'.$simb.utf8_encode($variables["categoria"]).'</option>';
			lista_cat_edit($variables["id_categoria"],$lv);
		}						
	}*/
/*	Desplegar menu con las categorias en un idioma definido
 *	
 *
 *
 *
 *

	function  lista_cat($id,$idioma,$compara){
	$dir_base2="http://www.posicionart.com/demos/c&a/";
		$cadena;
		$dbinfo = mysql_query("SELECT * FROM `cas_tab_categorias` WHERE id_padre=".$id." AND idioma='".$idioma."' ");
		while ($variables=mysql_fetch_array($dbinfo)){
			if($compara==utf8_encode($variables["categoria"])) 
				$cadena.= '<li class="sel"><a href="'.$dir_base2.limpiar_url($variables["categoria"]).'.php"><span>'.utf8_encode($variables["categoria"]).'</span></a>';
			else
				$cadena.= '<li><a href="'.$dir_base2.limpiar_url($variables["categoria"]).'.php"><span>'.utf8_encode($variables["categoria"]).'</span></a>';
			$cadena.= str_replace("<ul></ul>","","<ul>".lista_cat($variables["id_categoria"],$idioma,$compara)."</ul>");
			$cadena.="</li>";
		}
		return $cadena;
	} */
?>