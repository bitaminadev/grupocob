<?php //session_start();  if(!isset($_SESSION['usr'])){header("Location:login.php");} 
	?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<script type="text/javascript">
		  WebFontConfig = {
			google: { families: [ 'Open+Sans:400,300,600,800,700:latin'] }
		  };
		  (function() {
			var wf = document.createElement('script');
			wf.src = ('https:' == document.location.protocol ? 'https' : 'http') +
			  '://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js';
			wf.type = 'text/javascript';
			wf.async = 'true';
			var s = document.getElementsByTagName('script')[0];
			s.parentNode.insertBefore(wf, s);
		  })(); 
		</script>
		<link rel="icon" href="../favicon.ico" type="image/x-icon" />
		
		<link href="css/skin.css" rel="stylesheet" type="text/css" />
		<?php if(isset($cabeza))  echo $cabeza;  ?>
	</head>
	<body>
		<div id="cabezaConsola">
			<div class="contenidoConsola">
				<a href="index.php" class="logo"><img src="../images/logo_grupocob.png" alt="Grupo COB - Inmobiliaria"/>Propiedades e inmuebles en venta y renta</a>
			</div>
			<div class="cabezaInf">
				<p class="bien">Bienvenido <strong><?php echo $_SESSION['nombre'];?></strong></p>
				<ul class="menuConsola">
					<li><a href="logout.php">Salir</a></li>
					<?php 
						$urlInd=$_SERVER["REQUEST_URI"];
						if(!strpos($urlInd,"index.php")){
					?>
					<li><a href="javascript:window.history.back();">Regresar</a></li>
					<li><a href="index.php">Inicio</a></li>
					<?php } ?>
				</ul>
			</div>
		</div>
		<div id="cuerpoConsola"  class="editar">	
			<div class="contenidoConsola">