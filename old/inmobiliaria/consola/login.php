<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="ES-es">
<head>
		<script type="text/javascript">
		  WebFontConfig = {
			google: { families: [ 'Open+Sans:400,600,200:latin' ] }
		  };
		  (function() {
			var wf = document.createElement('script');
			wf.src = ('https:' == document.location.protocol ? 'https' : 'http') +
			  '://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js';
			wf.type = 'text/javascript';
			wf.async = 'true';
			var s = document.getElementsByTagName('script')[0];
			s.parentNode.insertBefore(wf, s);
		  })(); </script>
	<title>Consola</title>
	<link rel="icon" href="../favicon.ico" type="image/x-icon" />
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<link href="css/login.css" rel="stylesheet" type="text/css" />
	<script type="text/javascript" src="js/jquery-1.10.1.js"></script>
	<script type="text/javascript">
		var bh,wh,ww;
		$(function(){
		redim();});		
		function redim(){
			bh = $("body").height();
			wh = $(window).height();
			ww = $(window).width();			
			if(wh < bh)
				$("body > .pie").css({"bottom":"-30%"});
			else
				$("body > .pie").css({"bottom":"0"});			
			$(".video-background").css({"width":ww, "height":wh});
			if(ww>=wh)
				$("video").css({"width": "150%"});
			if(wh>=ww)
				$("video").css({"width": "100%"});
			setTimeout("redim()",1000);
		}
		</script>
</head>
<body>
	<div class="video-background">
		<!--<video id="clip_html5_api" class="vjs-tech" loop="" data-setup="{}" src="bokeh2.mp4" autoplay></video>-->
		<video id="clip_html5_api" class="vjs-tech" loop data-setup="{}" src="http://static-p.iuqo.com/media/home/home/video/cloud.mp4" preload="true" autoplay></video>
	</div>
	<div class="logo">
		<img src="../images/logo_grupocob.png" alt=" "/>
	</div>
	<?php if(isset($_GET['error'])&&$_GET['error']==1)
	echo "<p class='error' style='color:red;'>Usuario o contrase&ntilde;a no validos</p>";?>
	<div class="centro">
		<form method="post" class="formularios" action="lib/validar-user.php">
			<img src="images/trans_lateral.png" alt="trans_lateral"/>
			<div class="marco">
				<div class="renglon1"> <input type="text" name="usuario" placeholder="USUARIO"/></div>
				<div class="renglon2"> <input type="password" name="password" placeholder="CONTRASE&Ntilde;A"/></div>
				<input type="submit" value="Entrar"  class="sub"/>
			</div>
			<img src="images/trans_lateral.png" alt="trans_lateral"/>
		</form>
	</div>
	<p class="pie"><img src="images/acceso_logoP.png"/></p>
	</body>
</html>