<?php
//ruta a la carpeta, '.' es carpeta actual
$path="../";
$no_mostrar=Array("",".php");
$dir_handle = @opendir($path) or die("No se pudo abrir $path");
$salida= '<?xml version="1.0" encoding="UTF-8"?>
<urlset
      xmlns="http://www.sitemaps.org/schemas/sitemap/0.9"
      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
      xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd">
<url>
	<loc>http://inmobiliaria.grupocob.com.mx/</loc>
	<priority>1.00</priority>
</url>
<url>
	<loc>http://inmobiliaria.grupocob.com.mx/index.php</loc>
	<priority>0.80</priority>
</url>
<url>
	<loc>http://inmobiliaria.grupocob.com.mx/buscar.php</loc>
	<priority>0.60</priority>
</url>
<url>
	<loc>http://inmobiliaria.grupocob.com.mx/contacto.htm</loc>
	<priority>0.60</priority>
</url>

';
while ($file = readdir($dir_handle)) {
	$pos=strrpos($file,".");
	$extension=substr($file,$pos);
	if (in_array($extension, $no_mostrar)) { 
		if($file!="cabeza.php"&&$file!="buscar.php"&&$file!="index.php"&&$file!="correo-no-enviado.htm"&&$file!="correo-enviado.htm"&&$file!="correo.php"&&$file!="formulario.htm"&&$file!="sitemapGen.php"&&$file!="pie.php"&&$file!="propiedad.php"){
			$salida.= "
<url>
	<loc>http://inmobiliaria.grupocob.com.mx/$file</loc>
	<priority>0.50</priority>
</url>";
		}
	} 
}
$salida.="
</urlset>";
closedir($dir_handle);
$fp=fopen("../sitemap.xml","w+" );
fwrite($fp,$salida);
fclose($fp);
	
?>
