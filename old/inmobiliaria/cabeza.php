<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtm1/DTD/xhtm1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head><?php 
	if(isset($titulo)&&$titulo!="")
		echo $titulo;
	if(isset($description)&&$description!="")
		echo $description;
	if(isset($keywords)&&$keywords!="")
		echo $keywords; ?>	
	<link href="favicon.ico" rel="shortcut icon" type="image/x-icon" /> <link href="favicon.ico" rel="icon" type="image/x-icon" />	
	<meta content="IE=EmulateIE7" http-equiv="X-UA-Compatible" />
	<meta http-equiv="Content-Type" content="text/htm; charset=ISO-8859-1" />
	<link href="css/skin.css" rel="stylesheet" type="text/css" />
	<?php if(isset($header)) echo $header;?>
	<!--[IF IE 7 ]>
		<link href="css/skinIE7.css" rel="stylesheet" type="text/css" />
	<![endif]-->
	<!-- Codigo URCHIN -->
	<script type="text/javascript">
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

	  ga('create', 'UA-37890734-3', 'grupocob.com.mx');
	  ga('send', 'pageview');
	</script>
	<!-- Codigo URCHIN -->
</head>
<body>	
	<div id="head">
		<div id="cabeza">
			<h1><a href="index.php" class="logo"><img src="images/logo_grupocob.png" alt="Grupo COB"/></a></h1>
			<p class="nombre">Propiedades e inmuebles en venta y renta</p>
			<p class="tel">Grupo Cobertura S. de R.L de C.V. <span>Tel: (442) 260 9090</span></p>	
		</div>
		<div id="menu1">
			<ul class="menu">
				<li class="m1"><a href="http://www.grupocob.com.mx/" target="_blank"><span>Grupo COB</span></a></li>
				<li class="m2"><a href="buscar.php"><span>Propiedades</span></a></li>
				<li class="m3"><a href="contacto.htm"><span>Contacto</span></a></li>
			</ul>
		</div>
	</div>
    <div id="cuerpo">