$(document).ready(
function (){
	$("#pikame").PikaChoose({
		autoPlay: false,
		speed: 5000,
		carousel:true,
		text: { play: "", stop: "", previous: "<< Anterior", next: "Siguiente >>", loading: "Cargandp" },transition:[1],
		showCaption: true,
		IESafe: false,
		showTooltips: false,						
		swipe: true,
		carouselVertical: false,
		animationFinished: null,
		buildFinished: null,
		bindsFinished: null,
		startOn: 0,
		thumbOpacity: 0.4,
		hoverPause: false,
		animationSpeed: 600,
		fadeThumbsIn: false,
		carouselOptions: {},
		thumbChangeEvent: "click.pikachoose",
		stopOnClick: true,
		hideThumbnails: false});
});