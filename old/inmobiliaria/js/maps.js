//inicializar mapa
        function initialize() {
			//colocar puntos
            var latlng = new google.maps.LatLng(PuntoCentralX,PuntoCentralY);
            var settings = {
                zoom: MapaZoom,
                center: latlng,
                mapTypeControl: true,
                mapTypeControlOptions: {style: google.maps.MapTypeControlStyle.DROPDOWN_MENU},
                navigationControl: true,
				scrollwheel:false,
                navigationControlOptions: {style: google.maps.NavigationControlStyle.SMALL},
                mapTypeId: google.maps.MapTypeId.ROADMAP}; //SATELLITE HYBRID ROADMAP TERRAIN (Tipos de mapa)
				var map = new google.maps.Map(document.getElementById("map_canvas"), settings); 

			var companyLogo = new google.maps.MarkerImage('images/marca.png',
				new google.maps.Size(30,30),
				new google.maps.Point(0,0),
				new google.maps.Point(0,0)
			);
/*				var companyShadow = new google.maps.MarkerImage('images/icono_sombra.png',
				new google.maps.Size(100,50),
				new google.maps.Point(0,0),
				new google.maps.Point(50, 50)
			);*/
			
			companyPos=new Array ();
			companyMarker=new Array ();
			
			
			for(x=0;x<CiclosMapa;x++){
				var contentString =
					'<div class="ubicacion">'+
						'<p id="Titulo'+x+'" class="titulo_Campus">'+MatrizMapa[x][2]+'</p>'+
						'<div id="Contenido'+x+'">'+	
							'<p>'+MatrizMapa[x][3]+'<br/>'+MatrizMapa[x][4]+'</p>'+
						'</div>'+
					'</div>';
				companyPos[x] = new google.maps.LatLng(MatrizMapa[x][0],MatrizMapa[x][1]);
				companyMarker[x] = new google.maps.Marker({
					position: companyPos[x],
					map: map,
					icon: companyLogo,
//					shadow: companyShadow,
					title:MatrizMapa[x][2],
					html:contentString
					});			
				
					infowindow = new google.maps.InfoWindow({
                content: "Cargando..."
            });
				var contenidomarca=companyMarker[x];
				google.maps.event.addListener(contenidomarca, 'click', function() {
					infowindow.setContent(this.html);
					infowindow.open(map,this);
				});
			}

      }