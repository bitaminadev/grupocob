<?php
/**
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       Cake.Console.Templates.default.views
 * @since         CakePHP(tm) v 1.2.0.5234
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */
?>
<div class="row-fluid sortable">
	<div class="box span12">
    	<div class="box-header" data-original-title>
            <h2><i class="halflings-icon user"></i><span class="break"></span><?php echo "<?php echo __('{$pluralHumanName}'); ?>"; ?></h2>
            <div class="box-icon">
                 <i class="icon-plus-sign"></i>
					<?php echo "<?php echo \$this->Html->link(__('New " . $singularHumanName . "'), array('action' => 'add')); ?>"; ?>
                <a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
            </div>
        </div>
        <div class="box-content">
        	<table class="table table-striped table-bordered bootstrap-datatable datatable">
            	<thead>
                    <tr>
                    <?php  foreach ($fields as $field): ?>
                        <th><?php echo "<?php echo \$this->Paginator->sort('{$field}'); ?>"; ?></th>
                    <?php endforeach; ?>
                        <th class="actions"><?php echo "<?php echo __('Actions'); ?>"; ?></th>
                    </tr>
                </thead>
                <tbody>
                	<?php
						echo "<?php
						foreach (\${$pluralVar} as \${$singularVar}): ?>\n";
						echo "\t<tr>\n";
							foreach ($fields as $field) {
								$isKey = false;
								if (!empty($associations['belongsTo'])) {
									foreach ($associations['belongsTo'] as $alias => $details) {
										if ($field === $details['foreignKey']) {
											$isKey = true;
											echo "\t\t<td>\n\t\t\t<?php echo \$this->Html->link(\${$singularVar}['{$alias}']['{$details['displayField']}'], array('controller' => '{$details['controller']}', 'action' => 'view', \${$singularVar}['{$alias}']['{$details['primaryKey']}'])); ?>\n\t\t</td>\n";
											break;
										}
									}
								}
								if ($isKey !== true) {
									echo "\t\t<td><?php echo h(\${$singularVar}['{$modelClass}']['{$field}']); ?>&nbsp;</td>\n";
								}
							}
					
							echo "\t\t<td class=\"actions\">\n";
							echo "\t\t\t<?php echo \$this->Html->link(__(".'"'."<i class='halflings-icon white zoom-in'></i>".'"'."), array('action' => 'view', \${$singularVar}['{$modelClass}']['{$primaryKey}']),array( 'class'=>'btn btn-success', 'escape'=>false)); ?>\n";
							
							echo "\t\t\t<?php echo \$this->Html->link(__(".'"'."<i class='halflings-icon white edit'></i>".'"'."), array('action' => 'edit', \${$singularVar}['{$modelClass}']['{$primaryKey}']),array('class'=>'btn btn-info','escape'=>false)); ?>\n";
							
							echo "\t\t\t<?php echo \$this->Form->postLink(__(".'"'."<i class='halflings-icon white trash'></i>".'"'."), array('action' => 'delete', \${$singularVar}['{$modelClass}']['{$primaryKey}']),array( 'class'=>'btn btn-danger', 'escape'=>false), __('Are you sure you want to delete # %s?', \${$singularVar}['{$modelClass}']['{$primaryKey}'])); ?>\n";
							
							echo "\t\t</td>\n";
						echo "\t</tr>\n";
					
						echo "<?php endforeach; ?>\n";
					?>
                       
                </tbody>
            </table>
        </div>
    </div>
</div>