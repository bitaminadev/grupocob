<?php
/**
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       Cake.Console.Templates.default.views
 * @since         CakePHP(tm) v 1.2.0.5234
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */
?>
<div class="row-fluid sortable">		
	<div class="box span12">
    	<div class="box-header" data-original-title>
        	<h2><i class="icon-bar-chart"></i><span class="break"></span><?php echo "Viendo Datos"; ?></h2>
            <div class="box-icon">
               <a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
            </div>
         </div>
         <div class="box-content">
         	<div class="tabbable">
            	<ul class="nav nav-tabs">
                    <li class="active"><a href="#tab1" data-toggle="tab"><?php echo "<?php  echo __('{$singularHumanName}'); ?>"; ?></a></li>
                </ul>
            </div>
            <div class="tab-content">
                <div class="tab-pane active" id="tab1">
                	<p>
                    	<dl>
							<?php
                            foreach ($fields as $field) {
                                $isKey = false;
                                if (!empty($associations['belongsTo'])) {
                                    foreach ($associations['belongsTo'] as $alias => $details) {
                                        if ($field === $details['foreignKey']) {
                                            $isKey = true;
                                            echo "\t\t<dt><?php echo __('" . Inflector::humanize(Inflector::underscore($alias)) . "'); ?></dt>\n";
                                            echo "\t\t<dd>\n\t\t\t<?php echo \$this->Html->link(\${$singularVar}['{$alias}']['{$details['displayField']}'], array('controller' => '{$details['controller']}', 'action' => 'view', \${$singularVar}['{$alias}']['{$details['primaryKey']}'])); ?>\n\t\t\t&nbsp;\n\t\t</dd>\n";
                                            break;
                                        }
                                    }
                                }
                                if ($isKey !== true) {
                                    echo "\t\t<dt><?php echo __('" . Inflector::humanize($field) . "'); ?></dt>\n";
                                    echo "\t\t<dd>\n\t\t\t<?php echo h(\${$singularVar}['{$modelClass}']['{$field}']); ?>\n\t\t\t&nbsp;\n\t\t</dd>\n";
                                }
                            }
                            ?>
                        </dl>
                  	</p>
               	</div>
           	</div> 
      	</div>  
    </div>
</div>
<div class="row-fluid">	
    <div class="box blue span12">
        <div class="box-header">
            <h2><i class="halflings-icon hand-top"></i><span class="break"></span>Acciones</h2>
        </div>
        <div class="box-content">
        	<?php echo "<?php echo \$this->Html->link('<i class=".'"'."icon-undo".'"'."></i><p>Regresar</p>','#', array('class'=>'quick-button span2 back','escape'=>false)); ?>"; ?>
            <?php echo "\t\t<?php echo \$this->Html->link(__('<i class=".'"'."icon-edit".'"'."></i><p>Editar</p>'), array('action' => 'edit', \${$singularVar}['{$modelClass}']['{$primaryKey}']),array('class'=>'quick-button span2','escape'=>false)); ?>\n";?>
			<?php //echo $this->Html->link(__('<i class="icon-edit"></i><p>Editar</p>'), array('action' => 'edit', $article['Article']['id']),array('class'=>'quick-button span2','escape'=>false)); ?>
            <?php echo "\t\t<?php echo \$this->Form->postLink(__('<i class=".'"'."icon-trash".'"'."></i><p>Eliminar</p>'), array('action' => 'delete', \${$singularVar}['{$modelClass}']['{$primaryKey}']), array('class'=>'quick-button span2','escape'=>false), __('Are you sure you want to delete # %s?', \${$singularVar}['{$modelClass}']['{$primaryKey}'])); ?>\n";?>
            <?php //echo $this->Form->postLink(__('<i class="icon-trash"></i><p>Eliminar</p>'), array('action' => 'delete', $article['Article']['id']),array('class'=>'quick-button span2','escape'=>false), __('Are you sure you want to delete # %s?', $article['Article']['id']),array()); ?>
            <?php echo "\t\t<?php echo \$this->Html->link(__('<i class=".'"'."icon-plus".'"'."></i><p>Agregar".$singularHumanName."</p>'), array('action' => 'add'),array('class'=>'quick-button span2','escape'=>false)); ?>\n";?>
			
			<?php //echo $this->Html->link(__('<i class="icon-plus"></i><p>Agregar Artículo</p>'), array('action' => 'add'),array('class'=>'quick-button span2','escape'=>false)); ?>
            <div class="clearfix"></div>
        </div>	
    </div>  
</div>
