<?php
/**
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       Cake.Console.Templates.default.views
 * @since         CakePHP(tm) v 1.2.0.5234
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */
?>
<div class="row-fluid sortable">		
	<div class="box span12">
    	<div class="box-header" data-original-title>
            <h2><i class="halflings-icon edit"></i><span class="break"></span><?php printf("<?php echo __('%s %s'); ?>", Inflector::humanize($action), $singularHumanName); ?></h2>
           	<div class="box-icon">
                <a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
            </div>
        </div>
        <div class="box-content">
        	<?php echo "<?php echo \$this->Form->create('{$modelClass}'); ?>\n"; ?>
            <fieldset>
            	<div class="control-group">
                    <div class="span6">
                        <legend>Datos Generales</legend>
                        <?php
							echo "\t<?php\n";
							foreach ($fields as $field) {
								if (strpos($action, 'add') !== false && $field == $primaryKey) {
									continue;
								} elseif (!in_array($field, array('created', 'modified', 'updated'))) {
									echo "\t\techo \$this->Form->input('{$field}');\n";
								}
							}
							if (!empty($associations['hasAndBelongsToMany'])) {
								foreach ($associations['hasAndBelongsToMany'] as $assocName => $assocData) {
									echo "\t\techo \$this->Form->input('{$assocName}');\n";
								}
							}
							echo "\t?>\n";
						?>
                    </div>
        		</div>
          	</fieldset>
            <div class="form-actions">
            	<?php
					echo "<?php echo \$this->Form->end(array('label'=>'Guardar','class'=>'btn btn-primary','div'=>false)); ?>\n";
				?>
            	<button type="button" class="btn goBack back">Cancelar</button>
            </div 
    	</div>
	</div>
</div>
<div class="row-fluid">	
    <div class="box blue span12">
        <div class="box-header">
            <h2><i class="halflings-icon hand-top"></i><span class="break"></span>Acciones</h2>
        </div>
        <div class="box-content">
     		<?php echo "<?php echo \$this->Html->link('<i class=".'"'."icon-undo".'"'."></i><p>Regresar</p>','#', array('class'=>'quick-button span2 back','escape'=>false)); ?>"; ?>
            <?php //echo $this->Html->link('<i class="icon-undo"></i><p>Regresar</p>','#',array('class'=>'quick-button span2 back','escape'=>false)); ?>
            
			<?php //echo $this->Html->link(__('<i class="icon-edit"></i><p>Editar</p>'), array('action' => 'edit', $article['Article']['id']),array('class'=>'quick-button span2','escape'=>false)); ?>
            <?php if (strpos($action, 'add') === false): ?>
            <?php //echo $this->Form->postLink(__('<i class="icon-trash"></i><p>Eliminar</p>'), array('action' => 'delete', $this->data['Article']['id']),array('class'=>'quick-button span2','escape'=>false), __('Are you sure you want to delete # %s?', $this->data['Article']['id']),array()); ?>
		
        	<?php echo "<?php echo \$this->Form->postLink(__('<i class=".'"'."icon-trash".'"'."></i><p>Eliminar</p>'), array('action' => 'delete', \$this->Form->value('{$modelClass}.{$primaryKey}')), array('class'=>'quick-button span2','escape'=>false), __('Are you sure you want to delete # %s?', \$this->Form->value('{$modelClass}.{$primaryKey}'))); ?>"; ?>
            <?php //echo $this->Html->link(__('<i class="icon-plus"></i><p>Agregar Artículo</p>'), array('action' => 'add'),array('class'=>'quick-button span2','escape'=>false)); ?>
			<?php echo "<?php echo \$this->Html->link('<i class=".'"'."icon-plus".'"'."></i><p>Agregar ". $pluralHumanName . "</p>', array('action' => 'index'),array('class'=>'quick-button span2','escape'=>false)); ?>"; ?>
			
			<?php endif; ?>
            
            
            <div class="clearfix"></div>
        </div>	
    </div>  
</div>