<div class="row-fluid sortable">
	<div class="box span12">
    	<div class="box-header" data-original-title>
            <h2><i class="halflings-icon user"></i><span class="break"></span><?php echo __('Contractors'); ?></h2>
            <div class="box-icon">
                 <i class="icon-plus-sign"></i>
					<?php echo $this->Html->link(__('New Contractor'), array('action' => 'add')); ?>                <a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
            </div>
        </div>
        <div class="box-content">
        	<table class="table table-striped table-bordered bootstrap-datatable datatable">
            	<thead>
                    <tr>
                                            <th><?php echo $this->Paginator->sort('id'); ?></th>
                                            <th><?php echo $this->Paginator->sort('name'); ?></th>
                                            <th><?php echo $this->Paginator->sort('telefono'); ?></th>
                                            <th><?php echo $this->Paginator->sort('email'); ?></th>
                                            <th class="actions"><?php echo __('Actions'); ?></th>
                    </tr>
                </thead>
                <tbody>
                	<?php
						foreach ($contractors as $contractor): ?>
	<tr>
		<td><?php echo h($contractor['Contractor']['id']); ?>&nbsp;</td>
		<td><?php echo h($contractor['Contractor']['name']); ?>&nbsp;</td>
		<td><?php echo h($contractor['Contractor']['telefono']); ?>&nbsp;</td>
		<td><?php echo h($contractor['Contractor']['email']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__("<i class='halflings-icon white zoom-in'></i>"), array('action' => 'view', $contractor['Contractor']['id']),array( 'class'=>'btn btn-success', 'escape'=>false)); ?>
			<?php echo $this->Html->link(__("<i class='halflings-icon white edit'></i>"), array('action' => 'edit', $contractor['Contractor']['id']),array('class'=>'btn btn-info','escape'=>false)); ?>
			<?php echo $this->Form->postLink(__("<i class='halflings-icon white trash'></i>"), array('action' => 'delete', $contractor['Contractor']['id']),array( 'class'=>'btn btn-danger', 'escape'=>false), __('Are you sure you want to delete # %s?', $contractor['Contractor']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
                       
                </tbody>
            </table>
        </div>
    </div>
</div>