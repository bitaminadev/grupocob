<div class="row-fluid sortable">		
	<div class="box span12">
    	<div class="box-header" data-original-title>
        	<h2><i class="icon-bar-chart"></i><span class="break"></span>Viendo Datos</h2>
            <div class="box-icon">
               <a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
            </div>
         </div>
         <div class="box-content">
         	<div class="tabbable">
            	<ul class="nav nav-tabs">
                    <li class="active"><a href="#tab1" data-toggle="tab"><?php  echo __('Evaluation'); ?></a></li>
                </ul>
            </div>
            <div class="tab-content">
                <div class="tab-pane active" id="tab1">
                	<p>
                    	<dl>
									<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($evaluation['Evaluation']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Atencion'); ?></dt>
		<dd>
			<?php echo h($evaluation['Evaluation']['atencion']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Precio Competitivo'); ?></dt>
		<dd>
			<?php echo h($evaluation['Evaluation']['precio_competitivo']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Tiempo Entrega'); ?></dt>
		<dd>
			<?php echo h($evaluation['Evaluation']['tiempo_entrega']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Calidad Produco'); ?></dt>
		<dd>
			<?php echo h($evaluation['Evaluation']['calidad_produco']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Order'); ?></dt>
		<dd>
			<?php echo $this->Html->link($evaluation['Order']['numero_pedido'], array('controller' => 'orders', 'action' => 'view', $evaluation['Order']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Provider'); ?></dt>
		<dd>
			<?php echo $this->Html->link($evaluation['Provider']['nombre_empresa'], array('controller' => 'providers', 'action' => 'view', $evaluation['Provider']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Confianza'); ?></dt>
		<dd>
			<?php echo h($evaluation['Evaluation']['confianza']); ?>
			&nbsp;
		</dd>
                        </dl>
                  	</p>
               	</div>
           	</div> 
      	</div>  
    </div>
</div>
<div class="row-fluid">	
    <div class="box blue span12">
        <div class="box-header">
            <h2><i class="halflings-icon hand-top"></i><span class="break"></span>Acciones</h2>
        </div>
        <div class="box-content">
        	<?php echo $this->Html->link('<i class="icon-undo"></i><p>Regresar</p>','#', array('class'=>'quick-button span2 back','escape'=>false)); ?>            		<?php echo $this->Html->link(__('<i class="icon-edit"></i><p>Editar</p>'), array('action' => 'edit', $evaluation['Evaluation']['id']),array('class'=>'quick-button span2','escape'=>false)); ?>
			            		<?php echo $this->Form->postLink(__('<i class="icon-trash"></i><p>Eliminar</p>'), array('action' => 'delete', $evaluation['Evaluation']['id']), array('class'=>'quick-button span2','escape'=>false), __('Are you sure you want to delete # %s?', $evaluation['Evaluation']['id'])); ?>
                        		<?php echo $this->Html->link(__('<i class="icon-plus"></i><p>AgregarEvaluation</p>'), array('action' => 'add'),array('class'=>'quick-button span2','escape'=>false)); ?>
			
			            <div class="clearfix"></div>
        </div>	
    </div>  
</div>
