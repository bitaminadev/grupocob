<div class="row-fluid sortable">
	<div class="box span12">
    	<div class="box-header" data-original-title>
            <h2><i class="halflings-icon user"></i><span class="break"></span><?php echo __('Evaluations'); ?></h2>
            <div class="box-icon">
                 <i class="icon-plus-sign"></i>
					<?php echo $this->Html->link(__('New Evaluation'), array('action' => 'add')); ?>                <a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
            </div>
        </div>
        <div class="box-content">
        	<table class="table table-striped table-bordered bootstrap-datatable datatable">
            	<thead>
                    <tr>
                                            <th><?php echo $this->Paginator->sort('id'); ?></th>
                                            <th><?php echo $this->Paginator->sort('atencion'); ?></th>
                                            <th><?php echo $this->Paginator->sort('precio_competitivo'); ?></th>
                                            <th><?php echo $this->Paginator->sort('tiempo_entrega'); ?></th>
                                            <th><?php echo $this->Paginator->sort('calidad_produco'); ?></th>
                                            <th><?php echo $this->Paginator->sort('order_id'); ?></th>
                                            <th><?php echo $this->Paginator->sort('provider_id'); ?></th>
                                            <th><?php echo $this->Paginator->sort('confianza'); ?></th>
                                            <th class="actions"><?php echo __('Actions'); ?></th>
                    </tr>
                </thead>
                <tbody>
                	<?php
						foreach ($evaluations as $evaluation): ?>
	<tr>
		<td><?php echo h($evaluation['Evaluation']['id']); ?>&nbsp;</td>
		<td><?php echo h($evaluation['Evaluation']['atencion']); ?>&nbsp;</td>
		<td><?php echo h($evaluation['Evaluation']['precio_competitivo']); ?>&nbsp;</td>
		<td><?php echo h($evaluation['Evaluation']['tiempo_entrega']); ?>&nbsp;</td>
		<td><?php echo h($evaluation['Evaluation']['calidad_produco']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($evaluation['Order']['numero_pedido'], array('controller' => 'orders', 'action' => 'view', $evaluation['Order']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($evaluation['Provider']['nombre_empresa'], array('controller' => 'providers', 'action' => 'view', $evaluation['Provider']['id'])); ?>
		</td>
		<td><?php echo h($evaluation['Evaluation']['confianza']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__("<i class='halflings-icon white zoom-in'></i>"), array('action' => 'view', $evaluation['Evaluation']['id']),array( 'class'=>'btn btn-success', 'escape'=>false)); ?>
			<?php echo $this->Html->link(__("<i class='halflings-icon white edit'></i>"), array('action' => 'edit', $evaluation['Evaluation']['id']),array('class'=>'btn btn-info','escape'=>false)); ?>
			<?php echo $this->Form->postLink(__("<i class='halflings-icon white trash'></i>"), array('action' => 'delete', $evaluation['Evaluation']['id']),array( 'class'=>'btn btn-danger', 'escape'=>false), __('Are you sure you want to delete # %s?', $evaluation['Evaluation']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
                       
                </tbody>
            </table>
        </div>
    </div>
</div>