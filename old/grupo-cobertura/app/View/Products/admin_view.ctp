<div class="row-fluid sortable">		
	<div class="box span12">
    	<div class="box-header" data-original-title>
        	<h2><i class="icon-bar-chart"></i><span class="break"></span>Viendo Datos</h2>
            <div class="box-icon">
               <a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
            </div>
         </div>
         <div class="box-content">
         	<div class="tabbable">
            	<ul class="nav nav-tabs">
                    <li class="active"><a href="#tab1" data-toggle="tab"><?php  echo __('Product'); ?></a></li>
                </ul>
            </div>
            <div class="tab-content">
                <div class="tab-pane active" id="tab1">
                	<p>
                    	<dl>
									<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($product['Product']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Name'); ?></dt>
		<dd>
			<?php echo h($product['Product']['name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Descripcion'); ?></dt>
		<dd>
			<?php echo h($product['Product']['descripcion']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($product['Product']['created']); ?>
			&nbsp;
		</dd>
                        </dl>
                  	</p>
               	</div>
           	</div> 
      	</div>  
    </div>
</div>
<div class="row-fluid">	
    <div class="box blue span12">
        <div class="box-header">
            <h2><i class="halflings-icon hand-top"></i><span class="break"></span>Acciones</h2>
        </div>
        <div class="box-content">
        	<?php echo $this->Html->link('<i class="icon-undo"></i><p>Regresar</p>','#', array('class'=>'quick-button span2 back','escape'=>false)); ?>            		<?php echo $this->Html->link(__('<i class="icon-edit"></i><p>Editar</p>'), array('action' => 'edit', $product['Product']['id']),array('class'=>'quick-button span2','escape'=>false)); ?>
			            		<?php echo $this->Form->postLink(__('<i class="icon-trash"></i><p>Eliminar</p>'), array('action' => 'delete', $product['Product']['id']), array('class'=>'quick-button span2','escape'=>false), __('Are you sure you want to delete # %s?', $product['Product']['id'])); ?>
                        		<?php echo $this->Html->link(__('<i class="icon-plus"></i><p>AgregarProduct</p>'), array('action' => 'add'),array('class'=>'quick-button span2','escape'=>false)); ?>
			
			            <div class="clearfix"></div>
        </div>	
    </div>  
</div>
