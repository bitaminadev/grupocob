<div class="row-fluid sortable">
	<div class="box span12">
    	<div class="box-header" data-original-title>
            <h2><i class="halflings-icon user"></i><span class="break"></span><?php echo __('Products'); ?></h2>
            <div class="box-icon">
                 <i class="icon-plus-sign"></i>
					<?php echo $this->Html->link(__('New Product'), array('action' => 'add')); ?>                <a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
            </div>
        </div>
        <div class="box-content">
        	<table class="table table-striped table-bordered bootstrap-datatable datatable">
            	<thead>
                    <tr>
                                            <th><?php echo $this->Paginator->sort('id'); ?></th>
                                            <th><?php echo $this->Paginator->sort('name'); ?></th>
                                            <th><?php echo $this->Paginator->sort('descripcion'); ?></th>
                                            <th><?php echo $this->Paginator->sort('created'); ?></th>
                                            <th class="actions"><?php echo __('Actions'); ?></th>
                    </tr>
                </thead>
                <tbody>
                	<?php
						foreach ($products as $product): ?>
	<tr>
		<td><?php echo h($product['Product']['id']); ?>&nbsp;</td>
		<td><?php echo h($product['Product']['name']); ?>&nbsp;</td>
		<td><?php echo h($product['Product']['descripcion']); ?>&nbsp;</td>
		<td><?php echo h($product['Product']['created']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__("<i class='halflings-icon white zoom-in'></i>"), array('action' => 'view', $product['Product']['id']),array( 'class'=>'btn btn-success', 'escape'=>false)); ?>
			<?php echo $this->Html->link(__("<i class='halflings-icon white edit'></i>"), array('action' => 'edit', $product['Product']['id']),array('class'=>'btn btn-info','escape'=>false)); ?>
			<?php echo $this->Form->postLink(__("<i class='halflings-icon white trash'></i>"), array('action' => 'delete', $product['Product']['id']),array( 'class'=>'btn btn-danger', 'escape'=>false), __('Are you sure you want to delete # %s?', $product['Product']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
                       
                </tbody>
            </table>
        </div>
    </div>
</div>