<?php
 $articles = $this->Session->read('Cart-short');
 $contArticles = 0;
 if(!empty($articles)){
    $contArticles = count($articles);
 }
?>
<div class="well sidebar-nav" style="width: 180px; margin-left: 20px;">
    <ul class="nav nav-list">
        
        <?php if($this->params['controller'] == 'clients' && $this->params['action'] == 'editProfile'){ $class = 'active'; }else{ $class = '';}  ?>
        <li class="<?php echo $class; ?>"><?php echo $this->Html->link('Mi perfil ('.$this->Session->read('Auth.User.name').')',array('admin'=>false,'plugin'=>null,'controller'=>'clients','action'=>'editProfile'));?></li>

        <?php if($this->params['controller'] == 'clients' && $this->params['action'] == 'changePassword'){ $class = 'active'; }else{ $class = '';}  ?>
        <li class="<?php echo $class; ?>"><?php echo $this->Html->link('Cambiar contraseña',array('admin'=>false,'plugin'=>null,'controller'=>'clients','action'=>'changePassword'));?></li>

        <?php if($this->params['controller'] == 'clients' && ($this->params['action'] == 'allOrders' || $this->params['action'] == 'viewOrder')){ $class = 'active'; }else{ $class = '';}  ?>
        <li class="<?php echo $class; ?>"><?php echo $this->Html->link('Historial pedidos',array('admin'=>false,'plugin'=>null,'controller'=>'clients','action'=>'allOrders'));?></li>
        
        <li><?php echo $this->Html->link('Cerrar sesión',array('admin'=>true,'plugin'=>null,'controller'=>'users','action'=>'logout'));?></li>  

    </ul>
    <?php
    /*
    echo $this->Html->link(
        '<strong>Carrito '.$this->Number->currency($contArticles,'',array('places'=>0)).' art&iacute;culo(s) </strong>',
        '/cart/index',
        array('id'=>'cart_details','escape' => false)
    );
     * 
     */
    ?>
</div>

<script>
$(document).ready(function(){
	$("#cart_details").button({icons:{primary: "ui-icon-cart"},text:true});
});
</script>