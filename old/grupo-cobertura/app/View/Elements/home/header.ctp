<?php ?>
<div id="logo">
	<?php echo $this->Html->link($this->Html->image('home/header_logo.png'),'/',array('escape'=>false));?>
</div>
<div id="xarxes-socials-header">
	  <?php echo $this->Html->link($this->Html->image('home/header_redes1.png'),'https://twitter.com/BmgComercial',array('escape'=>false,'class'=>'header-menu','target'=>"_blank"));?>
    <?php echo $this->Html->link($this->Html->image('home/header_redes2.png'),'https://www.facebook.com/BMG-Comercial-235303506627345/',array('escape'=>false,'class'=>'header-menu','target'=>"_blank"));?>
    <?php echo $this->Html->link($this->Html->image('home/header_redes3.png'),'https://www.instagram.com/bmgcomercial/',array('escape'=>false,'class'=>'header-menu','target'=>"_blank"));?>
    <br>
    <span class="header-datos-texto1"><a href="tel:+525553686014" class="tel-header">(55) 53.68.60.14</a><br>  <a href="mailto:info@bmgcomercial.com">info@bmgcomercial.com</a>
	</span>
</div>