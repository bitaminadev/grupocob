<?php ?>
<div class="m40"></div>
<div class="section group">
    <div class="col span_12_of_12">
        <h1>BMG COMERCIAL</h1>
    </div>
</div>
<div class="section group">
    <div class="col span_12_of_12">
        <p class="intro-home">
            Integrada por jóvenes creativos e inquietos, BMG Comercial es una empresa mexicana con años de experiencia y desarrollo y comercialización de mobiliario en madera.
        </p>
    </div>
</div>
<div class="m30"></div>
<div class="section group">
    <div class="col span_12_of_12">
        <h1>PRODUCTOS</h1>
    </div>
</div>
<?php //$lineas = $this->requestAction('store/getProyectos'); //pr($categories);?>
<div class="m30"></div>
<?php $m = count($articles);?>
<?php $i = 1;$j = 1;?>
<?php foreach ($articles as $article): ?>
    <?php if ($i == 1): ?>
        <div class="section group">
    <?php endif;?>
        <div class="col span_4_of_12 txt_center">
            <div class="conte-home-pro h2-home">
                    <?php 
                        if(!empty($article['Image'][0]['image'])){
                            
                            echo $this->Html->link(
                                $this->Html->image('/img/_articles/mediana_'.$article['Image'][0]['image']),
                                array(
                                    'controller'=>'store',
                                    'action'=>'articulo',
                                    'id' => $article['Article']['id'],
                                    'slug' => Inflector::slug($article['Article']['name']),
                                    'ext' => 'html'
                                    ),
                                array('escape' => false)
                            );

                        }else{
                            echo $this->Html->link(
                                $this->Html->image('/img/_articles/mediana.jpg',array("class"=>"")),
                                array(
                                    'admin'=>false,
                                    'plugin'=>NULL,
                                    'controller'=>'store',
                                    'action'=>'articulo',
                                    'id' => $article['Article']['id'],
                                    'slug' => Inflector::slug($article['Article']['name']),
                                    'ext' => 'html'
                                    ),
                                array('escape' => false)
                            );
                        }
                    ?>
                <div class="home_titulo">
                    <?php
                        echo $this->Html->link(
                            $article['Article']['name'],
                            array(
                                'admin'=>false,
                                'plugin'=>NULL,
                                'controller'=>'store',
                                'action'=>'articulo',
                                'id' => $article['Article']['id'],
                                'slug' => Inflector::slug($article['Article']['name']),
                                'ext' => 'html'
                                ),
                            array('escape' => false)
                        );
                    ?>
                </div>
            </div>
         </div>
   <?php if ($j == $m): ?>
        <?php echo '</div><div class="m10"></div>'; ?>
    <?php elseif ($i == 3): ?>
        </div>
        <div class="m10"></div>
        <?php $i = 1;?>
    <?php elseif ($i == 1 or $i == 2): ?>
        <?php $i++;?>
    <?php endif;?>
    <?php $j++ ?>
<?php endforeach;?>









<div class="m30"></div>
<div class="section group">
    <div class="col span_12_of_12">
        <h1>PROYECTOS</h1>
    </div>
</div>
<div class="m15"></div>