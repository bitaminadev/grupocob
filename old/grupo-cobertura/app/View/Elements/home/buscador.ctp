<?php ?>
<section id="busqueda">
  <?php
$categories = $this->requestAction('store/getCategories');
?>
<?php
	echo $this->Form->create('Article', array(
        'url' => array_merge(array('controller'=>'store','action' => 'buscador'), $this->params['pass']),
        'class' => 'form-inline form-search',
        'div' => false
    ));
?>
    <div class="section group ">
        <div class="col span_4_of_12 text-busqueda">
            <span style="font-weight: 300">BÚSQUEDA</span> AVANZADA</span>
        </div>
        <div class="col span_3_of_12">
            <?PHP echo $this->Form->input(
            'Article.category_id',
            array(
                'options'=>$categories,
                'div'=>false,
                'label'=>false,
                'empty' => 'CATEGORÍAS',
                'class' => 'sf1'
            )
        );?>
        </div>
        <div class="col span_2_of_12">
            <?php
                echo $this->Form->input(
                'filter',
                array(
                    'div'=>false,
                    'label'=>false,
                    'class' => 'sf2',
                    'placeholder' => 'PALABRA CLAVE'
                )
                );
            ?>
        </div>
        <div class="col span_2_of_12">
            <?php
                echo $this->Form->input(
                'Article.code',
                array(
                    'div'=>false,
                    'label'=>false,
                    'class' => 'sf2',
                    'placeholder' => 'CÓDIGO'
                )
                );
            ?>
        </div>
        <div class="col span_1_of_12">
        	<?php echo $this->Form->end('home/bek-b-busqueda.png'); ?>
            
        </div>
    </div>
</div>