<?php ?>
<div class="section group">
    <div class="col span_4_of_12 rights">
        <div class="contenedor-datos1">
            <h1>Siguenos</h1>
            <p>
                <a href="https://twitter.com/BmgComercial" target="_blank"><img src="/bmgcomercial/img/home/footer_redes1.png" width="67" height="67"></a>
                <a href="https://www.facebook.com/BMG-Comercial-235303506627345/" target="_blank"><img src="/bmgcomercial/img/home/footer_redes2.png" width="67" height="67"></a>
                <a href="https://www.instagram.com/bmgcomercial/" target="_blank"><img src="/bmgcomercial/img/home/footer_redes3.png" width="67" height="67"></a>
            </p>
        </div>
    </div>
    <div class="col span_4_of_12 rights">
        <div class="contenedor-datos1">
            <h1>Contáctanos</h1>
            <p><img src="/bmgcomercial/img/home/footer_1.png" width="27" height="27">(55) 53.68.60.14 <br>
            <img src="/bmgcomercial/img/home/footer_2.png" width="27" height="27">info@bmgcomercial.com </p>
            <p><a href="#">AVISO DE PRIVACIDAD</a></p>
        </div>
    </div>
    <div class="col span_4_of_12 rights">
        <div class="contenedor-datos1">
            <h1>Encuéntranos</h1>
            <p>
                <a href="https://goo.gl/maps/AjMV1BaBa452"><img src="/bmgcomercial/img/home/footer_3.png" width="31" height="31"></a>Norte 65 No. 1074-A <br>
                Colonia Industrial Vallejo, <br>
                Azcapotzalco<br>
                C.P. 02300, CDMX
            </p>
        </div>
    </div>
</div>
<div class="section group">
    <div class="col span_6_of_12 left">
        <div class="text-creditos">
            Web created by <a href="http://www.contraste.net.mx/" title="Contraste Diseño y Publicidad">Contraste Diseño y Publicidad</a></div>
        </div>
    </div>
</div>
