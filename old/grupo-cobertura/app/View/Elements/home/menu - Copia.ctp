<?php $categories = $this->requestAction('store/getCategories'); //pr($categories);?>
<?php $lines = $this->requestAction('store/getProyectos'); //pr($categories);?>

<!--<link rel="stylesheet" href="/bmgcomercial/level_menu/style.css" type="text/css" media="screen">
<link rel="stylesheet" href="/bmgcomercial/level_menu/font-awesome/css/font-awesome.css" >
<script src="/bmgcomercial/level_menu/js/menu.js" type="text/javascript"></script> 
<a id="touch-menu" class="mobile-menu" href="#"><i class="icon-reorder"></i>Menu</a>
	<nav>
    	<ul class="menu">
   			<li>
            	<?php
					echo $this->Html->link(
					'Inicio',
					'/'
				);
				?>
   			</li>
  			<li>
            	<?php
					echo $this->Html->link(
					'Nosotros',
					'/store/nosotros'
				);
				?>
           	</li>
  			<li>
            	<?php
					echo $this->Html->link(
					'Proyectos',
					array(
						'controller'=>'store',
						'action'=>'cats',
						),
					array(
						'escape' => false,
						
					)
					);
				?>
  				<ul class="sub-menu">
                	<?php foreach($lines as $lin):?>
                    	<li>
							<?php
                                echo $this->Html->link(
                                    $lin['Linea']['name'],
                                    array(
                                        //'admin'=>false,
                                        //'plugin'=>NULL,
                                        'controller'=>'store',
                                        'action'=>'proyects',
                                        'id' => $lin['Linea']['id'],
                                        'slug' => Inflector::slug($lin['Linea']['name']),
                                        'ext' => 'html'
                                        ),
                                    array(
                                        'escape' => false,
                                        
                                    )
                                );
                            ?>
                            <ul>
                            	<?php foreach($lin['Proyecto'] as $pro):?>
                                	<?php
										echo $this->Html->link(
											$pro['name'],
											array(
												'admin'=>false,
												'plugin'=>NULL,
												'controller'=>'store',
												'action'=>'proyect',
												'id' => $pro['id'],
												'slug' => Inflector::slug($pro['name']),
												'ext' => 'html'
												),
											array('escape' => false)
										);
									?>
                                <?php endforeach;?>
                            </ul>
                        </li>
                    <?php endforeach;?>
   				</ul>
  			</li>
            <li>
            	<?php
					echo $this->Html->link(
					'Productos',
					array(
						'controller'=>'store',
						'action'=>'categorias',
						),
					array(
						'escape' => false,
						
					)
				);
				?>
  				<ul class="sub-menu">
                	<?php foreach($categories as $cats):?>
                    	<li>
                        	<?php
								echo $this->Html->link(
								$cats['Category']['name'],
								array(
									//'admin'=>false,
									//'plugin'=>NULL,
									'controller'=>'store',
									'action'=>'articulos',
									'id' => $cats['Category']['id'],
									'type' => 'categoria',
									'slug' => Inflector::slug($cats['Category']['name']),
									'ext' => 'html'
									),
								array(
									'escape' => false,
									
									)
								);
							?>
                            <ul>
                            	<?php foreach($cats['Article'] as $ar):?>
                                	<li>
                                    	<?php
											echo $this->Html->link(
													$ar['name'],
													array(
														'admin'=>false,
														'plugin'=>NULL,
														'controller'=>'store',
														'action'=>'articulo',
														'id' => $ar['id'],
														'slug' => Inflector::slug($ar['name']),
														'ext' => 'html'
														),
													array('escape' => false)
												);
										?>
                                    </li>
                                <?php endforeach?>
                            </ul>
                        </li>
					<?php endforeach;?>
   				</ul>
  			</li>
         	<li>
            	<?php
					echo $this->Html->link(
					'Servicios',
					'/store/servicios'
				);
				?>
           	</li>
            <li>
            	<?php
					echo $this->Html->link(
					'Contáctanos',
					'/store/contacto'
				);
				?>
           	</li>
  		</ul>
  </nav>-->

<link rel="stylesheet" href="/bmgcomercial/menu/css/defaults.css">
<link rel="stylesheet" href="/bmgcomercial/menu/css/nav-core.css">
<link rel="stylesheet" href="/bmgcomercial/menu/css/nav-layout.css">
<nav class="nav">
    <ul>
        <li>
        	<?php
				echo $this->Html->link(
					'Inicio',
					'/'
				);
			?>
        </li>
        <li>
        	<?php
					echo $this->Html->link(
					'Nosotros',
					'/store/nosotros'
				);
				?>
        </li>
        <li class="nav-submenu">
			<?php
			echo $this->Html->link(
			'Proyectos',
			array(
				'controller'=>'store',
				'action'=>'cats',
				),
			array(
				'escape' => false,
				
			)
			);
			?>
			<ul>
        	<?php foreach($lines as $lin):?>
            	<li class="nav-submenu">
					<?php
                        echo $this->Html->link(
                            $lin['Linea']['name'],
                            array(
                                //'admin'=>false,
                                //'plugin'=>NULL,
                                'controller'=>'store',
                                'action'=>'proyects',
                                'id' => $lin['Linea']['id'],
                                'slug' => Inflector::slug($lin['Linea']['name']),
                                'ext' => 'html'
                                ),
                            array(
                                'escape' => false,
                                
                            )
                        );
                    ?>
                    <ul>
                    	<?php foreach($lin['Proyecto'] as $pro):?>
                        	<?php
								echo $this->Html->link(
									$pro['name'],
									array(
										'admin'=>false,
										'plugin'=>NULL,
										'controller'=>'store',
										'action'=>'proyect',
										'id' => $pro['id'],
										'slug' => Inflector::slug($pro['name']),
										'ext' => 'html'
										),
									array('escape' => false)
								);
							?>
                        <?php endforeach;?>
                    </ul>
                </li>
            <?php endforeach;?>
			</ul>


        </li>
        <li class="nav-submenu">
        	<?php
					echo $this->Html->link(
					'Productos',
					array(
						'controller'=>'store',
						'action'=>'categorias',
						),
					array(
						'escape' => false,
						
					)
				);
				?>
  				<ul>
                	<?php foreach($categories as $cats):?>
                    	<li class="nav-submenu">
                        	<?php
								echo $this->Html->link(
								$cats['Category']['name'],
								array(
									//'admin'=>false,
									//'plugin'=>NULL,
									'controller'=>'store',
									'action'=>'articulos',
									'id' => $cats['Category']['id'],
									'type' => 'categoria',
									'slug' => Inflector::slug($cats['Category']['name']),
									'ext' => 'html'
									),
								array(
									'escape' => false,
									
									)
								);
							?>
                            <ul>
                            	<?php foreach($cats['Article'] as $ar):?>
                                	<li>
                                    	<?php
											echo $this->Html->link(
													$ar['name'],
													array(
														'admin'=>false,
														'plugin'=>NULL,
														'controller'=>'store',
														'action'=>'articulo',
														'id' => $ar['id'],
														'slug' => Inflector::slug($ar['name']),
														'ext' => 'html'
														),
													array('escape' => false)
												);
										?>
                                    </li>
                                <?php endforeach?>
                            </ul>
                        </li>
					<?php endforeach;?>
   				</ul>
        </li>
        <li>
        	<?php
					echo $this->Html->link(
					'Servicios',
					'/store/servicios'
				);
				?>
        </li>
        <li>
			<?php
					echo $this->Html->link(
					'Contáctanos',
					'/store/contacto'
				);
				?>
        </li>
    </ul>
</nav>
<script src="/bmgcomercial/menu/js/nav.jquery.min.js"></script>
<script>
    $('.nav').nav();
</script>
