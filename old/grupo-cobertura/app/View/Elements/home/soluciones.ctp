<?php $lineas = $this->requestAction('store/getProyectos'); //pr($categories);?>
<div class="m30"></div>
<?php $m = count($lineas);?>
<?php $i = 1;$j= 1;?>

<?php foreach ($lineas as $category): ?>
<?php if ($i == 1): ?>
    <div class="section group">
<?php endif;?>
    <div class="col span_4_of_12_ccccc txt_center">
        <?php if (!empty($category['Linea']['imagen'])): ?>
            <div class="categoria-pro">
                <?php
                    echo $this->Html->link(
                        $this->Html->image('/img/_lineas/' . $category['Linea']['imagen']),
                        array(
                            'controller' => 'store',
                            'action'     => 'proyects',
                            'id'         => $category['Linea']['id'],
                            'slug'       => Inflector::slug($category['Linea']['name']),
                            'ext'        => 'html',
                        ),
                        array('escape' => false)
                    );
                ?>
        <?php else: ?>
            <div class="categoria">
                <?php
                    echo $this->Html->link(
                        $this->Html->image('/img/_categories/default.jpg'),
                        array(
                            'controller' => 'store',
                            'action'     => 'proyects',
                            'id'         => $category['Linea']['id'],
                            'slug'       => Inflector::slug($category['Linea']['name']),
                            'ext'        => 'html',
                        ),
                        array('escape' => false)
                    );
                ?>
        <?php endif?>
            <div class="categoria_titulo-pro paprobot">
                <table width="100%" border="0"  cellpadding="0" cellspacing="0">
                    <tr>
                        <td width="100%" align="center">
                            <?php
                                echo $this->Html->link(
                                    $category['Linea']['name'],
                                    array(
                                        'controller' => 'store',
                                        'action'     => 'proyects',
                                        'id'         => $category['Linea']['id'],
                                        'slug'       => Inflector::slug($category['Linea']['name']),
                                        'ext'        => 'html',
                                    ),
                                    array('escape' => false)
                                );
                            ?>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
<?php if ($j == $m): ?>
    <?php echo '</div>'; ?>
<?php elseif ($i == 3): ?>
</div>
<?php $i = 1;?>
<?php elseif ($i == 1 or $i == 2): ?>
<?php $i++;?>
<?php endif;?>
<?php $j++;endforeach;?>


