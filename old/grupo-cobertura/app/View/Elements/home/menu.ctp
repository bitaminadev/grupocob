<?php $categories = $this->requestAction('store/getCategories'); //pr($categories);?>
<?php $lines = $this->requestAction('store/getProyectos'); //pr($categories);?>
<link rel="stylesheet" href="/bmgcomercial/menu/css/defaults.css">
<link rel="stylesheet" href="/bmgcomercial/menu/css/nav-core.css">
<link rel="stylesheet" href="/bmgcomercial/menu/css/nav-layout.css">
<a href="#" class="nav-button">Menu</a>
<nav class="nav">
    <ul>
    	<div class="m20"></div>
        <li>
        	<?php
				echo $this->Html->link(
					'Inicio',
					'/'
				);
			?>
        </li>
        <li>
        	<?php
					echo $this->Html->link(
					'Nosotros',
					'/store/nosotros'
				);
				?>
        </li>
        <li class="nav-submenu">
			<?php
			echo $this->Html->link(
			'Proyectos',
			array(
				'controller'=>'store',
				'action'=>'cats',
				),
			array(
				'escape' => false,
				
			)
			);
			?>
			<ul>
        	<?php foreach($lines as $lin):?>
            	<li class="nav-submenu">
					<?php
                        echo $this->Html->link(
                            $lin['Linea']['name'],
                            array(
                                //'admin'=>false,
                                //'plugin'=>NULL,
                                'controller'=>'store',
                                'action'=>'proyects',
                                'id' => $lin['Linea']['id'],
                                'slug' => Inflector::slug($lin['Linea']['name']),
                                'ext' => 'html'
                                ),
                            array(
                                'escape' => false,
                                
                            )
                        );
                    ?>
                    <ul>
                    	<?php foreach($lin['Proyecto'] as $pro):?>
                        	<?php
								echo $this->Html->link(
									$pro['name'],
									array(
										'admin'=>false,
										'plugin'=>NULL,
										'controller'=>'store',
										'action'=>'proyect',
										'id' => $pro['id'],
										'slug' => Inflector::slug($pro['name']),
										'ext' => 'html'
										),
									array('escape' => false)
								);
							?>
                        <?php endforeach;?>
                    </ul>
                </li>
            <?php endforeach;?>
			</ul>


        </li>
        <li class="nav-submenu">
        	<?php
					echo $this->Html->link(
					'Productos',
					array(
						'controller'=>'store',
						'action'=>'categorias',
						),
					array(
						'escape' => false,
						
					)
				);
				?>
  				<ul>
                	<?php foreach($categories as $cats):?>
                    	<li class="nav-submenu">
                        	<?php
								echo $this->Html->link(
								$cats['Category']['name'],
								array(
									//'admin'=>false,
									//'plugin'=>NULL,
									'controller'=>'store',
									'action'=>'articulos',
									'id' => $cats['Category']['id'],
									'type' => 'categoria',
									'slug' => Inflector::slug($cats['Category']['name']),
									'ext' => 'html'
									),
								array(
									'escape' => false,
									
									)
								);
							?>
                            <ul>
                            	<?php foreach($cats['Article'] as $ar):?>
                                	<li>
                                    	<?php
											echo $this->Html->link(
													$ar['name'],
													array(
														'admin'=>false,
														'plugin'=>NULL,
														'controller'=>'store',
														'action'=>'articulo',
														'id' => $ar['id'],
														'slug' => Inflector::slug($ar['name']),
														'ext' => 'html'
														),
													array('escape' => false)
												);
										?>
                                    </li>
                                <?php endforeach?>
                            </ul>
                        </li>
					<?php endforeach;?>
   				</ul>
        </li>
        <li>
        	<?php
					echo $this->Html->link(
					'Servicios',
					'/store/servicios'
				);
				?>
        </li>
        <li>
			<?php
					echo $this->Html->link(
					'Contáctanos',
					'/store/contacto'
				);
				?>
        </li>
    </ul>
    <div class="m20"></div>
</nav>

<script src="/bmgcomercial/menu/js/nav.jquery.min.js"></script>
<script>
    //$('.nav').nav('move');
    $('.nav').nav({
    // Mobile menu button selector
   // navButton: '.nav-button',
    // Sub menu selector (<li>)
   // subMenu: '.nav-submenu',
    // Open sub menu's on mouse over
    // when not in mobile mode
    mouseOver: true,
    // When clicking/touching a sub menu link, it will open the sub menu...
    // Not disabling the links will make sub menu's unreachable on touch devices!
    // A link with [href="#"] will always be disabled, regardless of this setting.
    // Disable the actual link in a particular mode:
    //   always|never|mobile|desktop
    disableSubMenuLink: 'disabled',
    // How fast should a sub menu open/close? (ms)
   // slideSpeed: 500
});
</script>