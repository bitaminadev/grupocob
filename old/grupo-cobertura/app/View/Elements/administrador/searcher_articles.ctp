<?php
$categories = $this->requestAction('store/getCategories');
$brands = $this->requestAction('store/getBrands');
?>
<?php
    echo $this->Form->create('Article', array(
        'url' => array_merge(array('controller'=>'articles','action' => 'index'), $this->params['pass']),
        'class' => 'form-inline form-search well',
        'div' => false
    ));
?>
    <?php
    echo $this->Form->input(
        'Article.category_id',
        array(
            'options'=>$categories,
            'div'=>false,
            'label'=>'Categorías',
            'empty' => '-----',
            'class' => 'span2'
        )
    );
    echo $this->Form->input(
        'Article.brand_id',
        array(
            'options'=>$brands,
            'div'=>false,
            'label'=>'Marcas',
            'empty' => '-----',
            'class' => 'span2'
        )
    );
    echo $this->Form->input(
        'filter',
        array(
            'div'=>false,
            'label'=>false,
            'class' => 'span3',
            'placeholder' => 'Código, Descripcion'
        )
    );
    /*
    echo $this->Form->input(
        'minimum_stock',
        array(
            'div' => false,
            'label' => 'Stock agotado',
            'type' => 'checkbox'
        )
    );
     * 
     */
    ?>
    <button type="submit" class="btn">Buscar</button>
  </form>