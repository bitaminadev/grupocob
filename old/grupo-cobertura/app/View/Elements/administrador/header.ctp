<?php ?>
<div class="navbar">
    <div class="navbar-inner">
        <div class="container-fluid">
            <a class="btn btn-navbar" data-toggle="collapse" data-target=".top-nav.nav-collapse,.sidebar-nav.nav-collapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </a>
            <?php echo $this->Html->link('<span>Grupo Cobertura S. de R.L. de C.V.</span>','#',array('escape'=>false,'class'=>'brand'));?>				
            <!-- start: Header Menu -->
            <div class="nav-no-collapse header-nav">
                <ul class="nav pull-right">
                    <!-- start: User Dropdown -->
                    <li class="dropdown">
                        <a class="btn dropdown-toggle" data-toggle="dropdown" href="#">
                            <i class="halflings-icon white user"></i> <?php echo $this->Session->read('Auth.User.username'); ?>
                            <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu">
                            <li class="dropdown-menu-title">
                                <span>Account Settings</span>
                            </li>
                            <li><?php echo $this->Html->link('<i class="halflings-icon user"></i> Perfil',
                                array('admin'=>true,'plugin'=>NULL,'controller'=>'users','action'=>'edit',$this->Session->read('Auth.User.id')),array('escape'=>false)); ?>
                            </li>
                            <li><?php echo $this->Html->link('<i class="halflings-icon off"></i> Logout',
                                array('admin'=>false,'plugin'=>NULL,'controller'=>'users','action'=>'logout'),array('escape'=>false)); ?>
                            </li>
                            <!--<li><a href="login.html"><i class="halflings-icon off"></i> Logout</a></li>-->
                        </ul>
                    </li>
                    <!-- end: User Dropdown -->
                </ul>
            </div>
            <!-- end: Header Menu -->
        </div>
    </div>
</div>