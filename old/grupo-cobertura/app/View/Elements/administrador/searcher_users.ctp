<?php
$groups = $this->requestAction('store/getGroups');
?>
<?php
    echo $this->Form->create('User', array(
        'url' => array_merge(array('controller'=>'users','action' => 'index'), $this->params['pass']),
        'class' => 'form-inline form-search well',
        'div' => false,
    ));
?>
    <?php
    echo $this->Form->input(
        'User.username',
        array(
            'div'=>false,
            'label'=>'Alias',
            'class' => 'span2',
        )
    );
    echo $this->Form->input(
        'User.name',
        array(
            'div'=>false,
            'label'=>'Nombre',
            'class' => 'span2',
        )
    );
    echo $this->Form->input(
        'User.email',
        array(
            'div'=>false,
            'label'=>'Correo',
            'class' => 'span2',
        )
    );
    echo $this->Form->input(
        'User.group_id',
        array(
            'options'=>$groups,
            'div'=>false,
            'label'=>'Grupo',
            'empty' => '-----',
            'class' => 'span2'
        )
    );
    ?>
    <button type="submit" class="btn">Buscar</button>
  </form>