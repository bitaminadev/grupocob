<?php $controller = $this->request->params['controller']; ?>
<div id="sidebar-left" class="span2">
    <div class="nav-collapse sidebar-nav">
        <ul class="nav nav-tabs nav-stacked main-menu">
        	<li class="nav-header">Administración</li>
            <?php if($controller == 'providers'){ $class ='active'; }else{ $class = ''; } ?>
            <li class="<?php echo $class; ?>">
                <?php echo $this->Html->link(__('<i class="icon-indent-right"></i><span class="hidden-tablet"> Provedores</span>',true),array('admin'=>true,'plugin' => NULL,'controller'=>'providers','action'=>'index'),array('escape'=>false)); ?>
            </li>
            <?php if($controller == 'evaluations'){ $class ='active'; }else{ $class = ''; } ?>
            <li class="<?php echo $class; ?>">
                <?php echo $this->Html->link(__('<i class="icon-indent-right"></i><span class="hidden-tablet"> Evaluaciones</span>',true),array('admin'=>true,'plugin' => NULL,'controller'=>'evaluations','action'=>'index'),array('escape'=>false)); ?>
            </li>
            <?php if($controller == 'orders'){ $class ='active'; }else{ $class = ''; } ?>
            <li class="<?php echo $class; ?>">
                <?php echo $this->Html->link(__('<i class="icon-indent-right"></i><span class="hidden-tablet"> Pedidos</span>',true),array('admin'=>true,'plugin' => NULL,'controller'=>'orders','action'=>'index'),array('escape'=>false)); ?>
            </li>
             <?php if($controller == 'articles'){ $class ='active'; }else{ $class = ''; } ?>
            <li class="<?php echo $class; ?>">
                <?php echo $this->Html->link(__('<i class="icon-indent-right"></i><span class="hidden-tablet"> Artículos</span>',true),array('admin'=>true,'plugin' => NULL,'controller'=>'articles','action'=>'index'),array('escape'=>false)); ?>
            </li>
            <?php if($controller == 'works'){ $class ='active'; }else{ $class = ''; } ?>
            <li class="<?php echo $class; ?>">
                <?php echo $this->Html->link(__('<i class="icon-indent-right"></i><span class="hidden-tablet"> Obras</span>',true),array('admin'=>true,'plugin' => NULL,'controller'=>'works','action'=>'index'),array('escape'=>false)); ?>
            </li>
            <?php if($controller == 'contractors'){ $class ='active'; }else{ $class = ''; } ?>
            <li class="<?php echo $class; ?>">
                <?php echo $this->Html->link(__('<i class="icon-indent-right"></i><span class="hidden-tablet"> Contratistas</span>',true),array('admin'=>true,'plugin' => NULL,'controller'=>'contractors','action'=>'index'),array('escape'=>false)); ?>
            </li>
            <?php if($controller == 'statuses'){ $class ='active'; }else{ $class = ''; } ?>
            <li class="<?php echo $class; ?>">
                <?php echo $this->Html->link(__('<i class="icon-indent-right"></i><span class="hidden-tablet"> Estatus</span>',true),array('admin'=>true,'plugin' => NULL,'controller'=>'statuses','action'=>'index'),array('escape'=>false)); ?>
            </li>
            <?php if($controller == 'quotations'){ $class ='active'; }else{ $class = ''; } ?>
            <li class="<?php echo $class; ?>">
                <?php echo $this->Html->link(__('<i class="icon-indent-right"></i><span class="hidden-tablet"> Cotizaciones</span>',true),array('admin'=>true,'plugin' => NULL,'controller'=>'quotations','action'=>'index'),array('escape'=>false)); ?>
            </li>
            <?php if($controller == 'products'){ $class ='active'; }else{ $class = ''; } ?>
            <li class="<?php echo $class; ?>">
                <?php echo $this->Html->link(__('<i class="icon-indent-right"></i><span class="hidden-tablet"> Productos</span>',true),array('admin'=>true,'plugin' => NULL,'controller'=>'products','action'=>'index'),array('escape'=>false)); ?>
            </li>
            <li class="nav-header">Gestion</li>
            <?php if($controller == 'users'){ $class ='active'; }else{ $class = ''; } ?>
            <li class="<?php echo $class; ?>">
				<?php echo $this->Html->link(__('<i class="icon-user"></i><span class="hidden-tablet"> Usuarios</span>',true),array('admin'=>true,'plugin' => NULL,'controller'=>'users','action'=>'index'),array('escape'=>false)); ?>
           	</li>
            <?php if($controller == 'groups'){ $class ='active'; }else{ $class = ''; } ?>
            <li class="<?php echo $class; ?>">
				<?php echo $this->Html->link(__('<i class="icon-group"></i><span class="hidden-tablet"> Grupos</span>',true),array('admin'=>true,'plugin' => NULL,'controller'=>'groups','action'=>'index'),array('escape'=>false)); ?>
           	</li>
            <?php if($controller == 'acl'){ $class ='active'; }else{ $class = ''; } ?>
            <li class="<?php echo $class; ?>">
				<?php echo $this->Html->link(__('<i class="icon-cog"></i><span class="hidden-tablet"> ACL</span>',true),array('admin'=>'true','plugin'=>'acl','controller'=>'acl','action'=>'index'),array('escape'=>false)); ?>
           	</li>
        </ul>
    </div>
</div>