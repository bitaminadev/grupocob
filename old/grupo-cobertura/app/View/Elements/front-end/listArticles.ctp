<?php ?>
<div class="m40"></div>
<div class="section group">
    <div class="col span_12_of_12">
        <div class="section group">
            <div class="col span_3_of_12">
            	<?php echo $this->element('front-end/menu'); ?>
            </div>
            <div class="col span_9_of_12">
            	<div class="section group">
    				<div class="col span_12_of_12">
                    	<div class="productos-imagenes-titulo">
                            <div class="productos-imagenes-titulo1"><?php echo 'Búsqueda:'?></div> 
                        	<!--<div class="text-ruta"><?php //echo $this->Html->link('CATEGORÍAS ','/store/categorias')?>| <?php //echo $category['Category']['name']?> </div> -->
                      	</div>
                    </div>
              	</div>
                <div class="section group">
    				<div class="col span_12_of_12 txt_center">
                    	<?php foreach($articles as $article): ?>
                       		<div class="contenedor-imagenes ">
                            	<?php 
								if(!empty($article['Image'][0]['image'])){
									
										//if(!empty($image['image'])){
											echo $this->Html->link(
												$this->Html->image('/img/_articles/mediana_'.$article['Image'][0]['image'],array("class"=>"imagen-borde"/*'style'=>'max-width:127px;max-height:149px;'*/)),
												array(
													'admin'=>false,
													'plugin'=>NULL,
													'controller'=>'store',
													'action'=>'articulo',
													'id' => $article['Article']['id'],
													'slug' => Inflector::slug($article['Article']['name']),
													'ext' => 'html'
													),
												array('escape' => false)
											);
											$show = true;
										//}
									
								}else{
									echo $this->Html->link(
												$this->Html->image('/img/_articles/mediana.jpg',array("class"=>"imagen-borde"/*'style'=>'max-width:127px;max-height:149px;'*/)),
												array(
													'admin'=>false,
													'plugin'=>NULL,
													'controller'=>'store',
													'action'=>'articulo',
													'id' => $article['Article']['id'],
													'slug' => Inflector::slug($article['Article']['name']),
													'ext' => 'html'
													),
												array('escape' => false)
											);
								}
								
								
								?>
                                <div class="contenedor-imagenes-texto"><?php echo $article['Article']['name'];?><br />
                                  <span class="codigo-prod">CÓDIGO: <?php echo $article['Article']['code'];?></span> <br />
                                </div>
                                <div class="contenedor-imagenes-bullet">
                                	<?php
										echo $this->Html->link(
												$this->Html->image('home/bek-b-mas-producto.png',array(/*'style'=>'max-width:127px;max-height:149px;'*/)),
												array(
													'admin'=>false,
													'plugin'=>NULL,
													'controller'=>'store',
													'action'=>'articulo',
													'id' => $article['Article']['id'],
													'slug' => Inflector::slug($article['Article']['name']),
													'ext' => 'html'
													),
												array('escape' => false)
											);
									?>
                                    
                               	</div>
                            </div>
                    	<?php endforeach?>
                    </div>
              	</div>
                <div class="section group">
    				<div class="col span_12_of_12 txt_center">
                    	<div class="paging">
                        	|<?php echo $this->Paginator->numbers(array('separator' => '|'));?>|
						</div>
                        <div class="m40"></div>
                    </div>
              	</div>
            </div>
       	</div>
   	</div>
</div>
<div class="m40"></div>