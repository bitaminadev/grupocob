<?php
echo $this->Html->css(array(
    '/wowslider/banner_home/engine1/style'
));
?>
<!-- Start WOWSlider.com BODY section -->
<div id="wowslider-container1">
    <div class="ws_images">
        <ul>
            <li>
                <!--<img src="data1/images/banner_1.jpg" alt="banner_1" border="0" id="wows0" title="banner_1"/>-->
                <?php echo $this->Html->image('/wowslider/banner_home/data1/images/banner_1.jpg',array('alt'=>'banner_1','id'=>'wows0','title'=>'banner_1')); ?>
            </li>
            <li>
                <!--<img src="data1/images/banner_2.jpg" alt="banner_2" title="banner_2" id="wows1"/></li>-->
                <?php echo $this->Html->image('/wowslider/banner_home/data1/images/banner_2.jpg',array('alt'=>'banner_2','id'=>'wows2','title'=>'banner_2')); ?>
        </ul>
    </div>
    <div class="ws_bullets">
        <div>
            <a href="#" title="banner_1">1</a>
            <a href="#" title="banner_2">2</a>
        </div>
    </div>
    <a class="wsl" href="http://wowslider.com">Photo Slideshow HTML Code by WOWSlider.com v2.1</a>
</div>
<?php
echo $this->Html->script(array(
    '/wowslider/banner_home/engine1/wowslider',
    '/wowslider/banner_home/engine1/script'
));
?>
<!--
<script type="text/javascript" src="engine1/wowslider.js"></script>
<script type="text/javascript" src="engine1/script.js"></script>
-->
<!-- End WOWSlider.com BODY section -->