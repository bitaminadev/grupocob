<?php $categories = $this->requestAction('store/getCategories');//pr($categories);?>
<div id="menu-lateral">
	<div class="menu-lateral-titulo">PRODUCTOS</div>
    	<div class="menu-lateral-categorias">
        	<?php foreach($categories as $key=>$category):?>
            	<span class="menu-lateral-categorias-texto">
				<?php //echo $category ?>
                <?php
                	echo $this->Html->link(
						$category,
						array(
							//'admin'=>false,
							//'plugin'=>NULL,
							'controller'=>'store',
							'action'=>'articulos',
							'id' => $key,
							'type' => 'categoria',
							'slug' => Inflector::slug($category),
							'ext' => 'html'
							),
						array(
							'escape' => false,
							'class' =>''
						)
					);
               	?>
                
                </span>
            	<?php
                	echo $this->Html->link(
						$this->Html->image('home/bek-b-mas-menu.png',array("class"=>"menu-lateral-categorias-bullet")),
						array(
							//'admin'=>false,
							//'plugin'=>NULL,
							'controller'=>'store',
							'action'=>'articulos',
							'id' => $key,
							'type' => 'categoria',
							'slug' => Inflector::slug($category),
							'ext' => 'html'
							),
						array(
							'escape' => false,
							
						)
					);
               	?>
                <br />
            <?php endforeach?>
        </div>
    </div>
 