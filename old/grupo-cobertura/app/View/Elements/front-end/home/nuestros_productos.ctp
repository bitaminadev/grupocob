


    <?php if(!empty($articles)): ?>
    <div class="home_nuestros_productos" style="padding-top: 20px;">
        <div class="row">
            <div class="span10 offset1">
                <ul class="thumbnails">
                <?php $i = 0; ?>
                <?php foreach($articles as $article): ?>
                        <?php if($i < 6): ?>
                        <li style="width:123px; margin-left: 15px; height: 145px;">
                            <div class="thumbnail" style="text-align: center; height: 100%;">
                              <?php

                                if(!empty($article['Image'])){
                                   $showImage = false;
                                    foreach($article['Image'] as $image){
                                        if($showImage == false){
                                            if(file_exists(WWW_ROOT.'/media/filter/m/img/'.$image['basename'])){
                                                echo $this->Html->link(
                                                    $this->Html->image('/media/filter/s/img/'.$image['basename'],array('style'=>'max-height: 110px;max-width:100px;')),
                                                    array(
                                                        'admin'=>false,
                                                        'plugin'=>NULL,
                                                        'controller'=>'store',
                                                        'action'=>'articulo',
                                                        'id' => $article['Article']['id'],
                                                        'slug' => Inflector::slug($article['Article']['description']),
                                                        'ext' => 'html'
                                                    ),
                                                    array('escape'=>false)
                                                );
                                                $showImage = true;
                                            }
                                        }
                                    }
                                    if($showImage == false){
                                        echo $this->Html->link(
                                            $this->Html->image('/media/filter/m/img/default.png',array('style'=>'max-width: 100%;')),
                                            array(
                                                'admin'=>false,
                                                'plugin'=>NULL,
                                                'controller'=>'store',
                                                'action'=>'articulo',
                                                'id' => $article['Article']['id'],
                                                'slug' => Inflector::slug($article['Article']['description']),
                                                'ext' => 'html'
                                            ),
                                            array('escape'=>false)
                                        );
                                    }
                                }else{
                                    echo $this->Html->link(
                                        $this->Html->image('/media/filter/m/img/default.png',array('style'=>'max-width: 100%;')),
                                        array(
                                            'admin'=>false,
                                            'plugin'=>NULL,
                                            'controller'=>'store',
                                            'action'=>'articulo',
                                            'id' => $article['Article']['id'],
                                            'slug' => Inflector::slug($article['Article']['description']),
                                            'ext' => 'html'
                                        ),
                                        array('escape'=>false)
                                    );
                                }
                                ?>
                              <span style="font-size: .7em;line-height: 0px;"><?php echo $this->Text->truncate($article['Article']['description'],25); ?></span>

                            </div>
                      </li>
                      <?php endif; ?>
                      <?php $i++; ?>
                <?php endforeach; ?>
                </ul>
            </div>
        </div>
    </div>
    <?php endif; ?>
    
