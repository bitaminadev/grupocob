<div class="home_login">
    <div class="row">
        <div class="span2 offset1">
            <h2 class="orange">Mayoristas</h2>
        </div>
        <?php echo $this->Form->create('User',array('url'=>array('admin'=>false,'plugin'=>null,'controller'=>'users','action'=>'login'))); ?>
        <div class="span3"><?php echo $this->Form->input('username',array('label'=>'Usuario:')); ?></div>
        <div class="span3"><?php echo $this->Form->input('password',array('label'=>'Contraseña:')); ?></div>
        <div class="span2"><br><?php echo $this->Form->end('Ingresar'); ?></div>
    </div>
</div>