<div class="home_novedades_y_ofertas">
    <div class="row">
        <div class="span12">
            <?php 
            echo $this->Html->link(
                $this->Html->image('front-end/promo_home2.jpg',array('style'=>'margin:40px auto auto 62px;')),
                array(
                    'admin'=>false,
                    'plugin'=>NULL,
                    'controller'=>'store',
                    'action'=>'articulos',
                    'id' => 9,
                    'type' => 'categoria',
                    'slug' => Inflector::slug('Novedades'),
                    'ext' => 'html'
                ),
                array('escape'=>false)
            );
            ?>
            <?php 
            echo $this->Html->link(
                $this->Html->image('front-end/promo_home1.jpg',array('style'=>'margin:40px auto auto 53px;')),
                array(
                    'admin'=>false,
                    'plugin'=>NULL,
                    'controller'=>'store',
                    'action'=>'articulos',
                    'id' => 10,
                    'type' => 'categoria',
                    'slug' => Inflector::slug('Ofertas'),
                    'ext' => 'html'
                ),
                array('escape'=>false)
            );
            ?>
        </div>
    </div>
</div>