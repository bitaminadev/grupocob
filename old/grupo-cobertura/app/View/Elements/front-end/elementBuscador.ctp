<?php
$categories = $this->requestAction('store/getCategories');
$brands = $this->requestAction('store/getBrands');
?>
<?php
/*
    echo $this->Form->create('Article',array(
        
        'url'=> array(
            'admin' => false,
            'plugin' => NULL,
            'controller' => 'store',
            'action' => 'buscador'
        ),
         
        'url' => array_merge(array('action' => 'find'), $this->params['pass']),
        'class' => 'form-inline form-search',
        'div' => false
    ));
   *
         */
    echo $this->Form->create('Article', array(
        'url' => array_merge(array('controller'=>'store','action' => 'buscador'), $this->params['pass']),
        'class' => 'form-inline form-search',
        'div' => false
    ));
?>
    <label class="title">Búsqueda</label>
    <?php 
    echo $this->Form->input(
        'Article.category_id',
        array(
            'options'=>$categories,
            'div'=>false,
            'label'=>'Categorías',
            'empty' => '-----',
            'class' => 'span2'
        )
    );
    echo $this->Form->input(
        'Article.brand_id',
        array(
            'options'=>$brands,
            'div'=>false,
            'label'=>'Marcas',
            'empty' => '-----',
            'class' => 'span2'
        )
    );
    echo $this->Form->input(
        'filter',
        array(
            'div'=>false,
            'label'=>false,
            'class' => 'span3',
            'placeholder' => 'Código, Descripcion'
        )
    );
    ?>
    <button type="submit" class="btn">Buscar</button>
  </form>