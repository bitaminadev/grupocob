<?php $categorias = $this->requestAction('home/element_menu');//pr($categorias);?>
<div id="header-container" class="container">
    <div class="row">
        <div class="span12">
            <div class="border">
                <div class="innerborder no-shadow">
                    <div class="row" >
                        <!-- START LOGO -->
                        <div id="logo" class="span3">
         	                   <div>
                                <a id="logo-img" href="/cake2/" title="Contraste Diseño y Publicidad">
                                	<?php echo $this->Html->image('icons/logo.png',array("title"=>"Contraste", "alt"=>"Contraste", "width"=>"110", "height"=>"64"));?>
                                    
                                </a>
                            </div>
                        </div>
                        <!-- END LOGO -->
                        <!-- START MENU - TOPBAR -->
                        <div id="nav-topbar" class="span9">
                            <!-- START TOPBAR -->
                            <div id="topbar" class="row">
                                <div class="topbar-left span7" style="text-align:right;">
                                    <!-- login links -->
                                    <div class="welcome_username">
                                        <a href="/cake2/users/login">Login <span> / </span> Register</a>    
                                    </div>
                                    <!-- /login links -->
                                    <!-- Top bar menu -->
                                    <!-- /Top bar menu -->
                                    <!-- Widget area -->
                                    <div id="text-2" class="widget-1 widget-first widget widget_text">			
                                        <div class="textwidget">
                                            <a href="#" target="_blank" title="Facebook Contraste Diseño y Publicidad">
                                            	<?php echo $this->Html->image('/wp-content/themes/room09/images/facebook2013.jpg');?>	
                                                
                                            </a>
                                            <a href="#" target="_blank" title="twitter Contraste Diseño y Publicidad">
                                            	<?php echo $this->Html->image('/wp-content/themes/room09/images/twitter.png');?>	
                                                
                                            </a>
                                        </div>
                                        
                                    </div>
                                    <!-- /Widget area -->
                                </div>
                            </div>
                            <div class="topbar-border">
                            </div>
                            <!-- END TOPBAR -->
                            <!-- START NAVIGATION -->
                            <div id="nav">
                                <ul id="menu-menu-principal" class="level-1">
                                	
                                	<li  class="menu-item menu-item-type-post_type menu-item-object-page current-menu-item page_item page-item-2 current_page_item menu-item-children-0">
                                    	
                                        <?php echo $this->Html->link('Home','/');?>
                                    </li>
                                    <li id="menu-item-71" class="megamenu menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children dropdown menu-item-children-3">
                                    	<a href="shop/index.html">Portafolio</a>
                                    	<ul class="sub-menu">
                                        	<?php foreach($categorias as $cat):?>
                                    			<li id="menu-item-126" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children dropdown menu-item-children-6">
                                                	<?php echo $this->Html->link($cat['Categoria']['name'],
															array(
																'admin'=>false,
																'plugin'=>NULL,
																'controller'=>'home',
																'action'=>'portafolio',
																'id' => $cat['Categoria']['id'],
																'slug' => Inflector::slug($cat['Categoria']['name']),
																'ext' => 'html'
															)
														);
													?>
													<?php if(!empty($cat['Cliente'])):?>
                                                        <ul class="sub-menu">
                                                            <?php foreach($cat['Cliente'] as $cliente):?>
                                                                <li id="menu-item-160" class="menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-children-0">
                                                                    <?php echo $this->Html->link($cliente['name'],
																	array(
																		'admin'=>false,
																		'plugin'=>NULL,
																		'controller'=>'home',
																		'action'=>'cliente',
																		'id' => $cliente['id'],
																		'cat' => $cat['Categoria']['id'],
																		'slug' => Inflector::slug($cliente['name']),
																		'ext' => 'html'
																	)
																	
																	);?>
                                                                </li>
                                                           <?php endforeach;?>
                                                       </ul>
                                                   <?php endif;?>
                                                </li>
                                    		<?php endforeach;?>
                                    	</ul>
                                  	</li>
                                    <li id="menu-item-72" class="menu-item menu-item-type-post_type menu-item-object-page current-menu-item page_item page-item-2 current_page_item menu-item-children-0">
                                    	<a href="#">Contacto</a>
                                    </li>
                               	</ul>                                    
                            </div>
                            <!-- END NAVIGATION -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- CART -->
        <!--<div id="header-cart" class="no-shadow span2">
            <div class="border" >
                <div class="innerborder">
                    <span class="cart-label">Cart</span>
                    <div class="topbar-border">
                    </div>
                    <div class="yit_cart_widget widget_shopping_cart">
                        <div class="cart_label">
                            <a href="cart/index.html" class="cart-icon">
                                <img width="28" height="22" src="../pillufashion.com/wp-content/themes/room09/woocommerce/images/header-cart.png" alt="View Cart" />
                             </a>
                            <a href="cart/index.html" class="cart-items">
                                <span class="cart-items-number">0</span>
                                <span class="cart-items-label">Items</span>
                                <span>|</span>
                                <span class="cart-subtotal"><span class="amount">0,00</span></span>
                                <span class="cart-subtotal-currency">&euro;</span>
                            </a>
                        </div>
                        <div class="cart_wrapper" style="display:none">
                            <div class="widget_shopping_cart_content group">
                                <ul class="cart_list product_list_widget">
                                    <li class="empty">No products in the cart.</li>
                                </ul>
                            </div>
                        </div>
                        <script type="text/javascript">
                            jQuery(document).ready(function($){
                                $(document).on('mouseover', '.cart_label', function(){
                                    $(this).next('.cart_wrapper').slideDown();
                                }).on('mouseleave', '.cart_label', function(){
                                    $(this).next('.cart_wrapper').delay(500).slideUp();
                                });
                
                                $(document)
                                    .on('mouseenter', '.cart_wrapper', function(){ $(this).stop(true,true).show() })
                                    .on('mouseleave', '.cart_wrapper',  function(){ $(this).delay(500).slideUp() });
                            });
                        </script>
                    </div>
                </div>
            </div>
        </div>-->
        <!-- /END CART -->
    </div>
</div>
<!-- BEGIN FLEXSLIDER SLIDER -->
<div id="slider-sliderhome-0" class="slider slider-sliderhome flexslider container" style="">
    <div class="slider-shadow">
    </div>
</div>
<script type="text/javascript">
    jQuery(document).ready(function($){
        //$('#slider-sliderhome.flexslider img.attachment-full').css('width', '100%').css('height', '400px');
        
        $('#slider-sliderhome-0.flexslider .slider-wrapper').flexslider({
            animation: 'fade',
            slideshowSpeed: 3000,
            animationSpeed: 800,
            controlNav: 0,
            
        });
    });
</script>