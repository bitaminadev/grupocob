<div class="navbar navbar-inverse navbar-fixed-top">
      <div class="navbar-inner">
        <div class="container-fluid">
          <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </a>
          <a class="brand" href="#">ADA INNOVA</a>
          <div class="nav-collapse collapse">
            <p class="navbar-text pull-right">
              Logged in as <a href="#" class="navbar-link"><?php echo $this->Session->read('Auth.User.username'); ?></a>
            </p>
            <ul class="nav">
              <li class="active"><a href="#">Back-end</a></li>
              <li><?php echo $this->Html->link('Front-end','/',array('target'=>'_blank')); ?></li>
              <li><?php echo $this->Html->link('Salir',array('admin'=>false,'plugin'=>NULL,'controller'=>'users','action'=>'logout')); ?></li>
            </ul>
          </div><!--/.nav-collapse -->
        </div>
      </div>
    </div>