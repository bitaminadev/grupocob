<div class="well sidebar-nav">
    <ul class="nav nav-list">
        <li class="nav-header">Tienda</li>
        
		<?php $controller = $this->request->params['controller']; ?>
      <?php if($controller == 'articles' || $controller == 'images'){ $class ='active'; }else{ $class = ''; } ?>
        <li class="<?php echo $class; ?>"><?php echo $this->Html->link(__('Articles',true),array('admin'=>true,'plugin' => NULL,'controller'=>'articles','action'=>'index')); ?></li>

        <?php if($controller == 'brands'){ $class ='active'; }else{ $class = ''; } ?>
        <li class="<?php echo $class; ?>"><?php echo $this->Html->link(__('Brands',true),array('admin'=>true,'plugin' => NULL,'controller'=>'brands','action'=>'index')); ?></li>

        <?php if($controller == 'categories'){ $class ='active'; }else{ $class = ''; } ?>
        <li class="<?php echo $class; ?>"><?php echo $this->Html->link(__('Categories',true),array('admin'=>true,'plugin' => NULL,'controller'=>'categories','action'=>'index')); ?></li>

        <?php if($controller == 'presentations'){ $class ='active'; }else{ $class = ''; } ?>
        <li class="<?php echo $class; ?>"><?php echo $this->Html->link(__('Presentations',true),array('admin'=>true,'plugin' => NULL,'controller'=>'presentations','action'=>'index')); ?></li>

        <?php if($controller == 'configurations'){ $class ='active'; }else{ $class = ''; } ?>
        <li class="<?php echo $class; ?>"><?php echo $this->Html->link(__('Configurations',true),array('admin'=>true,'plugin' => NULL,'controller'=>'configurations','action'=>'index')); ?></li>
        
        <li class="nav-header">Pagos</li>

        <?php if($controller == 'orders'){ $class ='active'; }else{ $class = ''; } ?>
        <li class="<?php echo $class; ?>"><?php echo $this->Html->link(__('Orders',true),array('admin'=>true,'plugin' => NULL,'controller'=>'orders','action'=>'index')); ?></li>

        <?php if($controller == 'processors'){ $class ='active'; }else{ $class = ''; } ?>
        <li class="<?php echo $class; ?>"><?php echo $this->Html->link(__('Processors',true),array('admin'=>true,'plugin' => NULL,'controller'=>'processors','action'=>'index')); ?></li>
  
        <?php if($controller == 'statuses'){ $class ='active'; }else{ $class = ''; } ?>
        <li class="<?php echo $class; ?>"><?php echo $this->Html->link(__('Statuses',true),array('admin'=>true,'plugin' => NULL,'controller'=>'statuses','action'=>'index')); ?></li>
              
        <li class="nav-header">Gestion</li>

        <?php if($controller == 'users'){ $class ='active'; }else{ $class = ''; } ?>
        <li class="<?php echo $class; ?>"><?php echo $this->Html->link(__('Users',true),array('admin'=>true,'plugin' => NULL,'controller'=>'users','action'=>'index')); ?></li>

        <?php if($controller == 'groups'){ $class ='active'; }else{ $class = ''; } ?>
        <li class="<?php echo $class; ?>"><?php echo $this->Html->link(__('Groups',true),array('admin'=>true,'plugin' => NULL,'controller'=>'groups','action'=>'index')); ?></li>

        <?php if($controller == 'acl'){ $class ='active'; }else{ $class = ''; } ?>
        <li class="<?php echo $class; ?>"><?php echo $this->Html->link(__('ACL',true),array('admin'=>'true','plugin'=>'acl','controller'=>'acl','action'=>'index')); ?></li>
    </ul>
    <?php //pr(); ?>
</div><!--/.well -->