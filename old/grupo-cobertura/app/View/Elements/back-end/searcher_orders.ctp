<?php
$status = $this->requestAction('store/getStatuses');
$processors = $this->requestAction('store/getProcessors');
?>
<?php
    echo $this->Form->create('Order', array(
        'url' => array_merge(array('controller'=>'orders','action' => 'index'), $this->params['pass']),
        'class' => 'form-inline form-search well',
        'div' => false,
        'style' => 'padding:3px;'
    ));
?>
    <?php
    echo $this->Form->input(
        'Order.id',
        array(
            'div'=>false,
            'label'=>'Folio',
            'class' => 'span1',
            'type' => 'text'
        )
    );
    echo $this->Form->input(
        'Order.status_id',
        array(
            'options'=>$status,
            'div'=>false,
            'label'=>'Estatus',
            'empty' => '-----',
            'class' => 'span1'
        )
    );
    echo $this->Form->input(
        'Order.processor_id',
        array(
            'options'=>$processors,
            'div'=>false,
            'label'=>'Métodos de pago',
            'empty' => '-----',
            'class' => 'span1'
        )
    );
    echo $this->Form->input(
        'Order.user_name',
        array(
            'div'=>false,
            'label'=>'Nombre cliente',
            'class' => 'span2'
        )
    );
    echo $this->Form->input(
        'Order.user_email',
        array(
            'div'=>false,
            'label'=>'Correo cliente',
            'class' => 'span2'
        )
    );
    ?>
    <button type="submit" class="btn">Buscar</button>
  </form>