<div class="row-fluid sortable">
	<div class="box span12">
    	<div class="box-header" data-original-title>
            <h2><i class="halflings-icon user"></i><span class="break"></span><?php echo __('Articles'); ?></h2>
            <div class="box-icon">
                 <i class="icon-plus-sign"></i>
					<?php echo $this->Html->link(__('New Article'), array('action' => 'add')); ?>                <a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
            </div>
        </div>
        <div class="box-content">
        	<table class="table table-striped table-bordered bootstrap-datatable datatable">
            	<thead>
                    <tr>
                                            <th><?php echo $this->Paginator->sort('id'); ?></th>
                                            <th><?php echo $this->Paginator->sort('name'); ?></th>
                                            <th><?php echo $this->Paginator->sort('descripcion'); ?></th>
                                            <th><?php echo $this->Paginator->sort('precio'); ?></th>
                                            <th><?php echo $this->Paginator->sort('cantidad'); ?></th>
                                            <th><?php echo $this->Paginator->sort('order_id'); ?></th>
                                            <th class="actions"><?php echo __('Actions'); ?></th>
                    </tr>
                </thead>
                <tbody>
                	<?php
						foreach ($articles as $article): ?>
	<tr>
		<td><?php echo h($article['Article']['id']); ?>&nbsp;</td>
		<td><?php echo h($article['Article']['name']); ?>&nbsp;</td>
		<td><?php echo h($article['Article']['descripcion']); ?>&nbsp;</td>
		<td><?php echo h($article['Article']['precio']); ?>&nbsp;</td>
		<td><?php echo h($article['Article']['cantidad']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($article['Order']['numero_pedido'], array('controller' => 'orders', 'action' => 'view', $article['Order']['id'])); ?>
		</td>
		<td class="actions">
			<?php echo $this->Html->link(__("<i class='halflings-icon white zoom-in'></i>"), array('action' => 'view', $article['Article']['id']),array( 'class'=>'btn btn-success', 'escape'=>false)); ?>
			<?php echo $this->Html->link(__("<i class='halflings-icon white edit'></i>"), array('action' => 'edit', $article['Article']['id']),array('class'=>'btn btn-info','escape'=>false)); ?>
			<?php echo $this->Form->postLink(__("<i class='halflings-icon white trash'></i>"), array('action' => 'delete', $article['Article']['id']),array( 'class'=>'btn btn-danger', 'escape'=>false), __('Are you sure you want to delete # %s?', $article['Article']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
                       
                </tbody>
            </table>
        </div>
    </div>
</div>