<span style="color:#666; clear:both; font-size:12px; font-family:Arial, Helvetica, sans-serif;">
	Estimado(a) <?php echo $order['Order']['user_name']; ?>,
    <br>
    <?php echo $order['Status']['message'];?>
    <br><br>
</span>
<table style="border:1px solid #e6e6e6; width:600px; padding:20px; color:#666;">
	<tr>
    	<td>
        	<table style="border-bottom:1px solid #868686; width:100%;">
            	<tr>
                	<td><?php echo $this->Html->image(FULL_BASE_URL.Configure::read('cakePagePath').Configure::read('cakeAppPath').DS.'img'.DS.'customer'.DS.'logo.png'); ?></td>
                    <td style="font-size:25px; text-align:right; font-weight:bold;">PEDIDO</td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
    	<td>
        	<table style="width:100%; min-height:100px; vertical-align:top;">
            	<tr>
                	<td style="width:50%;" valign="top">
                    	<span style="color:#006e12; clear:both; font-size:17px; font-weight:bold; font-family:Arial, Helvetica, sans-serif;">
                        	ADA INNOVA
                        </span>
                        <span>
                        	<table style="font-size:12px; color:#333;font-family:Arial, Helvetica, sans-serif;">
                            	<tr>
                                    <td colspan="2">
                                        Tacuba No. 16 Planta alta,<br>
                                        Col. Centro M&eacute;xico D.F.<br>
                                        Tel. 55 12 64 74
                                    </td>
                                </tr>

                                <tr>
                                	<th style="text-align:left;">SITIO WEB:</th>
                                    <td><?php echo $this->Html->link('Ada Innova','http://www.contraste.tv/AdaInnova',array('style'=>'color:#03C'));;?></td>
                                </tr>
                                <!--
                                <tr>
                                	<th style="text-align:left;">EMAIL:</th>
                                    <td>
                                    	info@carabela.com.mx ,
                                        ventas@carabela.com.mx ,
					www.carabela.com.mx

                                    </td>
                                </tr>
                                -->
                            </table>

                        </span>
                    </td>
                    <td style="width:50%;" valign="top">
                    	<span style="color:#006e12; clear:both; font-size:17px; font-weight:bold; font-family:Arial, Helvetica, sans-serif;">
                    		Pedido #<?php echo $order['Order']['id'];?>
                        </span>
                        <span>
                        	<table style="font-size:12px; color:#333;font-family:Arial, Helvetica, sans-serif;">
                            	<tr>
                                	<th style="text-align:left;">ESTATUS:</th>
                                    <td><?php echo $order['Status']['name'];?></td>
                                </tr>
                                <tr>
                                	<th style="text-align:left;">FECHA:</th>
                                    <td><?php echo $order['Order']['created'];?></td>
                                </tr>
                                <tr>
                                	<th style="text-align:left;">METODO PAGO:</th>
                                    <td><?php echo $order['Processor']['name'];?></td>
                                </tr>
                            </table>
                        </span>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
    	<td valign="top">
            <table style="width:100%; min-height:100px;">
                <tr>
                    <td valign="top">
                        <span style="color:#006e12; clear:both; font-size:17px; font-weight:bold; font-family:Arial, Helvetica, sans-serif;">
                            Cliente:
                        </span>
                        <span>
                            <table style="font-size:12px; color:#333;font-family:Arial, Helvetica, sans-serif;">
                                <tr>
                                    <td><strong>Nombre usuario/Alias: </strong><?php echo $order['Order']['user_username'];?></td>
                                </tr>
                                <tr>
                                    <td><strong>Nombre: </strong><?php echo $order['Order']['user_name'];?></td>
                                </tr>
                                <tr>
                                    <td><strong>Correo electrónico: </strong><?php echo $order['Order']['user_email'];?></td>
                                </tr>
                                <tr>
                                    <td><strong>Comentario: </strong><?php echo $order['Order']['cart_comment'];?></td>
                                </tr>
                            </table>
                        </span>
                        <span style="color:#006e12; clear:both; font-size:17px; font-weight:bold; font-family:Arial, Helvetica, sans-serif;">
                            Env&iacute;o:
                        </span>
                        <span>
                            <table style="font-size:12px; color:#333;font-family:Arial, Helvetica, sans-serif;">
                                <tr>
                                    <td><strong>Calle y n&uacute;mero: </strong><?php echo $order['Order']['user_envio_calle_numero'];?></td>
                                </tr>
                                <tr>
                                    <td><strong>Colonia: </strong><?php echo $order['Order']['user_envio_colonia'];?></td>
                                </tr>
                                <tr>
                                    <td><strong>Delegación o Municipio: </strong><?php echo $order['Order']['user_envio_delegacion_municipio_name'];?></td>
                                </tr>
                                <tr>
                                    <td><strong>Estado: </strong><?php echo $order['Order']['user_envio_estado_name'];?></td>
                                </tr>
                                <tr>
                                    <td><strong>C&oacute;digo Postal: </strong><?php echo $order['Order']['user_envio_codigo_postal'];?></td>
                                </tr>
                                <tr>
                                    <td><strong>Teléfono: </strong><?php echo $order['Order']['user_envio_telefono'];?></td>
                                </tr>
                                <tr>
                                    <td><strong>Nombre de quien recibe: </strong><?php echo $order['Order']['user_envio_de_nombre_quien_recibe'];?></td>
                                </tr>
                                <tr>
                                    <td><strong>Nombre transporte: </strong><?php echo $order['Order']['user_envio_transporte_nombre'];?></td>
                                </tr>
                                <tr>
                                    <td><strong>Teléfono transporte: </strong><?php echo $order['Order']['user_envio_transporte_telefono'];?></td>
                                </tr>
                            </table>
                        </span>
                    </td>
                    <td valign="top">
                        <span style="color:#006e12; clear:both; font-size:17px; font-weight:bold; font-family:Arial, Helvetica, sans-serif;">
                            Contacto:
                        </span>
                        <span>
                            <table style="font-size:12px; color:#333;font-family:Arial, Helvetica, sans-serif;">
                                <tr>
                                    <td><strong>Nombre: </strong><?php echo $order['Order']['user_contacto_nombre'];?></td>
                                </tr>
                                <tr>
                                    <td><strong>Cargo: </strong><?php echo $order['Order']['user_contacto_cargo'];?></td>
                                </tr>
                                <tr>
                                    <td><strong>Página web: </strong><?php echo $order['Order']['user_contacto_pagina_web'];?></td>
                                </tr>
                            </table>
                        </span>
                    	<span style="color:#006e12; clear:both; font-size:17px; font-weight:bold; font-family:Arial, Helvetica, sans-serif;">
                    		Facturaci&oacute;n:
                        </span>
                        <span>
                            <table style="font-size:12px; color:#333;font-family:Arial, Helvetica, sans-serif;">
                            	<tr>
                                    <td><strong>RFC: </strong><?php echo $order['Order']['user_facturacion_rfc'];?></td>
                                </tr>
                                <tr>
                                    <td><strong>Raz&oacute;n social: </strong><?php echo $order['Order']['user_facturacion_razon_social'];?></td>
                                </tr>
                                <tr>
                                    <td><strong>Folio c&eacute;dula fiscal: </strong><?php echo $order['Order']['user_facturacion_folio_cedula_fiscal'];?></td>
                                </tr>
                                <tr>
                                    <td><strong>Giro comercial: </strong><?php echo $order['Order']['user_facturacion_giro_comercial'];?></td>
                                </tr>
                                <tr>
                                    <td><strong>Calle y n&uacute;mero: </strong><?php echo $order['Order']['user_facturacion_calle_numero'];?></td>
                                </tr>
                                <tr>
                                    <td><strong>Colonia: </strong><?php echo $order['Order']['user_facturacion_colonia'];?></td>
                                </tr>
                                <tr>
                                    <td><strong>Delegación o Municipio: </strong><?php echo $order['Order']['user_facturacion_delegacion_municipio_name'];?></td>
                                </tr>
                                <tr>
                                    <td><strong>Estado: </strong><?php echo $order['Order']['user_facturacion_estado_name'];?></td>
                                </tr>
                                <tr>
                                    <td><strong>C&oacute;digo Postal: </strong><?php echo $order['Order']['user_facturacion_codigo_postal'];?></td>
                                </tr>
                                <tr>
                                    <td><strong>Tel&eacute;fono: </strong><?php echo $order['Order']['user_facturacion_telefono'];?></td>
                                </tr>
                                <tr>
                                    <td><strong>T&eacute;lefono 2: </strong><?php echo $order['Order']['user_facturacion_telefono_2'];?></td>
                                </tr>
                            </table>
                        </span>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
    	<td>
        	<span style="color:#006e12; clear:both; font-size:17px; font-weight:bold; font-family:Arial, Helvetica, sans-serif;">
           		Detalles Art&iacute;culos
            </span>
        	<table style="width:100%; min-height:100px; border:1px solid#dddddd; border-collapse:collapse;font-weight:bold; font-family:Arial, Helvetica, sans-serif; font-size:12px;">
            	<tr>
                	<th style="border-bottom:1px solid #dddddd; border-right:1px solid #dddddd; background-color:#eeeeee; color:#000; padding:4px;">
                    	Art&iacute;culo
                    </th>
                    <th style="border-bottom:1px solid #dddddd; border-right:1px solid #dddddd; background-color:#eeeeee; color:#000; padding:4px;">
                    	Precio
                    </th>


                </tr>
                <?php foreach($order['Detail'] as $detail):?>
                <tr>
                    <td style="border-bottom:1px solid #dddddd; border-right:1px solid #dddddd; padding:4px; font-size: 11px; width: 150px;">
                        <?php echo $detail['article_code']; ?><br>
                        <em><?php echo $detail['article_description'];?></em>
                    </td>
                    <td style="border-bottom:1px solid #dddddd; border-right:1px solid #dddddd; padding:4px;">
                        <table style="font-size: 11px; border-collapse: collapse; width: 98%; text-align: left;">
                          <tr>
                              <th>Artículo</th>
                              <th>Cantidad surtida</th>
                              <th>Cantidad no surtida</th>
                              <th>Precio unitario</th>
                              <th>Subtotal</th>
                          </tr>
                          <?php foreach($order['Detail'] as $detail): ?>
                          <tr>
                              <td>
                                    <span class="red"><?php echo $detail['article_code']; ?></span><br>
                                    <em><?php echo $detail['article_description']; ?></em>
                              </td>
                              <td><?php echo $detail['article_quantity_assorted']; ?></td>
                              <td><?php echo $detail['article_quantity_no_assorted']; ?></td>
                              <td><?php echo $this->Number->currency($detail['cart_article_unit_price_after_discount_and_iva']); ?></td>
                              <td><?php echo $this->Number->currency(($detail['cart_article_unit_price_after_discount_and_iva'] * ($detail['article_quantity_assorted'] + $detail['article_quantity_no_assorted']))); ?></td>
                          </tr>
                          <?php endforeach; ?>
                      </table>


                        
                    </td>


                </tr>
                <?php endforeach;?>
            </table>
        </td>
    </tr>
    <tr>
    	<td>
        	<br><br>
        	<span>
            	<table style="font-size:12px; color:#333;font-family:Arial, Helvetica, sans-serif;">
                    <tr>
                        <th style="text-align:left">Subtotal:</th>
                	<td><?php echo $this->Number->currency($order['Order']['order_subtotal']);?></td>
                    </tr>
                    <tr>
                    	<th style="text-align:left">Gastos de Envio:</th>
                        <td><?php echo $this->Number->currency($order['Order']['order_shipping_costs']);?></td>
                    </tr>
                    <tr>
                    	<th style="text-align:left;border-top:1px solid #dddddd; font-size:16px;">Total:</th>
                        <td style="font-size:16px;border-top:1px solid #dddddd;">
                    <?php echo $this->Number->currency($order['Order']['order_total']);?></td>
                    </tr>
               </table>
        	</span>
        </td>
    </tr>
</table>
<br><br>
<span style="color:#666; clear:both; font-size:12px; font-family:Arial, Helvetica, sans-serif;">
	Gracias por utilizar nuestra tienda.
</span>
<?php //pr($order);?>