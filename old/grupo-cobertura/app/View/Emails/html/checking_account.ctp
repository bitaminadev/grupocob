<div style="color:#666; clear:both; font-size:12px; font-family:Arial, Helvetica, sans-serif;">
	Estimado(a) <?php echo $user['User']['name'];?>, visita el siguiente link para verificar tu cuenta.<br>
        <?php
        echo $this->Html->link(
            'Verificar mi cuenta',
            FULL_BASE_URL.Configure::read('cakePagePath').Configure::read('cakeAppPath').DS.'registration'.DS.'activeNewUser'.DS.$user['User']['registration_hash']
        );
        ?>
    <br><br>
</div>