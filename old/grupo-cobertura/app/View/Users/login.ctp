<?php 
    	//echo $this->Html->css('/admin/css/bootstrap.min.css');
		//echo $this->Html->css('/admin/css/bootstrap-responsive.min.css');
		//echo $this->Html->css('/admin/css/style.css');
		//echo $this->Html->css('/admin/css/style-responsive.css');
		//echo $this->Html->css('administrador');
    ?>
<div class="login-box">
    <div class="icons">
    	<?php echo $this->Html->link('<i class="halflings-icon home"></i>','#',array('escape'=>false));?>
        <!--<a href="#"><i class="halflings-icon cog"></i></a>-->
    </div>
    <h2>Iniciar sesión</h2>
    <?php echo $this->Form->create('User', array('url' => array('controller' => 'users', 'action' => 'login')));?>
        <fieldset>
        	<div class="input-prepend" title="Username">
                <span class="add-on"><i class="halflings-icon user"></i></span>
        		<input class="input-large span10" name="data[User][username]" id="UserUsername" type="text" placeholder="Usuario"/>
            </div>
            <div class="clearfix"></div>
            <div class="input-prepend" title="Password">
                <span class="add-on"><i class="halflings-icon lock"></i></span>
                <input class="input-large span10" name="data[User][password]" id="UserPassword" type="password" placeholder="Contraseña"/>
            </div>
            <div class="clearfix"></div>
            <div class="button-login">	
            	<?php echo $this->Form->end(array('label'=>'Guardar','class'=>'btn btn-primary','div'=>false)); ?>
            </div>
            <div class="clearfix"></div>
        </fieldset>        
    <hr>
    <h3>No puedes acceder a tu cuneta?</h3>
    <p>
       No hay problema, <?php  
       	 echo $this->Html->link('haga clic aquí',array('admin'=>false,'plugin'=>NULL,'controller'=>'registration','action'=>'recoverPassword'));
      ?> para obtener una nueva contraseña.
    </p>	
</div>