<?php ?>
<div class="row-fluid sortable">        
    <div class="box span12">
        <div class="box-header" data-original-title>
            <h2><i class="icon-bar-chart"></i><span class="break"></span><?php echo 'Datos'; ?></h2>
            <div class="box-icon">
               <a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
            </div>
         </div>
         <div class="box-content">
            <div class="tabbable">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#tab1" data-toggle="tab"><?php echo 'User'; ?></a></li>
                </ul>
            </div>
            <div class="tab-content">
                <div class="tab-pane active" id="tab1">
                    <p>
                        <dl>
                <dt><?php echo __('Id'); ?></dt>
                <dd><?php echo h($user['User']['id']); ?>&nbsp;</dd>
                <dt><?php echo __('Enable'); ?></dt>
                <dd>
                    <?php
                        if($user['User']['enable']){
                            echo '<i class="icon-ok"></i>';
                        }else{
                            echo '<i class="icon-remove"></i>';
                        }

                    ?>&nbsp;
                </dd>
                <dt><?php echo __('Username'); ?></dt>
                <dd><?php echo h($user['User']['username']); ?>&nbsp;</dd>
                <dt><?php echo __('Name'); ?></dt>
                <dd>
                    <?php echo h($user['User']['name']); ?>
                    &nbsp;
                </dd>
                        <dt><?php echo __('Email'); ?></dt>
                <dd>
                    <?php echo h($user['User']['email']); ?>
                    &nbsp;
                </dd>

                <dt><?php echo __('Group'); ?></dt>
                <dd>
                    <?php echo $this->Html->link($user['Group']['name'], array('controller' => 'groups', 'action' => 'view', $user['Group']['id'])); ?>
                    &nbsp;
                </dd>
                        <dt><?php echo __('Created'); ?></dt>
                <dd>
                    <?php echo h($user['User']['created']); ?>
                    &nbsp;
                </dd>
                <dt><?php echo __('Modified'); ?></dt>
                <dd>
                    <?php echo h($user['User']['modified']); ?>
                    &nbsp;
                </dd>
            </dl>
                   </p>
                </div>
            </div> 
        </div>  
    </div>
</div>
<div class="row-fluid"> 
    <div class="box blue span12">
        <div class="box-header">
            <h2><i class="halflings-icon hand-top"></i><span class="break"></span>Acciones</h2>
        </div>
        <div class="box-content">
            <?php echo $this->Html->link('<i class="icon-undo"></i><p>Regresar</p>','#', array('class'=>'quick-button span2 back','escape'=>false)); ?>                 
            <?php echo $this->Html->link(__('<i class="icon-edit"></i><p>Editar</p>'), array('action' => 'edit', $user['User']['id']),array('class'=>'quick-button span2','escape'=>false)); ?>
            <?php echo $this->Form->postLink(__('<i class="icon-trash"></i><p>Eliminar</p>'), array('action' => 'delete', $user['User']['id']), array('class'=>'quick-button span2','escape'=>false), __('Are you sure you want to delete # %s?', $user['User']['id'])); ?>
            <?php echo $this->Html->link(__('<i class="icon-plus"></i><p>Agregar Usuario</p>'), array('action' => 'add'),array('class'=>'quick-button span2','escape'=>false)); ?>
            <div class="clearfix"></div>
        </div>  
    </div>  
</div>