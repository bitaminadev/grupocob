<?php //echo $this->element('back-end/searcher_articles'); ?>
<div class="row-fluid sortable">        
    <div class="box span12">
        <div class="box-header" data-original-title>
            <h2><i class="halflings-icon user"></i><span class="break"></span><?php echo __('Users'); ?></h2>
            <div class="box-icon">
                 <i class="icon-plus-sign"></i>
                    <?php echo $this->Html->link(__('New User'), array('action' => 'add')); ?>
                <a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
            </div>
        </div>
        <div class="box-content">
            <table class="table table-striped table-bordered bootstrap-datatable datatable">
                <thead>
                    <tr>
                        <th><?php echo ('enable'); ?></th>
                        <th><?php echo ('username'); ?></th>
                        <th><?php echo ('name'); ?></th>
                        <th><?php echo ('group_id'); ?></th>
                        <th class="actions"><?php echo __('Actions'); ?></th>
                    </tr>
                </thead>
                <tbody>
                <?php
                foreach ($users as $user): ?>
                    <td>
                        <?php
                            if($user['User']['enable']){
                                echo '<i class="icon-ok"></i>';
                            }else{
                                echo '<i class="icon-remove"></i>';
                            }
                        ?>&nbsp;
                    </td>
                    <td><?php echo h($user['User']['username']); ?>&nbsp;</td>
                    <td><?php echo h($user['User']['name']); ?>&nbsp;</td>
                    <td>
                        <?php echo $this->Html->link($user['Group']['name'], array('controller' => 'groups', 'action' => 'view', $user['Group']['id'])); ?>
                    </td>
                    <td class="actions">
 <?php echo $this->Html->link(__('Ver'), array('action' => 'view', $user['User']['id']),array( "class"=>"btn btn-success", 'escape'=>false)); ?>&nbsp;
<?php echo $this->Html->link(__('Editar'), array('action' => 'edit', $user['User']['id']),array( "class"=>"btn btn-info", 'escape'=>false)); ?>&nbsp;
<?php echo $this->Form->postLink(__('Eliminar'), array('action' => 'delete', $user['User']['id']),array( "class"=>"btn btn-danger", 'escape'=>false), __('Are you sure you want to delete # %s?', $user['User']['id'])); ?>&nbsp;
                    </td>
                </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
            <?php
                /*echo $this->Paginator->counter(array(
                'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
                ));*/
                ?>
            <!--<div class="pagination pagination-centered">
              <ul>
                <li><?php echo $this->Paginator->prev(__('previous'), array(), null, array('class' => 'prev disabled'));?></li>
                <li><?php echo $this->Paginator->numbers(array('separator' => ''));?></li>
                <li><?php echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));?></li>
              </ul>
            </div>-->
        </div>
    </div>
</div>