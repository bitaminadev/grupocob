<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>CONTRASTE</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Le styles -->
    <link rel="stylesheet/less" href="<?php echo FULL_BASE_URL.Configure::read('cakePagePath').Configure::read('cakeAppPath'); ?>/libs/bootstrap_twitter/less/bootstrap.less" />
    <?php 
    //echo $this->Html->css('/libs/bootstrap_twitter/less/bootstrap.less','stylesheet/less');
    echo $this->Html->script('less-1.3.0.min');
    echo $this->Html->script('jquery/jquery-1.8.2.min');
    echo $this->Html->script('back-end');
    echo $this->Html->css('back-end');
    ?>
    <style type="text/css">
      body {
        padding-top: 60px;
        padding-bottom: 40px;
      }
      .sidebar-nav {
        padding: 9px 0;
      }
    </style>

    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!-- Le fav and touch icons -->
    <link rel="shortcut icon" href="../assets/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../assets/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../assets/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../assets/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="../assets/ico/apple-touch-icon-57-precomposed.png">
  </head>

  <body>
    <?php echo $this->element('back-end/header'); ?>
    <div class="container-fluid">
        <div class="row-fluid">
            <div class="span2">
                <?php echo $this->element('back-end/nav'); ?>
            </div><!--/span-->
            <div class="span10 container well">
                <div class="">
                    <?php echo $this->Session->flash(); ?>
                    <?php echo $this->Session->flash('auth'); ?>
                    <?php echo $this->fetch('content'); ?>
                </div>
            </div><!--/span-->
        </div><!--/row-->
        <hr>
        <footer>
            <p>&copy; Powered by Contraste diseño y publicidad 2012</p>
        </footer>
    </div><!--/.fluid-container-->

    <!-- Le javascript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <?php
    echo $this->Html->script('/libs/bootstrap_twitter/js/bootstrap-tab');
    //echo $this->Html->script('/libs/bootstrap_twitter/js/bootstrap-button');
    ?>
    <!--
    <script src="../assets/js/jquery.js"></script>
    <script src="../assets/js/bootstrap-transition.js"></script>
    <script src="../assets/js/bootstrap-alert.js"></script>
    <script src="../assets/js/bootstrap-modal.js"></script>
    <script src="../assets/js/bootstrap-dropdown.js"></script>
    <script src="../assets/js/bootstrap-scrollspy.js"></script>
    <script src="../assets/js/bootstrap-tooltip.js"></script>
    <script src="../assets/js/bootstrap-popover.js"></script>
    <script src="../assets/js/bootstrap-collapse.js"></script>
    <script src="../assets/js/bootstrap-carousel.js"></script>
    <script src="../assets/js/bootstrap-typeahead.js"></script>
    -->
    <?php echo $this->element('sql_dump'); ?>
  </body>
</html>
