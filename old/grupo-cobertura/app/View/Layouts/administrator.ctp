<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Grupo Cobertura</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- start: Mobile Specific -->
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- end: Mobile Specific -->
	
	<!-- start: CSS -->
    <?php 
		echo $this->Html->css('reste.css');
    	echo $this->Html->css('/admin/css/bootstrap.css');
		echo $this->Html->css('/admin/css/bootstrap-responsive.min.css');
		echo $this->Html->css('/admin/css/style.css');
		echo $this->Html->css('/admin/css/style-responsive.css');
    ?>
     <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800&subset=latin,cyrillic-ext,latin-ext' rel='stylesheet' type='text/css'>
	<!-- end: CSS -->
	

	<!-- The HTML5 shim, for IE6-8 support of HTML5 elements -->
	<!--[if lt IE 9]>
	  	<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<link id="ie-style" href="css/ie.css" rel="stylesheet">
	<![endif]-->
	
	<!--[if IE 9]>
		<link id="ie9style" href="css/ie9.css" rel="stylesheet">
	<![endif]-->
		
	<!-- start: Favicon -->
	<link rel="shortcut icon" href="img/favicon.ico">
	<!-- end: Favicon -->
  </head>

  <body>
  	<!-- start: Header -->
        <?php echo $this->element('administrador/header'); ?>
	<!-- start: Header -->
	
		<div class="container-fluid-full">
            <div class="row-fluid">
                <!-- start: Main Menu -->
                    <?php echo $this->element('administrador/nav'); ?>
                <!-- end: Main Menu -->
                <noscript>
                    <div class="alert alert-block span10">
                        <h4 class="alert-heading">Warning!</h4>
                        <p>You need to have <a href="http://en.wikipedia.org/wiki/JavaScript" target="_blank">JavaScript</a> enabled to use this site.</p>
                    </div>
                </noscript>
                <!-- start: Content -->
                <div id="content" class="span10">
                    <?php echo $this->Session->flash(); ?>
                    <?php echo $this->Session->flash('auth'); ?>
                    <?php echo $this->fetch('content'); ?>
                </div>
                <!-- end: Content -->
            </div><!--/#content.span10-->
		</div><!--/fluid-row-->
		
	<div class="modal hide fade" id="myModal">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal">×</button>
			<h3>Settings</h3>
		</div>
		<div class="modal-body">
			<p>Here settings can be configured...</p>
		</div>
		<div class="modal-footer">
			<a href="#" class="btn" data-dismiss="modal">Close</a>
			<a href="#" class="btn btn-primary">Save changes</a>
		</div>
	</div>
	
	<div class="clearfix"></div>
	
	<footer>

		<p>
			<span style="text-align:left;float:left">&copy; 2018 <a href="#" alt="">Grupo Cobertura S. de R.L. de C.V.</a></span>
			
		</p>

	</footer>

    <!-- Le javascript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <?php
    	echo $this->Html->script('/admin/js/jquery-1.9.1.min.js');
		echo $this->Html->script('/admin/js/jquery-migrate-1.0.0.min.js');
		echo $this->Html->script('/admin/js/jquery-ui-1.10.0.custom.min.js');
		echo $this->Html->script('/admin/js/jquery.ui.touch-punch.js');
		echo $this->Html->script('/admin/js/modernizr.js');
		echo $this->Html->script('/admin/js/bootstrap.min.js');
		echo $this->Html->script('/admin/js/jquery.cookie.js');
		echo $this->Html->script('/admin/js/fullcalendar.min.js');
		echo $this->Html->script('/admin/js/jquery.dataTables.js');
		echo $this->Html->script('/admin/js/excanvas.js');
		echo $this->Html->script('/admin/js/jquery.flot.js');
		echo $this->Html->script('/admin/js/jquery.flot.pie.js');
		echo $this->Html->script('/admin/js/jquery.flot.stack.js');
		echo $this->Html->script('/admin/js/jquery.flot.resize.min.js');
		echo $this->Html->script('/admin/js/jquery.chosen.min.js');
		echo $this->Html->script('/admin/js/jquery.uniform.min.js');
		echo $this->Html->script('/admin/js/jquery.cleditor.min.js');
		echo $this->Html->script('/admin/js/jquery.noty.js');
		echo $this->Html->script('/admin/js/jquery.elfinder.min.js');
		echo $this->Html->script('/admin/js/jquery.raty.min.js');
		echo $this->Html->script('/admin/js/jquery.iphone.toggle.js');
		echo $this->Html->script('/admin/js/jquery.uploadify-3.1.min.js');
		echo $this->Html->script('/admin/js/jquery.gritter.min.js');
		echo $this->Html->script('/admin/js/jquery.imagesloaded.js');
		echo $this->Html->script('/admin/js/jquery.masonry.min.js');
		echo $this->Html->script('/admin/js/jquery.knob.modified.js');
		echo $this->Html->script('/admin/js/jquery.sparkline.min.js');
		echo $this->Html->script('/admin/js/counter.js');
		echo $this->Html->script('/admin/js/retina.js');
		echo $this->Html->script('/admin/js/custom.js');
		echo $this->Html->script('administrador.js');
    //echo $this->Html->script('/libs/bootstrap_twitter/js/bootstrap-button');
    ?>
    <!-- start: JavaScript-->

		
	<!-- end: JavaScript-->
    <!--
    <script src="../assets/js/jquery.js"></script>
    <script src="../assets/js/bootstrap-transition.js"></script>
    <script src="../assets/js/bootstrap-alert.js"></script>
    <script src="../assets/js/bootstrap-modal.js"></script>
    <script src="../assets/js/bootstrap-dropdown.js"></script>
    <script src="../assets/js/bootstrap-scrollspy.js"></script>
    <script src="../assets/js/bootstrap-tooltip.js"></script>
    <script src="../assets/js/bootstrap-popover.js"></script>
    <script src="../assets/js/bootstrap-collapse.js"></script>
    <script src="../assets/js/bootstrap-carousel.js"></script>
    <script src="../assets/js/bootstrap-typeahead.js"></script>
    -->
    <?php echo $this->element('sql_dump'); ?>
  </body>
</html>
