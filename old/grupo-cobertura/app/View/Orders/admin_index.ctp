<div class="row-fluid sortable">
	<div class="box span12">
    	<div class="box-header" data-original-title>
            <h2><i class="halflings-icon user"></i><span class="break"></span><?php echo __('Orders'); ?></h2>
            <div class="box-icon">
                 <i class="icon-plus-sign"></i>
					<?php echo $this->Html->link(__('New Order'), array('action' => 'add')); ?>                <a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
            </div>
        </div>
        <div class="box-content">
        	<table class="table table-striped table-bordered bootstrap-datatable datatable">
            	<thead>
                    <tr>
                                            <th><?php echo $this->Paginator->sort('id'); ?></th>
                                            <th><?php echo $this->Paginator->sort('numero_pedido'); ?></th>
                                            <th><?php echo $this->Paginator->sort('fecha_pedido'); ?></th>
                                            <th><?php echo $this->Paginator->sort('provider_id'); ?></th>
                                            <th><?php echo $this->Paginator->sort('total'); ?></th>
                                            <th><?php echo $this->Paginator->sort('work_id'); ?></th>
                                            <th><?php echo $this->Paginator->sort('status_id'); ?></th>
                                            <th class="actions"><?php echo __('Actions'); ?></th>
                    </tr>
                </thead>
                <tbody>
                	<?php
						foreach ($orders as $order): ?>
	<tr>
		<td><?php echo h($order['Order']['id']); ?>&nbsp;</td>
		<td><?php echo h($order['Order']['numero_pedido']); ?>&nbsp;</td>
		<td><?php echo h($order['Order']['fecha_pedido']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($order['Provider']['nombre_empresa'], array('controller' => 'providers', 'action' => 'view', $order['Provider']['id'])); ?>
		</td>
		<td><?php echo h($order['Order']['total']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($order['Work']['name'], array('controller' => 'works', 'action' => 'view', $order['Work']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($order['Status']['name'], array('controller' => 'statuses', 'action' => 'view', $order['Status']['id'])); ?>
		</td>
		<td class="actions">
			<?php echo $this->Html->link(__("<i class='halflings-icon white zoom-in'></i>"), array('action' => 'view', $order['Order']['id']),array( 'class'=>'btn btn-success', 'escape'=>false)); ?>
			<?php echo $this->Html->link(__("<i class='halflings-icon white edit'></i>"), array('action' => 'edit', $order['Order']['id']),array('class'=>'btn btn-info','escape'=>false)); ?>
			<?php echo $this->Form->postLink(__("<i class='halflings-icon white trash'></i>"), array('action' => 'delete', $order['Order']['id']),array( 'class'=>'btn btn-danger', 'escape'=>false), __('Are you sure you want to delete # %s?', $order['Order']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
                       
                </tbody>
            </table>
        </div>
    </div>
</div>