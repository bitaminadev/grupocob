<div class="row-fluid sortable">		
	<div class="box span12">
    	<div class="box-header" data-original-title>
            <h2><i class="halflings-icon edit"></i><span class="break"></span><?php echo __('Admin Add Order'); ?></h2>
           	<div class="box-icon">
                <a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
            </div>
        </div>
        <div class="box-content">
        	<?php echo $this->Form->create('Order'); ?>
            <fieldset>
            	<div class="control-group">
                    <div class="span6">
                        <legend>Datos Generales</legend>
                        	<?php
		echo $this->Form->input('numero_pedido');
		echo $this->Form->input('fecha_pedido');
		echo $this->Form->input('provider_id');
		echo $this->Form->input('total');
		echo $this->Form->input('work_id');
		echo $this->Form->input('status_id');
	?>
                    </div>
        		</div>
          	</fieldset>
            <div class="form-actions">
            	<?php echo $this->Form->end(array('label'=>'Guardar','class'=>'btn btn-primary','div'=>false)); ?>
            	<button type="button" class="btn goBack back">Cancelar</button>
            </div 
    	</div>
	</div>
</div>
<div class="row-fluid">	
    <div class="box blue span12">
        <div class="box-header">
            <h2><i class="halflings-icon hand-top"></i><span class="break"></span>Acciones</h2>
        </div>
        <div class="box-content">
     		<?php echo $this->Html->link('<i class="icon-undo"></i><p>Regresar</p>','#', array('class'=>'quick-button span2 back','escape'=>false)); ?>                        
			                        
            
            <div class="clearfix"></div>
        </div>	
    </div>  
</div>