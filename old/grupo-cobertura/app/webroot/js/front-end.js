/*
 * Bootstrap
 */
$(document).ready(function(){
   /*Ingles*/
   $("a:contains('View')").addClass('btn');
   $("a:contains('Delete')").addClass('btn');
   $("a:contains('Edit')").addClass('btn');
   $("a:contains('Change password')").addClass('btn btn-primary');
   $("a:contains('Go back')").addClass('btn btn-primary');
   $("a:contains('Go back')").click(function(){
      history.back();
   });
   $(".goBack").click(function(){
      history.back();
   });
   /*Español*/
   $("a:contains('Ver')").addClass('btn');
   $("a:contains('Eliminar')").addClass('btn');
   $("a:contains('Editar')").addClass('btn');
   $("a:contains('Cambiar contraseña')").addClass('btn');
   $("a:contains('Regresar')").addClass('btn btn-primary');
   $("a:contains('Regresar')").click(function(){
      history.back();
   });
   /*Comun*/
   $('input[type="submit"]').addClass('btn');
  
});