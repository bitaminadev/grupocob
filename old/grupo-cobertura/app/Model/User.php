<?php
//App::uses('AppModel', 'Model');
App::uses('AuthComponent', 'Controller/Component');
/**
 * User Model
 *
 * @property Group $Group
 * @property Order $Order
 */
class User extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'name';
        public $primaryKey = 'id';
        public $actsAs = array('Acl' => array('type' => 'requester'),'Containable','Search.Searchable');
/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
            /*
             * INICIO DE SESION
             */
            'username' => array(
                'notempty' => array(
                    'rule' => array('notempty'),
                    'message' => 'Este campo es obligatorio',
                    //'allowEmpty' => false,
                    //'required' => false,
                    //'last' => false, // Stop validation after this rule
                    //'on' => 'create', // Limit validation to 'create' or 'update' operations
		),
                'isUnique' => array(
                    'rule' => array('isUnique'),
                    'message'=>'Este alias ya existe'
                ),
                'alphaNumeric' => array(
                    'rule' => array('alphaNumeric'),
                    'message' => 'Solo se permiten letras y números'
                )
            ),
            'password' => array(
                'notempty' => array(
                    'rule' => array('notempty'),
                    'message' => 'Este campo es obligatorio',
                    //'allowEmpty' => false,
                    //'required' => false,
                    //'last' => false, // Stop validation after this rule
                    //'on' => 'create', // Limit validation to 'create' or 'update' operations
		),
                'minLength' => array(
                    'rule' => array('minLength',5),
                    'message' => 'Indique un mínimo de 5 caracteres'
                )
            ),
            'name' => array(
                'notempty' => array(
                    'rule' => array('notempty'),
                    'message' => 'Este campo es obligatorio',
                )
            ),
            'email' => array(
                'notempty' => array(
                    'rule' => array('notempty'),
                    'message' => 'Este campo es obligatorio',
                ),
                'email' => array(
                    'rule' => array('email',true),
                    'message' => 'Indique un correo electrónico valido',
                    //'allowEmpty' => false,
                    //'required' => false,
                    //'last' => false, // Stop validation after this rule
                    //'on' => 'create', // Limit validation to 'create' or 'update' operations
                ),
                'isUnique' => array(
                    'rule' => array('isUnique'),
                    'message'=>'Este correo electrónico ya existe'
                )
            ),
            'discount' => array(
                'numeric' => array(
                    'rule' => array('numeric'),
                    'message' => 'Indique números'
                ),
                'comparison' => array(
                    'rule' => array('comparison','<=',100),
                    'message' => 'No puedes definir un descuento mayor al 100%'
                )
            ),
            'shipping_costs' => array(
                'notempty' => array(
                    'rule' => array('notempty'),
                    'message' => 'Este campo es obligatorio',
                ),
                'dicimal' => array(
                    'rule' => array('decimal',2),
                    'message' => 'Indique este campo con dos números decimales'
                )
            ),
            /*
             * FACTURACION
             */
            'facturacion_rfc' => array(
                'notempty' => array(
                    'rule' => array('notempty'),
                    'message' => 'Este campo es obligatorio',
                )
            ),
            'facturacion_razon_social' => array(
                'notempty' => array(
                    'rule' => array('notempty'),
                    'message' => 'Este campo es obligatorio',
                )
            ),
            'facturacion_folio_cedula_fiscal' => array(
                'notempty' => array(
                    'rule' => array('notempty'),
                    'message' => 'Este campo es obligatorio',
                )
            ),
            'facturacion_giro_comercial' => array(
                'notempty' => array(
                    'rule' => array('notempty'),
                    'message' => 'Este campo es obligatorio',
                )
            ),
            'facturacion_calle_numero' => array(
                'notempty' => array(
                    'rule' => array('notempty'),
                    'message' => 'Este campo es obligatorio',
                )
            ),
            'facturacion_colonia' => array(
                'notempty' => array(
                    'rule' => array('notempty'),
                    'message' => 'Este campo es obligatorio',
                )
            ),
            'facturacion_delegacion_municipio_id' => array(
                'notempty' => array(
                    'rule' => array('notempty'),
                    'message' => 'Este campo es obligatorio',
                )
            ),
            'facturacion_estado_id' => array(
                'notempty' => array(
                    'rule' => array('notempty'),
                    'message' => 'Este campo es obligatorio',
                )
            ),
            'facturacion_codigo_postal' => array(
                'notempty' => array(
                    'rule' => array('notempty'),
                    'message' => 'Este campo es obligatorio',
                )
            ),
            'facturacion_telefono' => array(
                'notempty' => array(
                    'rule' => array('notempty'),
                    'message' => 'Este campo es obligatorio',
                )
            ),
            /*
             * ENVIO
             */
            'envio_calle_numero' => array(
                'notempty' => array(
                    'rule' => array('notempty'),
                    'message' => 'Este campo es obligatorio',
                )
            ),
            'envio_colonia' => array(
                'notempty' => array(
                    'rule' => array('notempty'),
                    'message' => 'Este campo es obligatorio',
                )
            ),
            'envio_delegacion_municipio_id' => array(
                'notempty' => array(
                    'rule' => array('notempty'),
                    'message' => 'Este campo es obligatorio',
                )
            ),
            'envio_estado_id' => array(
                'notempty' => array(
                    'rule' => array('notempty'),
                    'message' => 'Este campo es obligatorio',
                )
            ),
            'envio_codigo_postal' => array(
                'notempty' => array(
                    'rule' => array('notempty'),
                    'message' => 'Este campo es obligatorio',
                )
            ),
            'envio_telefono' => array(
                'notempty' => array(
                    'rule' => array('notempty'),
                    'message' => 'Este campo es obligatorio',
                )
            ),
            'envio_de_nombre_quien_recibe' => array(
                'notempty' => array(
                    'rule' => array('notempty'),
                    'message' => 'Este campo es obligatorio',
                )
            ),
            'envio_transporte_nombre' => array(
                'notempty' => array(
                    'rule' => array('notempty'),
                    'message' => 'Este campo es obligatorio',
                )
            ),
            'envio_transporte_telefono' => array(
                'notempty' => array(
                    'rule' => array('notempty'),
                    'message' => 'Este campo es obligatorio',
                )
            ),
            /*
             * CONTACTO
             */
            'contacto_nombre' => array(
                'notempty' => array(
                    'rule' => array('notempty'),
                    'message' => 'Este campo es obligatorio',
                )
            ),
            'contacto_cargo' => array(
                'notempty' => array(
                    'rule' => array('notempty'),
                    'message' => 'Este campo es obligatorio',
                )
            ),
            'contacto_pagina_web' => array(
                'notempty' => array(
                    'rule' => array('notempty'),
                    'message' => 'Este campo es obligatorio',
                )
            ),
            /*
             * OTROS
             */
            'group_id' => array(
                'notempty' => array(
                    'rule' => array('notempty'),
                    'message' => 'Este campo es obligatorio',
                    //'allowEmpty' => false,
                    //'required' => false,
                    //'last' => false, // Stop validation after this rule
                    //'on' => 'create', // Limit validation to 'create' or 'update' operations
		),
            ),
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Group' => array(
			'className' => 'Group',
			'foreignKey' => 'group_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
                /*
                'State' => array(
                    'className' => 'State',
                    'foreignKey' => 'state_id'
                ),
                'City' => array(
                    'className' => 'City',
                    'foreignKey' => 'city_id'
                )
                 * 
                 */
	);

/**
 * hasMany associations
 *
 * @var array
 */
	
        

        public function parentNode() {
            if (!$this->id && empty($this->data)) {
                return null;
            }
            if (isset($this->data['User']['group_id'])) {
                $groupId = $this->data['User']['group_id'];
            } else {
                $groupId = $this->field('group_id');
            }
            if (!$groupId) {
                return null;
            } else {
                return array('Group' => array('id' => $groupId));
            }
        }
        //Encripta la contraseña
        public function beforeSave($options = array()) {
            if(!empty($this->data['User']['password'])){
                $this->data['User']['password'] = AuthComponent::password($this->data['User']['password']);
                return true;
            }
        }
        /*
        public function bindNode($user) {
            return array('model' => 'Group', 'foreign_key' => $user['User']['group_id']);
        }
         *
         */
        /*
         * PLUGIN SEARCH
         */
        public $filterArgs = array(
            'username' => array('type'=>'like'),
            'name' => array('type'=>'like'),
            'email' => array('type'=>'like'),
            'group_id' => array('type'=>'value'),
        );
}
?>