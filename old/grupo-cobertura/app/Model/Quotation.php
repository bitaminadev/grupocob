<?php
App::uses('AppModel', 'Model');
/**
 * Quotation Model
 *
 */
class Quotation extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'name';

}
