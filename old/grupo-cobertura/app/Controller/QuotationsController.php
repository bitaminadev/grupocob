<?php
App::uses('AppController', 'Controller');
/**
 * Quotations Controller
 *
 * @property Quotation $Quotation
 */
class QuotationsController extends AppController {

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->Quotation->recursive = 0;
		$this->set('quotations', $this->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		$this->Quotation->id = $id;
		if (!$this->Quotation->exists()) {
			throw new NotFoundException(__('Invalid quotation'));
		}
		$this->set('quotation', $this->Quotation->read(null, $id));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->Quotation->create();
			if ($this->Quotation->save($this->request->data)) {
				$this->Session->setFlash(__('The quotation has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The quotation could not be saved. Please, try again.'));
			}
		}
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		$this->Quotation->id = $id;
		if (!$this->Quotation->exists()) {
			throw new NotFoundException(__('Invalid quotation'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Quotation->save($this->request->data)) {
				$this->Session->setFlash(__('The quotation has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The quotation could not be saved. Please, try again.'));
			}
		} else {
			$this->request->data = $this->Quotation->read(null, $id);
		}
	}

/**
 * admin_delete method
 *
 * @throws MethodNotAllowedException
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->Quotation->id = $id;
		if (!$this->Quotation->exists()) {
			throw new NotFoundException(__('Invalid quotation'));
		}
		if ($this->Quotation->delete()) {
			$this->Session->setFlash(__('Quotation deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Quotation was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
}
