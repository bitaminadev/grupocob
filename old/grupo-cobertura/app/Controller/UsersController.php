<?php
App::uses('AppController', 'Controller');
/**
 * Users Controller
 *
 * @property User $User
 */
class UsersController extends AppController {
        public $uses = array('User');
        public $paginate = array();
        public $components  = array('Search.Prg');
        public $presetVars = true; // using the model configuration
        public function beforeFilter(){
            parent::beforeFilter();
            $this->Auth->allow('login','logout');
        }
/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
				$this->layout = 'administrator';
				$this->set('title_for_layout', 'Usuarios');
                $this->Prg->commonProcess();
                $this->paginate['conditions'] = $this->User->parseCriteria($this->passedArgs);
                $users = $this->paginate('User');
                $this->set('users',$users);
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		$this->User->id = $id;
		if (!$this->User->exists()) {
			throw new NotFoundException(__('Invalid user'));
		}
		$this->set('user', $this->User->read(null, $id));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->User->create();
			if ($this->User->save($this->request->data)) {
				$this->Session->setFlash(__('The user has been saved'));
				$this->redirect(array('action' => 'view',$this->User->id));
			} else {
				$this->Session->setFlash(__('The user could not be saved. Please, try again.'));
			}
		}
		$groups = $this->User->Group->find('list');
		$this->set(compact('groups'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		$this->User->id = $id;
		if (!$this->User->exists()) {
			throw new NotFoundException(__('Invalid user'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->User->save($this->request->data)) {
				$this->Session->setFlash(__('The user has been saved'));
				$this->redirect(array('action' => 'view',$this->User->id));
			} else {
				$this->Session->setFlash(__('The user could not be saved. Please, try again.'));
			}
		} else {
			$this->request->data = $this->User->read(null, $id);
		}
		$groups = $this->User->Group->find('list');
		$this->set(compact('groups'));
    }

/**
 * admin_delete method
 *
 * @throws MethodNotAllowedException
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->User->id = $id;
		if (!$this->User->exists()) {
			throw new NotFoundException(__('Invalid user'));
		}
		if ($this->User->delete()) {
			$this->Session->setFlash(__('User deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('User was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
   	public function admin_changePassword($id = null){
           $this->User->id = $id;
		if (!$this->User->exists()) {
			throw new NotFoundException(__('Invalid user'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
                    if($this->request->data['User']['password'] == $this->request->data['User']['passwordConfirm']){
                            if ($this->User->save($this->request->data)) {
                                    $this->Session->setFlash(__('The new password has been saved'));
                                    $this->redirect(array('action' => 'view',$this->User->id));
                            } else {
                                    $this->Session->setFlash(__('The new password could not be saved. Please, try again.'));
                            }
                    }else{
                        $this->Session->setFlash(__('Passwords do not match'));
                    }
		}else{
                    $this->request->data = $this->User->read(null, $id);
                    unset($this->request->data['User']['password']);
                }
                
       }
       public function login() {
            $this->set('title_for_layout','Iniciar sesión');
			$this->layout = 'login';
            if ($this->request->is('post')) {
                if ($this->Auth->login()) {
                   
                    if(in_array($this->Session->read('Auth.User.group_id'),array(1,2))){
                        $this->redirect(array('admin'=>true,'plugin'=>NULL,'controller'=>'users','action'=>'index'));
                    }
                    $this->Session->setFlash('Bienvenido');
                    $this->redirect(array('admin'=>false,'plugin'=>NULL,'controller'=>'customer','action'=>'myOrders'));
                } else {
                    $this->Session->setFlash('Your username or password was incorrect.');
                }
            }
        }
        public function logout(){
            $this->Session->setFlash('Good-Bye');
            $this->redirect($this->Auth->logout());
        }
}
