<?php
/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */

App::uses('Controller', 'Controller');

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package       app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller {
    public $helpers = array('Html', 'Form', 'Session');
    /*
    public $components = array(
        'Acl',
        'Auth' => array(
            //'authorize' => array('Actions' => array('actionPath' => 'controllers')),

            
            'authenticate' => array(
                'Form' => array(
                    'userModel' => 'Userid'
                )
            ),
            
            'loginAction' => array('admin'=>false,'controller' => 'users','action' => 'login','plugin' => NULL),
            'scope' => array('User.enable' => 1),
            'authError' => 'Did you really think you are allowed to see that?',
            'loginRedirect' => array('admin'=>true,'plugin'=>NULL,'controller'=>'articles','action'=>'index'),
            'logoutRedirect' => array('admin'=>false,'controller' => 'users','action' => 'login','plugin' => NULL),
        ),
        'Session',
        'RequestHandler'
    );
            */
    public $components = array(
        'Acl',
        'Auth' => array(
            'authorize' => array(
                'Actions' => array('actionPath' => 'controllers')
            )
        ),
        'Session'
    );
    
   

    function  beforeFilter() {
        parent::beforeFilter();
        
        $this->Auth->loginAction = array('admin'=>false,'plugin'=>NULL,'controller' => 'users', 'action' => 'login');
        $this->Auth->logoutRedirect = array('admin'=>false,'plugin'=>NULL,'controller' => 'users', 'action' => 'login');
        $this->Auth->loginRedirect = array('admin'=>true,'plugin'=>NULL,'controller' => 'users', 'action' => 'index');
        $this->Auth->scope = array('User.enable' => 1);
        $this->Auth->autoRedirect = false;
        $this->Auth->authError = 'Inicia sesión o registrate para acceder';
        if(isset($this->request->params['admin']) && $this->request->params['admin'] == 1){
            $this->layout = 'administrator';
        }
    }
}
