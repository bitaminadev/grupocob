<?php
App::uses('EvaluationsController', 'Controller');

/**
 * EvaluationsController Test Case
 *
 */
class EvaluationsControllerTest extends ControllerTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.evaluation',
		'app.order',
		'app.provider',
		'app.quoatation',
		'app.providers_quoatation',
		'app.work',
		'app.contractor',
		'app.status',
		'app.article'
	);

/**
 * testAdminIndex method
 *
 * @return void
 */
	public function testAdminIndex() {
	}

/**
 * testAdminView method
 *
 * @return void
 */
	public function testAdminView() {
	}

/**
 * testAdminAdd method
 *
 * @return void
 */
	public function testAdminAdd() {
	}

/**
 * testAdminEdit method
 *
 * @return void
 */
	public function testAdminEdit() {
	}

/**
 * testAdminDelete method
 *
 * @return void
 */
	public function testAdminDelete() {
	}

}
