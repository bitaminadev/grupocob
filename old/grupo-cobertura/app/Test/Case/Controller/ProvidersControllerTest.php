<?php
App::uses('ProvidersController', 'Controller');

/**
 * ProvidersController Test Case
 *
 */
class ProvidersControllerTest extends ControllerTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.provider',
		'app.evaluation',
		'app.order',
		'app.work',
		'app.contractor',
		'app.status',
		'app.article',
		'app.quoatation',
		'app.providers_quoatation'
	);

/**
 * testAdminIndex method
 *
 * @return void
 */
	public function testAdminIndex() {
	}

/**
 * testAdminView method
 *
 * @return void
 */
	public function testAdminView() {
	}

/**
 * testAdminAdd method
 *
 * @return void
 */
	public function testAdminAdd() {
	}

/**
 * testAdminEdit method
 *
 * @return void
 */
	public function testAdminEdit() {
	}

/**
 * testAdminDelete method
 *
 * @return void
 */
	public function testAdminDelete() {
	}

}
