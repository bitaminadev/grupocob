<?php
App::uses('ContractorsController', 'Controller');

/**
 * ContractorsController Test Case
 *
 */
class ContractorsControllerTest extends ControllerTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.contractor',
		'app.work',
		'app.order',
		'app.provider',
		'app.evaluation',
		'app.quoatation',
		'app.providers_quoatation',
		'app.status',
		'app.article'
	);

/**
 * testAdminIndex method
 *
 * @return void
 */
	public function testAdminIndex() {
	}

/**
 * testAdminView method
 *
 * @return void
 */
	public function testAdminView() {
	}

/**
 * testAdminAdd method
 *
 * @return void
 */
	public function testAdminAdd() {
	}

/**
 * testAdminEdit method
 *
 * @return void
 */
	public function testAdminEdit() {
	}

/**
 * testAdminDelete method
 *
 * @return void
 */
	public function testAdminDelete() {
	}

}
