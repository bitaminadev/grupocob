<?php
App::uses('WorksController', 'Controller');

/**
 * WorksController Test Case
 *
 */
class WorksControllerTest extends ControllerTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.work',
		'app.contractor',
		'app.order',
		'app.provider',
		'app.evaluation',
		'app.quoatation',
		'app.providers_quoatation',
		'app.status',
		'app.article'
	);

/**
 * testAdminIndex method
 *
 * @return void
 */
	public function testAdminIndex() {
	}

/**
 * testAdminView method
 *
 * @return void
 */
	public function testAdminView() {
	}

/**
 * testAdminAdd method
 *
 * @return void
 */
	public function testAdminAdd() {
	}

/**
 * testAdminEdit method
 *
 * @return void
 */
	public function testAdminEdit() {
	}

/**
 * testAdminDelete method
 *
 * @return void
 */
	public function testAdminDelete() {
	}

}
