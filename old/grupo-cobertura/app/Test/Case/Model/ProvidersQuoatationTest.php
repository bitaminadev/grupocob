<?php
App::uses('ProvidersQuoatation', 'Model');

/**
 * ProvidersQuoatation Test Case
 *
 */
class ProvidersQuoatationTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.providers_quoatation',
		'app.quoatation',
		'app.product',
		'app.provider',
		'app.evaluation',
		'app.order',
		'app.work',
		'app.status',
		'app.article'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->ProvidersQuoatation = ClassRegistry::init('ProvidersQuoatation');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->ProvidersQuoatation);

		parent::tearDown();
	}

}
