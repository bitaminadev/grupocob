<?php
App::uses('ProvidersQuotation', 'Model');

/**
 * ProvidersQuotation Test Case
 *
 */
class ProvidersQuotationTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.providers_quotation',
		'app.quotation',
		'app.product',
		'app.providers_quoatation',
		'app.provider',
		'app.evaluation',
		'app.order',
		'app.work',
		'app.contractor',
		'app.status',
		'app.article',
		'app.quoatation'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->ProvidersQuotation = ClassRegistry::init('ProvidersQuotation');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->ProvidersQuotation);

		parent::tearDown();
	}

}
