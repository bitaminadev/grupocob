<?php
/**
 * EvaluationFixture
 *
 */
class EvaluationFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'key' => 'primary'),
		'atencion' => array('type' => 'string', 'null' => true, 'default' => null, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'precio_competitivo' => array('type' => 'string', 'null' => true, 'default' => null, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'tiempo_entrega' => array('type' => 'string', 'null' => true, 'default' => null, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'calidad_produco' => array('type' => 'string', 'null' => true, 'default' => null, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'order_id' => array('type' => 'integer', 'null' => true, 'default' => null),
		'provider_id' => array('type' => 'integer', 'null' => true, 'default' => null),
		'confianza' => array('type' => 'string', 'null' => true, 'default' => null, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'latin1', 'collate' => 'latin1_swedish_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'atencion' => 'Lorem ipsum dolor sit amet',
			'precio_competitivo' => 'Lorem ipsum dolor sit amet',
			'tiempo_entrega' => 'Lorem ipsum dolor sit amet',
			'calidad_produco' => 'Lorem ipsum dolor sit amet',
			'order_id' => 1,
			'provider_id' => 1,
			'confianza' => 'Lorem ipsum dolor sit amet'
		),
	);

}
