<?php
/**
 * OrderFixture
 *
 */
class OrderFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'key' => 'primary'),
		'numero_pedido' => array('type' => 'string', 'null' => true, 'default' => null, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'fecha_pedido' => array('type' => 'date', 'null' => true, 'default' => null),
		'provider_id' => array('type' => 'integer', 'null' => true, 'default' => null),
		'total' => array('type' => 'string', 'null' => true, 'default' => null, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'work_id' => array('type' => 'integer', 'null' => true, 'default' => null),
		'status_id' => array('type' => 'integer', 'null' => true, 'default' => null),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'latin1', 'collate' => 'latin1_swedish_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'numero_pedido' => 'Lorem ipsum dolor sit amet',
			'fecha_pedido' => '2018-12-17',
			'provider_id' => 1,
			'total' => 'Lorem ipsum dolor sit amet',
			'work_id' => 1,
			'status_id' => 1
		),
	);

}
