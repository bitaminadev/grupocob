<?php
/**
 * Copyright 2010 - 2011, Cake Development Corporation (http://cakedc.com)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright 2010 - 2011, Cake Development Corporation (http://cakedc.com)
 * @license MIT License (http://www.opensource.org/licenses/mit-license.php)
 */
?>
<div class="row">
    <div class="span12">
        <?php echo $this->element('front-end/elementBuscador'); ?>
    </div>
</div>
<div class="row">
    <div class="span4 offset4">
        <h3 class="bg_color_green">Registro nuevos usuarios</h3>
        <?php
        echo $this->Form->create($model);
        echo $this->Form->input('username', array('label' => __d('users', 'Username'),'class'=>'span4'));
	echo $this->Form->input('email', array(
            'label' => 'Correo (usado para ingresar)',
            'error' => array(
                'isValid' => __d('users', 'Must be a valid email address'),
                'isUnique' => __d('users', 'An account with that email already exists')
            ),
            'class'=>'span4'
        ));
	echo $this->Form->input('password', array(
            'label' => __d('users', 'Password'),
            'type' => 'password',
            'class'=>'span4'
        ));
	echo $this->Form->input('temppassword', array(
            'label' => __d('users', 'Password (confirm)'),
            'type' => 'password',
            'class'=>'span4'
        ));
        //$tosLink = $this->Html->link(__d('users', 'Terms of Service'), array('controller' => 'pages', 'action' => 'tos'));
	//echo $this->Form->input('tos', array('label' => __d('users', 'I have read and agreed to ') . $tosLink));
	echo $this->Form->end(__d('users', 'Submit'));
        ?>
    </div>
</div>