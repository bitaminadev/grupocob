<?php
/**
 * Copyright 2010 - 2011, Cake Development Corporation (http://cakedc.com)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright 2010 - 2011, Cake Development Corporation (http://cakedc.com)
 * @license MIT License (http://www.opensource.org/licenses/mit-license.php)
 */
?>
<div class="row">
    <div class="span12">
        <?php echo $this->element('front-end/elementBuscador'); ?>
    </div>
</div>
<div class="row">
    <div class="span4 offset4">
        <h3 class="bg_color_green">Iniciar sesión</h3>
        <?php
        echo $this->Form->create($model, array('action' => 'login','id' => 'LoginForm'));
        echo $this->Form->input('email', array('label' => __d('users', 'Email'),'class'=>'span4'));
        echo $this->Form->input('password',  array('label' => __d('users', 'Password'),'class'=>'span4'));
        echo '<p>' . __d('users', 'Remember Me') . $this->Form->checkbox('remember_me') . '</p>';
        echo '<p>' . $this->Html->link('Recordar contraseña', array('action' => 'reset_password')) . '</p>';
        echo $this->Form->hidden('User.return_to', array('value' => $return_to));
        echo $this->Form->end(__d('users', 'Submit'));
        ?>
    </div>
</div>
<?php //echo $this->element('Users/sidebar'); ?>