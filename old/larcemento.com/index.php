<!DOCTYPE html>
<html>
<head><meta http-equiv="Content-Type" content="text/html; charset=gb18030">
	<title>Lar Construcciones</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- JQuery -->
    <script src="js/jquery-3.2.1.js"></script>

    <link rel="stylesheet" type="text/css" href="css/main.css">
    <link rel="stylesheet" type="text/css" href="css/responsive_main.css">
	<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
	<script type="text/javascript" src="js/bootstrap.min.js"></script>
</head>
<body class="">

	<header class="col-md-12 col-sm-12 nav-conte no-pad">
		<div class="col-md-6 col-sm-6 col-xs-2 no-pad">
			<img src="img/logo.png" class="logo">
		</div>
		<div class="col-md-6 col-sm-6 col-xs-10 inbl no-pad">
			<h3 style="font-weight: bold;margin: 0;">VENTA DE MATERIALES PARA LA CONSTRUCCI&Oacute;N</h3>
			<h3 style="margin: 0 0 5px 0;">TEL:(998)286 02 31</h3>
			<h4 style="font-weight: bold;margin: 0 0 5px 0;">SM 65 M7 L1 CALLE 13 ESQ. FRANCISCO I MADERO</h4>
			<h4 style="font-weight: bold;margin: 0 0 5px 0;">(RUTA 4) MUNICIPIO BENITO JU&Aacute;REZ</h4>
			<h4 style="font-weight: bold;margin: 0 0 5px 0;">CANC&Uacute;N, QROO C.P. 77524</h4>
		</div>
	</header>

	<div class="wrap row">
	<div class="slider">
  		<h2></h2>  
		<div id="myCarousel" class="carousel slide" data-ride="carousel" style="width:100%;">
		    <!-- Indicators -->
		    <ol class="carousel-indicators">
		      <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
		      <!--li data-target="#myCarousel" data-slide-to="1"></li>
		      <li data-target="#myCarousel" data-slide-to="2"></li-->
		    </ol>

		    <!-- Wrapper for slides -->
		    <div class="carousel-inner">
		      <div class="item active">
		        <img src="img/slider_1.jpg" alt="Cemento" style="width:100%;">
		      </div>

		      <!--div class="item">
		        <img src="chicago.jpg" alt="Chicago" style="width:100%;">
		      </div>
		    
		      <div class="item">
		        <img src="ny.jpg" alt="New york" style="width:100%;">
		      </div-->
		    </div>

		    <!-- Left and right controls -->
		    <a class="left carousel-control" href="#myCarousel" data-slide="prev">
		      <span class="glyphicon glyphicon-chevron-left"></span>
		      <span class="sr-only">Previous</span>
		    </a>
		    <a class="right carousel-control" href="#myCarousel" data-slide="next">
		      <span class="glyphicon glyphicon-chevron-right"></span>
		      <span class="sr-only">Next</span>
		    </a>
		</div>
	</div>

	<div class="row">
		<div class="col-md-4 col-sm-4 col-xs-4" style="padding: 0;"><hr class="separadores sseparador"></div>
		<div class="col-md-4 col-sm-4 col-xs-4" style="padding: 0;"><h2 class="titulos" style="text-align: center;">PRODUCTOS</h2></div>
		<div class="col-md-4 col-sm-4 col-xs-4 rseparador" style="padding: 0;"><hr class="separadores sseparador" ></div>
	</div>

	<div class="">
		<!--h5 class="info text-muted">
			Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto
		</h5-->
	</div>

	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="col-md-12 col-sm-12 col-xs-12 placeholders" style="text-align: center;">
            <div class="col-xs-6 col-sm-3 placeholder">
              <img src="img/productos1.jpg" class="img-responsive circleimage" alt="Generic placeholder thumbnail">
              <h5 class="text-muted" style="font-weight: bold;">PRODUCTOS ENVASADOS</h5>
            </div>
            <div class="col-xs-6 col-sm-3 placeholder">
              <img src="img/productos2.jpg" class="img-responsive circleimage" alt="Generic placeholder thumbnail">
              <h5 class="text-muted" style="font-weight: bold;">ACEROS</h5>
            </div>
            <div class="col-xs-6 col-sm-3 placeholder">
              <img src="img/productos3.jpg" class="img-responsive circleimage" alt="Generic placeholder thumbnail">
              <h5 class="text-muted" style="font-weight: bold;">BLOCKS Y AGREGADOS</h5>
            </div>
            <div class="col-xs-6 col-sm-3 placeholder">
              <img src="img/productos4.png" class="img-responsive circleimage" alt="Generic placeholder thumbnail">
              <h5 class="text-muted" style="font-weight: bold;">HERRAMIENTA TRUPER</h5>
            </div>
        </div>

        <div class="cold-md-12 col-sm-12 col-xs-12" style="margin-top: 25px;">
			<div class="col-md-6 col-sm-12 col-xs-12 text-center">
				<img class="img-des" src="img/Envasados.png">
			</div>
			<div class="col-md-6 col-sm-12 col-xs-12">
				<h2 class="titulos" style="text-align: center;">ENVASADOS</h2>
				<hr class="separadores">
				<h5 class="info text-muted">
					ENVASADOS<br><br>
					Siendo distribuidores oficiales de CEMEX, contamos con toda la gama de productos de esta reconocida marca, como lo es:<br>
					Cemento CEMEX MAYA EXTRA, dise&ntilde;ado especialmente para reducir la aparici&oacute;n de grietas, facilita el proceso de curado y tiene un mejor mezclado, adherencia y trabajabilidad, a&uacute;n con agregados dif&iacute;ciles.<br>
					Permite que tanto la arena, la grava, el cemento y el agua se mantengan unidos dentro de la masa de concreto, evitando la segregaci&oacute;n y sangrado en exceso, lo que facilita los procesos de preparaci&oacute;n, colocaci&oacute;n y curado.<br>
					Cemento BLANCO CEMEX, Es un cemento con las mismas caracter&iacute;sticas que un cemento Portland y posee la caracter&iacute;stica de ser blanco, es ideal para obras ornamentales o arquitect&oacute;nicas. As&iacute; mismo se recomienda como adhesivo o estuco para hacer emboquillados y junta de azulejos.<br>
					MORTERO CEMEX TOLTECA, Est&aacute; dise&ntilde;ado para trabajos que no requieren elevadas resistencias y producir mezclas m&aacute;s manejables y de m&aacute;xima adherencia.<br>
					De igual forma contamos con una extensa gama de marcas y productos envasados como: MASILLA, CAL, PEGAZULEJO, entre otros.
				</h5>
			</div>
		</div>

	</div>

	<div class="cold-md-12 col-sm-12 col-xs-12">
		<div class="col-md-4 col-sm-4 col-xs-4" style="padding: 0;"><hr class="separadores"></div>
		<div class="col-md-4 col-sm-4 col-xs-4" style="padding: 0;"><h2 class="titulos" style="text-align: center;">QUI&Eacute;NES SOMOS</h2></div>
		<div class="col-md-4 col-sm-4 col-xs-4" style="padding: 0;"><hr class="separadores"></div>
	</div>

	<div class="col-md-12 col-sm-12 col-xs-12">
		<h5 class="cold-md-12 col-sm-12 col-xs-12 info text-muted">
			LAR CEMENTO Y ACERO S.A. DE C.V. es una empresa comprometida con el medio ambiente y cuidadosamente creada para satisfacer las necesidades de nuestros clientes con formalidad, oportunidad y el trato que se merecen, en la distribuci&oacute;n, comercializaci&oacute;n y venta de productos de la mas alta calidad a un excelente precio.
			Somos distribuidores directos de las marcas m&aacute;s importantes.
		</h5>
	</div>

	<div class="cold-md-12 col-sm-12 col-xs-12" style="padding: 0 20px">
		<div id="padtoright" class="col-md-6 col-sm-12">
			<div class="slider">
		  		<h2></h2>  
				<div id="myCarousel2" class="carousel slide" data-ride="carousel" style="width:100%;">
				    <!-- Indicators -->
				    <ol class="carousel-indicators">
				      <li data-target="#myCarousel2" data-slide-to="0" class="active"></li>
				      <!--li data-target="#myCarousel" data-slide-to="1"></li>
				      <li data-target="#myCarousel" data-slide-to="2"></li-->
				    </ol>

				    <!-- Wrapper for slides -->
				    <div id="slide2" class="carousel-inner">
				      <div class="item active">
				        <img id="slide2-item1" src="img/slide1.jpg" alt="Cemento">
				      </div>

				      <div class="item">
				        <img id="slide2-item2" src="img/slide2.png" alt="Herramientas">
				      </div>
				    
				      <!--div class="item">
				        <img src="ny.jpg" alt="New york" style="width:100%;">
				      </div-->
				    </div>

				    <!-- Left and right controls -->
				    <a class="left carousel-control" href="#myCarousel2" data-slide="prev">
				      <span class="glyphicon glyphicon-chevron-left"></span>
				      <span class="sr-only">Previous</span>
				    </a>
				    <a class="right carousel-control" href="#myCarousel2" data-slide="next">
				      <span class="glyphicon glyphicon-chevron-right"></span>
				      <span class="sr-only">Next</span>
				    </a>
				</div>
			</div>
		</div>
		<div class="col-md-6 col-sm-12 pad-mv">
			<h2 class="titulos" style="text-align: center;">MISI&Oacute;N / VISI&Oacute;N</h2>
			<hr class="separadores">
			<div class="cold-md-12 col-sm-12 col-xs-12 cont-mv">
				<h5 class="info text-muted">
					MISI&Oacute;N / VISI&Oacute;N
					Ser la empresa l&iacute;der con el mejor servicio de distribuci&oacute;n y venta de productos de calidad para el profesional de la construcci&oacute;n, esforz&aacute;ndonos cada d&iacute;a m&aacute;s en el cuidado del medio ambiente.
				</h5>
				<h5 class="info text-muted">
					VALORES
					Para LAR CEMENTO Y ACERO el respeto, la honestidad, la pasi&oacute;n y la formalidad constituyen la principal forma de trabajar de todos nuestros colaboradores.
				</h5>
			</div>
		</div>
	</div>
	</div>

	<footer class="col-md-12 col-sm-12 nav-conte no-pad">
		<div class="col-md-6 col-sm-6 col-xs-2 no-pad">
			<img src="img/logo.png" class="logo">
		</div>
		<div class="col-md-6 col-sm-6 col-xs-10 inbl no-pad">
			<h3 style="font-weight: bold;margin: 0;">VENTA DE MATERIALES PARA LA CONSTRUCCI&Oacute;N</h3>
			<h3 style="margin: 0 0 5px 0;">TEL:(998)286 02 31</h3>
			<h4 style="font-weight: bold;margin: 0 0 5px 0;">SM 65 M7 L1 CALLE 13 ESQ. FRANCISCO I MADERO</h4>
			<h4 style="font-weight: bold;margin: 0 0 5px 0;">(RUTA 4) MUNICIPIO BENITO JU&Aacute;REZ</h4>
			<h4 style="font-weight: bold;margin: 0 0 5px 0;">CANC&Uacute;N, QROO C.P. 77524</h4>
		</div>
	</footer>

</body>
</html>