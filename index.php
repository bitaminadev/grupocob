<!DOCTYPE html>
<?php 
    $title = 'Despacho de arquitectos para proyectos y construcción en Querétaro';
	$description = 'Servicios de planos arquitectónicos, ingeniería, BIM, modelos 3d y construcción de casas, comercios, edificios, plazas comerciales, naves industriales y más en Querétaro';
	$keywords = 'despacho de arquitectos en Querétaro, planos arquitectónicos, ingeniería, construcción';
    $author = 'Posicionart';
    
    $httpProtocol = 'https://';
    $host = $_SERVER['SERVER_NAME'];
    $url = '/grupocob/';

    $page = 'home';
    include('commons/_headOpen.php');
    $css .= '<link rel="stylesheet" type="text/css" href="'.$httpProtocol.$host.$url.'css/stylesHome.css">';
    include('commons/_headClose.php');
    include('views/navbar.html');
    include('views/index.html');
    include('views/footer.html');
?>