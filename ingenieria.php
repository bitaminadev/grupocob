<!DOCTYPE html>
<?php 
    $title = 'Servicios de ingeniería para arquitectos y constructores en Querétaro';
	$description = 'Servicio de ingeniería estructural, eléctrica, hidráulica y sanitaria para arquitectos y constructores en Querétaro.';
	$keywords = 'ingeniería estructural, ingeniería eléctrica, ingeniería hidráulica, servicio de ingeniería en querétaro';
    $author = 'Posicionart';
    
    $httpProtocol = 'https://';
    $host = $_SERVER['SERVER_NAME'];
    $url = '/grupocob/';

    $page = 'ingenieria';
    include('commons/_headOpen.php');
    $css .= '
        <link rel="stylesheet" type="text/css" href="'.$httpProtocol.$host.$url.'css/stylesIngenierias.css">
        <link rel="stylesheet" type="text/css" href="'.$httpProtocol.$host.$url.'css/stylesHome.css">
        ';
    include('commons/_headClose.php');
    include('views/navbar.html');
    include('views/ingenieria.html');
    include('views/footer.html');
?>