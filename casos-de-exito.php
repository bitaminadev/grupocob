<!DOCTYPE html>
<?php 
    session_start();
    $_SESSION["error"] = 0;
    $title = 'Casos de Éxito - Grupo COB';
	$description = 'Estos son algunos de los muchos casos de éxito que hemos generado.';
	$keywords = 'casos, exito, grupo, cob, harley';
    $author = 'Posicionart';
    
    $httpProtocol = 'https://';
    $host = $_SERVER['SERVER_NAME'];
    $url = '/grupocob/';

    $page = 'casos';
    include('commons/_headOpen.php');
    $js.= '
        <script src="'.$httpProtocol.$host.$url.'js/casos.js"></script>';
    $css .= '
        <link rel="stylesheet" type="text/css" href="'.$httpProtocol.$host.$url.'css/stylesHome.css">
        <link rel="stylesheet" type="text/css" href="'.$httpProtocol.$host.$url.'css/stylesCasos.css">
        ';
    include('commons/_headClose.php');
    include('views/navbar.html');
    include('views/casos-exito.html');
    include('views/footer.html');
?>