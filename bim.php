<!DOCTYPE html>
<?php 
    $title = 'Servicio de BIM modelos de pre-construcción virtual en Querétaro';
	$description = 'Creamos modelos de pre-construcción virtual para generar anticipaciones de estructura e instalaciones para diferentes tipos de proyectos en Querétaro.';
	$keywords = 'servicio BIM, pre construcción, modelo virtual, arquitectura en querétaro';
    $author = 'Posicionart';
    
    $httpProtocol = 'https://';
    $host = $_SERVER['SERVER_NAME'];
    $url = '/grupocob/';

    $page = 'bim';
    include('commons/_headOpen.php');
    $css .= '
        <link rel="stylesheet" type="text/css" href="'.$httpProtocol.$host.$url.'css/stylesBIM.css">
        <link rel="stylesheet" type="text/css" href="'.$httpProtocol.$host.$url.'css/stylesHome.css">
        ';
    include('commons/_headClose.php');
    include('views/navbar.html');
    include('views/bim.html');
    include('views/footer.html');
?>