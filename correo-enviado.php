<!DOCTYPE html>
<?php 
    $title = 'Correo Enviado - Grupo COB';
	$description = 'Página de correo enviado.';
	$keywords = 'correo, enviado, grupo, cob';
    $author = 'Posicionart';
    
    $httpProtocol = 'https://';
    $host = $_SERVER['SERVER_NAME'];
    $url = '/grupocob/';

    include('commons/_headOpen.php');
    $css .= '
        <link rel="stylesheet" type="text/css" href="'.$httpProtocol.$host.$url.'css/stylesSecondaryNavbar.css">
        ';
    include('commons/_headClose.php');
    include('views/navbar2.html');
    include('views/correo-enviado.html');
    include('views/footer.html');
?>