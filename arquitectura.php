<!DOCTYPE html>
<?php 
    $title = 'Despacho de arquitectura en Querétaro para proyectos de construcción';
	$description = 'Servicio de renders arquitectónicos, planos arquitectónicos y recorridos virtuales para proyectos residenciales, comerciales e industriales en Querétaro';
	$keywords = 'despacho de arquitectura, rendes arquitectónicos, planos arquitectónicos, recorridos virtuales';
    $author = 'Posicionart';
    
    $httpProtocol = 'https://';
    $host = $_SERVER['SERVER_NAME'];
    $url = '/grupocob/';

    $page = 'arquitectura';
    include('commons/_headOpen.php');
    $css .= '
        <link rel="stylesheet" type="text/css" href="'.$httpProtocol.$host.$url.'css/stylesArquitectura.css">
        <link rel="stylesheet" type="text/css" href="'.$httpProtocol.$host.$url.'css/stylesHome.css">
        ';
    include('commons/_headClose.php');
    include('views/navbar.html');
    include('views/arquitectura.html');
    include('views/footer.html');
?>