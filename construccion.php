<!DOCTYPE html>
<?php 
    $title = 'Construcción residencial, comercial e industrial en Querétaro';
	$description = 'Servicio de construcción en Querétaro para proyectos en topografía, terracería, cimentaciones, instalaciones eléctricas, hidráulicas y sanitarias.';
	$keywords = 'construcción en querétaro, topografía, terracería, cimentaciones, instalaciones eléctricas, instalaciones hidráulicas';
    $author = 'Posicionart';
    
    $httpProtocol = 'https://';
    $host = $_SERVER['SERVER_NAME'];
    $url = '/grupocob/';

    $page = 'construccion';
    include('commons/_headOpen.php');
    $css .= '
        <link rel="stylesheet" type="text/css" href="'.$httpProtocol.$host.$url.'css/stylesConstruccion.css">
        <link rel="stylesheet" type="text/css" href="'.$httpProtocol.$host.$url.'css/stylesHome.css">
        ';
    include('commons/_headClose.php');
    include('views/navbar.html');
    include('views/construccion.html');
    include('views/footer.html');
?>