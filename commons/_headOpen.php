<?php
    // Variables que almacenan el css y el js de la pagina
    $js = '
        <script src="'.$httpProtocol.$host.$url.'js/jquery-3.4.1.min.js"></script>
        <script src="'.$httpProtocol.$host.$url.'js/bootstrap.min.js"></script>
        <script src="'.$httpProtocol.$host.$url.'js/index.js"></script>
        <script>
        $(function () {
          $(document).scroll(function () {
            var $nav = $("#navbar-transparent");
            var $nav2 = $(".banner-main");
            var $tog = $(".custom-toggler");
            $nav.toggleClass("scrolled", $(this).scrollTop() > $nav2.height()-48);
            $tog.toggleClass("scrolled", $(this).scrollTop() > $nav2.height()-48);
          });
        });
        </script>
    ';
    $css = '
        <link rel="stylesheet" type="text/css" href="'.$httpProtocol.$host.$url.'css/webfonts/all.min.css">
        <link rel="stylesheet" type="text/css" href="'.$httpProtocol.$host.$url.'css/bootstrap.min.css">
        <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="'.$httpProtocol.$host.$url.'css/styles.css">
        <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap" rel="stylesheet">
    ';
    $hreflang = '<link rel="alternate" hreflang="x-default" href="'.$httpProtocol.$host.$_SERVER["REQUEST_URI"].'">';
?>
<html prefix="og: http://ogp.me/ns#" lang="es">
<head>
	
    <title><?php echo $title?></title>
    <meta charset="UTF-8">
    <meta name="description" content="<?php echo $description ?>">
    <meta name="keywords" content="<?php echo $keywords ?>">
    <meta name="author" content="<?php echo $author ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"/>
    
    <link rel="icon" type="image/png" href="<?php echo $httpProtocol.$host.$url.'img/favicon/favicon.ico'?>">

    <!--Metas Geo-->
    <meta name="geo.region" content="MX-QUE" />
    <meta name="geo.placename" content="Quer&eacute;taro" />
    <meta name="geo.position" content="20.589103;-100.365155" />
    <meta name="ICBM" content="20.589103, -100.365155" />
    
    <!--Metas OG-->
    <meta property="og:locale" content="es_MX" />
    <meta property="og:title" content="<?php echo $title?>" />
    <meta property="og:description" content="<?php echo $description ?>" />
    <meta property="og:type" content="website" />
    <meta property="og:url" content="<?php echo $httpProtocol.$host.$_SERVER['REQUEST_URI'] ?>" />
    <meta property="og:image" content="<?php echo $httpProtocol.$host.$url.'images/favicon/favicon.ico'?>" />
    <meta property="og:site_name" content="Grupo COB" />

    <!--Metas DC-->
    <meta content="<?php echo $title?>" NAME='DC.Title'/>
    <meta content="<?php echo $description ?>" NAME='DC.Description'/>
    <meta content="<?php echo $author ?>" NAME='DC.Creator'/>
    <meta content='Grupo COB' NAME='DC.Publisher'/>
    <meta content="<?php echo $httpProtocol.$host.$_SERVER['REQUEST_URI'] ?>" NAME='DC.Identifier'/>
    <meta content="<?php echo $keywords ?>" NAME='DC.keywords'/>